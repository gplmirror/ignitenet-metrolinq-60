#include <stdio.h>
#include <stdlib.h>
//#include <malloc.h>
#include <sys/types.h>
//#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
//#include <sys/shm.h>
#include <math.h>


#define IGI_2G 0x30
#define IGI_5G 0x3e
#define BASE_2G 62
#define BASE_5G 59


int psd(char* inf, char* channel, double* dbm);

int main(int argc, char** argv)
{
	int rc;
	double dbm[32];
	char echo_file[500];
	
	rc=psd(argv[1],argv[2],dbm);
	if (rc)
		return rc;
	
	//printf("\n\r%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n\r",dbm[0],dbm[1],dbm[2],dbm[3],dbm[4],dbm[5],dbm[6],dbm[7],dbm[8],dbm[9],dbm[10],dbm[11],dbm[12],dbm[13],dbm[14],dbm[15],dbm[16],dbm[17],dbm[18],dbm[19],dbm[20],dbm[21],dbm[22],dbm[23],dbm[24],dbm[25],dbm[26],dbm[27],dbm[28],dbm[29],dbm[30],dbm[31]);
	
	sprintf(echo_file,"echo %.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f  > /var/psd",dbm[0],dbm[1],dbm[2],dbm[3],dbm[4],dbm[5],dbm[6],dbm[7],dbm[8],dbm[9],dbm[10],dbm[11],dbm[12],dbm[13],dbm[14],dbm[15],dbm[16],dbm[17],dbm[18],dbm[19],dbm[20],dbm[21],dbm[22],dbm[23],dbm[24],dbm[25],dbm[26],dbm[27],dbm[28],dbm[29],dbm[30],dbm[31]);
	
	system(echo_file);
	return 0;
}

int psd(char* inf, char* channel, double* dbm)
{
	char cmd[100],file_path[100];
	char read_file[500],temp_file[300],elm[32][10],*temp;
	int i,j,k, wait_cnt = 0;
	int elm_int[32],base;
	double igi;
	int fh;
	
	sprintf(cmd,"echo 0 > /proc/%s/psd_scan_done",inf);
	system(cmd);
	
	sprintf(cmd,"echo \"%s\" Channel > /proc/%s/psd_scan",channel,inf);
	system(cmd);
	
	sprintf(file_path,"/proc/%s/psd_scan_done",inf);
	fh = open(file_path, O_RDONLY);
	if ( fh == -1 )
		return 1;
	read(fh, read_file, sizeof(read_file));
	
	while (read_file == "0") {
	    if(wait_cnt > 200 )
	        break;
	    usleep(10000);
	    read(fh, read_file, sizeof(read_file));
	    wait_cnt++;
	}
	close(fh);

	//usleep(600000);
	//sleep(1);
	sprintf(file_path,"/proc/%s/psd_scan",inf);
	fh = open(file_path, O_RDONLY);
	if ( fh == -1 )
		return 1;

	read(fh, read_file, sizeof(read_file));
	close(fh);
	
	
	//printf("\n\r%s\n\r",cmd);
	//printf("\n\r%s\n\r",read_file);
	temp=strstr(read_file,")");
	//printf("\n\r%s\n\r",temp_file);
	temp++;
	while ((*temp==' ')||(*temp=='	')||(*temp=='\n'))
		temp++;
		
	strcpy(temp_file,temp);
	//printf("\n\r%s\n\r",temp_file);
	
	i=j=k=0;

	while (temp_file[i]!='\0')
	{
		if ((temp_file[i]!=' ')&&(temp_file[i]!='	')&&(temp_file[i]!='\n'))
		{
			elm[j][k]=temp_file[i];
			k++;
		}	
		else
		{
			elm[j][k]='\0';
			if ((temp_file[i+1]!=' ')&&(temp_file[i+1]!='	')&&(temp_file[i+1]!='\n'))
			{
				j++;
				k=0;
			}
		}
	
		i++;
	}
	elm[j][k]='\0';
	
	//printf("\n\rs0=%s  s1=%s  s2=%s  s3=%s",elm[0],elm[1],elm[2],elm[3]);
	//printf("\n\rs4=%s  s5=%s  s6=%s  s7=%s",elm[4],elm[5],elm[6],elm[7]);
	//printf("\n\rs8=%s  s9=%s  s10=%s  s11=%s",elm[8],elm[9],elm[10],elm[11]);
	//printf("\n\rs12=%s  s13=%s  s14=%s  s15=%s",elm[12],elm[13],elm[14],elm[15]);
	//printf("\n\rs16=%s  s17=%s  s18=%s  s19=%s",elm[16],elm[17],elm[18],elm[19]);
	//printf("\n\rs20=%s  s21=%s  s22=%s  s23=%s",elm[20],elm[21],elm[22],elm[23]);
	//printf("\n\rs24=%s  s25=%s  s26=%s  s27=%s",elm[24],elm[25],elm[26],elm[27]);
	//printf("\n\rs28=%s  s29=%s  s30=%s  s31=%s",elm[28],elm[29],elm[30],elm[31]);
	//printf("\n\r");
	
	for (i=0;i<32;i++)
	{
		elm_int[i]=strtol(elm[i],NULL,16);
	}

	//printf("\n\rs0=%d  s1=%d  s2=%d  s3=%d",elm_int[0],elm_int[1],elm_int[2],elm_int[3]);
	//printf("\n\rs4=%d  s5=%d  s6=%d  s7=%d",elm_int[4],elm_int[5],elm_int[6],elm_int[7]);
	//printf("\n\rs8=%d  s9=%d  s10=%d  s11=%d",elm_int[8],elm_int[9],elm_int[10],elm_int[11]);
	//printf("\n\rs12=%d  s13=%d  s14=%d  s15=%d",elm_int[12],elm_int[13],elm_int[14],elm_int[15]);
	//printf("\n\rs16=%d  s17=%d  s18=%d  s19=%d",elm_int[16],elm_int[17],elm_int[18],elm_int[19]);
	//printf("\n\rs20=%d  s21=%d  s22=%d  s23=%d",elm_int[20],elm_int[21],elm_int[22],elm_int[23]);
	//printf("\n\rs24=%d  s25=%d  s26=%d  s27=%d",elm_int[24],elm_int[25],elm_int[26],elm_int[27]);
	//printf("\n\rs28=%d  s29=%d  s30=%d  s31=%d",elm_int[28],elm_int[29],elm_int[30],elm_int[31]);
	#if 1 //by stoneshih
	for (i=0;i<32;i++)
		dbm[i] = ((double)elm_int[i])/4 -100; //dB format to dBm format
	#else
	if (!(strncmp(inf,"wlan0",5)))
	{
		igi=IGI_5G;
		base=BASE_5G;
	}
	else if (!(strncmp(inf,"wlan1",5)))
	{
		igi=IGI_2G;
		base=BASE_2G;
	}
	
	
	for (i=0;i<32;i++)
		dbm[i]=igi-110+20*(log(elm_int[i])-log(base));
	#endif	
	return 0;
		
}