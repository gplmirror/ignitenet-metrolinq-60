#!/bin/sh
devIdx=$1

wifi_type="`iw dev wlan${devIdx} info | grep type | awk '{print $2}'`"
result=""
if [ "$wifi_type" == "AP" ];then
    phyIdx="`iw dev wlan${devIdx} info | grep wiphy | awk '{print $2}'`"
    runtimeChannel=`iwpriv wlan${devIdx} get_mib channel | awk -F':' '{print $2}'`
    if [ $runtimeChannel -ge 52 -a $runtimeChannel -le 64 ] || [ $runtimeChannel -ge 100 -a $runtimeChannel -le 140 ]; then
      result="`iw phy${phyIdx} info  | sed ':label;N;s/(no IR, radar detection)\n/ /;b label' | grep ${runtimeChannel} | awk '{print $9}' | tr -d '\n'`"
    fi
fi
echo -n $result
