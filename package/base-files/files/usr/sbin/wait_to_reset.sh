#!/bin.sh
i=1
rm -f /var/reset.lock

while [ "$i" -le 5 ]
do
        sleep 1
        [ -f /var/reset.lock ] && exit 0
        i=`expr $i + 1`
done


echo "FACTORY RESET" > /dev/console
jffs2reset -y && reboot &
