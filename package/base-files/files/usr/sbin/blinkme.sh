#!/bin/sh

RSSI_0FF=0
RSSI_ON=255

WIFI_OFF=0
WIFI0_ON=255
WIFI1_ON=255

SKY=0

count=5

[ -n "$1" ] && count="$1"

eval "local $(acc hw get product_name)"
case "$product_name" in
	"SS-N300"*|EAP1282*|"SP-"*)
		WIFI0_ON=0
		RSSI_ON=0
		;;
	OAP7235RL*|OAP1235RL*|OAP1232RL*)
		SKY=1
		WIFI1_ON=0
		;;
	*)
		exit 0
		;;
esac


/etc/init.d/rssi stop

for i in `seq 1 $count`; do
	echo $RSSI_0FF > /sys/class/leds/rssi0/brightness
	echo $WIFI_OFF > /sys/class/leds/wlan0/brightness
	echo $WIFI_OFF > /sys/class/leds/wlan1/brightness
	if [ $SKY == 1 ]; then
		echo $RSSI_0FF > /sys/class/leds/rssi1/brightness
		echo $RSSI_0FF > /sys/class/leds/rssi2/brightness
		echo $RSSI_0FF > /sys/class/leds/rssi3/brightness
	fi

	sleep 1;
	echo $RSSI_ON > /sys/class/leds/rssi0/brightness
	echo $WIFI0_ON > /sys/class/leds/wlan0/brightness
	echo $WIFI1_ON > /sys/class/leds/wlan1/brightness
	if [ $SKY == 1 ]; then
		echo $RSSI_0N > /sys/class/leds/rssi1/brightness
		echo $RSSI_0N > /sys/class/leds/rssi2/brightness
		echo $RSSI_0N > /sys/class/leds/rssi3/brightness
	fi
	sleep 1;
done

/etc/init.d/rssi start

exit 0

