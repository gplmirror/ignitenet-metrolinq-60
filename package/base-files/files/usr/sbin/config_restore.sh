#!/bin/sh

target=$1
key=$2

[ -z "$key" ] && key=ACN

ccrypt -K "${key}" -c ${target} > ${target}.tar.gz

# check configuration
if [ ! -s "${target}.tar.gz" ]; then
	rm -f ${target} ${target}.tar.gz
    return 1
fi

tar -zxf ${target}.tar.gz -C / 2>/dev/null
rm -f ${target} ${target}.tar.gz
return 0