#!/bin/sh

TROUBLE_DIR=/tmp/trouble
TROUBLE_FILE=$1
TMP_CONFIG_DIR="$TROUBLE_DIR/etc/config"
TMP_SET_PWD_FILE="$TMP_CONFIG_DIR/set_pwd.sh"
TROUBLE_WIRELESS_DIR="$TROUBLE_DIR/wireless"

replace_pwd() {
    section=$1
    replace_item=$2

    if [ "$section" == "wireless" -a \( "$replace_item" == "key" -o "$replace_item" == "password" -o "$replace_item" == "encryption_60g_key" \) ]; then
        for j in 0 1; do
            for i in 1 2 3 4 5 6 7 8; do
                cfg="`uci get wireless.radio${j}_${i}.${replace_item}`"
                if [ "$cfg" != "" ]; then
                    current_item_val_length=${#cfg}
                    cnt=1
                    new_str=""
                    while [ $cnt -le $current_item_val_length ]; do
                        new_str=$new_str"*"
                        cnt=`expr $cnt + 1`
                    done
                    echo "uci set wireless.radio${j}_${i}.${replace_item}=$new_str -c $TMP_CONFIG_DIR" >> $TMP_SET_PWD_FILE
                fi
            done
        done
    else   
        uci show $section -c $TMP_CONFIG_DIR | grep $replace_item | awk -F"=" '{print $1}' | grep $replace_item |awk -F"=" '{print "uci set "$1"=* -c '$TMP_CONFIG_DIR'"}' >> $TMP_SET_PWD_FILE
    fi
    echo "uci commit $section -c $TMP_CONFIG_DIR" >> $TMP_SET_PWD_FILE
}

mask_pwd()
{
	key=$1
	val=$(uci get -c $TMP_CONFIG_DIR $1 2> /dev/null)
	[ -z "$val" ] && return

	# Number of characters to be visible / leftover is stripped/masked
	len=$((`echo $val | wc -m` / 4))
	new=$(echo $val | cut -c -$len)$(printf "%"$len"s" | tr \  \*)
	uci -c $TMP_CONFIG_DIR set $1=$new 2> /dev/null
	uci -c $TMP_CONFIG_DIR commit `echo $key | cut -f 1 -d "."`
}

rm $TROUBLE_FILE 2> /dev/null

mkdir $TROUBLE_DIR
mkdir -p $TROUBLE_DIR/var/state
mkdir -p $TMP_CONFIG_DIR
mkdir -p $TROUBLE_WIRELESS_DIR
mkdir -p $TROUBLE_DIR/ethernet_ports
logread > $TROUBLE_DIR/logread

cp -a /etc/config/* $TMP_CONFIG_DIR/
cp -a /var/state/* $TROUBLE_DIR/var/state/
cp -a  /var/run/* $TROUBLE_DIR/var/run
cat /proc/wlan0/stats > $TROUBLE_WIRELESS_DIR/wlan0_stats 2> /dev/null
cat /proc/wlan1/stats > $TROUBLE_WIRELESS_DIR/wlan1_stats 2> /dev/null
iwlist wlan0 scan > $TROUBLE_WIRELESS_DIR/wlan0_scan 2> /dev/null
iwlist wlan1 scan > $TROUBLE_WIRELESS_DIR/wlan1_scan 2> /dev/null
cat /proc/wlan0/mib_all > $TROUBLE_WIRELESS_DIR/wlan0_mib_all 2> /dev/null
cat /proc/wlan1/mib_all > $TROUBLE_WIRELESS_DIR/wlan1_mib_all 2> /dev/null
cat /var/run/hostapd-phy0.conf > $TROUBLE_WIRELESS_DIR/hostapd-phy0.conf 2> /dev/null
cat /var/run/hostapd-phy1.conf > $TROUBLE_WIRELESS_DIR/hostapd-phy1.conf 2> /dev/null
wifi_sta_info_file=`ls /proc/wlan* -d | sed 's/$/&\/sta_info/g'`
for i in $wifi_sta_info_file; do
    file_name=`echo $i | sed 's/\//_/g'`
    cp -af $i "$TROUBLE_WIRELESS_DIR/$file_name"
done

wifi_mac_file=`ls /sys/devices/virtual/RTKWiFi*/RTKWiFi*/net/wlan*/address -d`
for i in $wifi_mac_file; do
    file_name=`echo $i | sed 's/\//_/g'`
    cp -af $i "$TROUBLE_WIRELESS_DIR/$file_name"
done

wifi_connect_status_file=`ls /sys/devices/virtual/RTKWiFi*/RTKWiFi*/net/wlan*/operstate -d`
for i in $wifi_connect_status_file; do
    file_name=`echo $i | sed 's/\//_/g'`
    cp -af $i "$TROUBLE_WIRELESS_DIR/$file_name"
done

prswifi_mac_file=`ls /sys/devices/platform/xhci-hcd/usb2/2-1/net/wlan*/address -d`
for i in $prswifi_mac_file; do
    file_name=`echo $i | sed 's/\//_/g'`
    cp -af $i "$TROUBLE_WIRELESS_DIR/$file_name"
done

prswifi_connect_status_file=`ls /sys/devices/platform/xhci-hcd/usb2/2-1/net/wlan*/operstate -d`
for i in $prswifi_connect_status_file; do
    file_name=`echo $i | sed 's/\//_/g'`
    cp -af $i "$TROUBLE_WIRELESS_DIR/$file_name"
done

prs_serial status > $TROUBLE_WIRELESS_DIR/prs_serial_status 2> /dev/null

trouble_rssi_60g_file=$TROUBLE_WIRELESS_DIR/rssi_60g
rssi_file="/sys/devices/platform/xhci-hcd/usb2/2-1/prs_attrs/rx_rssi"
remote_rssi_file="/sys/devices/platform/xhci-hcd/usb2/2-1/mib/extMac/staInfo/1/LastRemoteRssi"
rm -f $trouble_rssi_60g_file
if [ -f $rssi_file ]; then
    echo -n "rssi:" >> $trouble_rssi_60g_file
    cat $rssi_file >> $trouble_rssi_60g_file 2> /dev/null
fi
if [ -f $remote_rssi_file ]; then
    remote_rssi=`cat $remote_rssi_file`
    remote_rssi=`expr $remote_rssi - 256`
    echo "remote rssi:$remote_rssi" >> $trouble_rssi_60g_file
fi

iw dev wlan1 scan trigger 2> /dev/null
iw dev wlan1 station dump > $TROUBLE_DIR/iw_dev_station_dump 2> /dev/null
iwpriv wlan0 reg_dump
iwpriv wlan1 reg_dump
dmesg > $TROUBLE_DIR/dmesg

acc hw all > $TROUBLE_DIR/info
echo -n "firmware version=" >> $TROUBLE_DIR/info
cat /etc/version >> $TROUBLE_DIR/info
echo -n "loadavg=" >> $TROUBLE_DIR/info
cat /proc/loadavg >> $TROUBLE_DIR/info
echo -n "system uptime=" >> $TROUBLE_DIR/info
cat /proc/uptime| awk -F. '{run_days=$1 / 86400;run_hour=($1 % 86400)/3600;run_minute=($1 % 3600)/60;run_second=$1 % 60;printf("%dday %dh %dmin %dsec\n",run_days,run_hour,run_minute,run_second)}' >> $TROUBLE_DIR/info
echo -n "modelname=" >> $TROUBLE_DIR/info
cat /var/run/modelname >> $TROUBLE_DIR/info
cat /etc/device_info >> $TROUBLE_DIR/info

echo "============= iptables --list-rules =============" > $TROUBLE_DIR/iptables
iptables --list-rules >> $TROUBLE_DIR/iptables
echo -e "\n============= iptables -L -nv =============" >> $TROUBLE_DIR/iptables
iptables -L -nv >> $TROUBLE_DIR/iptables
echo -e "\n============= iptables -t nat -L -nv =============" >> $TROUBLE_DIR/iptables
iptables -t nat -L -nv >> $TROUBLE_DIR/iptables
echo -e "\n============= iptables -t mangle -L -nv =============" >> $TROUBLE_DIR/iptables
iptables -t mangle -L -nv >> $TROUBLE_DIR/iptables

cat /proc/rtl865x/port_status > $TROUBLE_DIR/ethernet_ports/port_status
cat /proc/rtl865x/hs > $TROUBLE_DIR/ethernet_ports/hs
cat /proc/rtl865x/stats > $TROUBLE_DIR/ethernet_ports/stats
cat /proc/rtl865x/netif > $TROUBLE_DIR/ethernet_ports/netif
cat /proc/rtl865x/rx_limit > $TROUBLE_DIR/ethernet_ports/rx_limit

route -n > $TROUBLE_DIR/route

ip addr > $TROUBLE_DIR/ip_addr

ifconfig -a > $TROUBLE_DIR/ifconfig

iwconfig > $TROUBLE_DIR/iwconfig

brctl show > $TROUBLE_DIR/brctl

brctl showmacs br-wan  > $TROUBLE_DIR/brctl_showmacs

cat /proc/net/vlan/config >> $TROUBLE_DIR/vlan_config

cat /etc/migrate/migratever >> $TROUBLE_DIR/migratever

cat  /etc/usbwatchd_log.txt > $TROUBLE_DIR/usbwatchd_log.txt

cat /etc/rc.local >> $TROUBLE_DIR/rc.local

ls -l /etc/rc.d/ >> $TROUBLE_DIR/rc.d

cat /tmp/dhcp.leases >> $TROUBLE_DIR/dhcp.leases

cat /proc/meminfo > $TROUBLE_DIR/meminfo

cat /proc/cmdline > $TROUBLE_DIR/cmdline

dd if=/dev/mtd7 > $TROUBLE_DIR/prev_dmesg

ps -wwww > $TROUBLE_DIR/ps

cp -a /tmp/mgmtd_status $TROUBLE_DIR/mgmtd_status

cat /proc/mounts > $TROUBLE_DIR/mounts

if [ -d /mnt/media ]; then
    ls -laR /mnt/media > $TROUBLE_DIR/media_info
    if [ -f /mnt/media/schedule.json ]; then
        cat "/mnt/media/schedule.json" > $TROUBLE_DIR/media_schedule.json
    fi
else
    echo "There is no media folder." > $TROUBLE_DIR/media_info
fi

[ -f "/usr/bin/lsusb" ] && /usr/bin/lsusb > $TROUBLE_DIR/lsusb

cp -a /tmp/state $TROUBLE_DIR/
cp -a /sys/fs/pstore $TROUBLE_DIR/ramoops_logs

[ -f "/usr/bin/iwinfo" ] && {
  echo "============= iwinfo =============" > $TROUBLE_WIRELESS_DIR/iwinfo
  iwinfo >> $TROUBLE_WIRELESS_DIR/iwinfo
  echo -e "\n============= iwinfo wlan1 assoclist =============" >> $TROUBLE_WIRELESS_DIR/iwinfo
  iwinfo wlan1 assoclist >> $TROUBLE_WIRELESS_DIR/iwinfo
  echo -e "\n============= iwinfo wlan1 txpowerlist =============" >> $TROUBLE_WIRELESS_DIR/iwinfo
  iwinfo wlan1 txpowerlist >> $TROUBLE_WIRELESS_DIR/iwinfo
  echo -e "\n============= iwinfo wlan1 freqlist =============" >> $TROUBLE_WIRELESS_DIR/iwinfo
  iwinfo wlan1 freqlist >> $TROUBLE_WIRELESS_DIR/iwinfo
  echo -e "\n============= iwinfo wlan1 countrylist =============" >> $TROUBLE_WIRELESS_DIR/iwinfo
  iwinfo wlan1 countrylist >> $TROUBLE_WIRELESS_DIR/iwinfo
}

# Replace passwords with * in the configuation files
mask_pwd "acn.register.pass"
replace_pwd "users"    "passwd"             2> /dev/null
replace_pwd "network"  "password"           2> /dev/null
replace_pwd "hotspot"  "hs_shared"          2> /dev/null
replace_pwd "hotspot"  "hs_portal_secret"   2> /dev/null
replace_pwd "wireless" "key"                2> /dev/null
replace_pwd "wireless" "auth_secret"        2> /dev/null
replace_pwd "wireless" "acct_secret"        2> /dev/null
replace_pwd "wireless" "password"           2> /dev/null
replace_pwd "wireless" "encryption_60g_key" 2> /dev/null
[ -f $TMP_SET_PWD_FILE ] && {
    chmod 777 $TMP_SET_PWD_FILE 2> /dev/null
    $TMP_SET_PWD_FILE 2> /dev/null
    rm -f $TMP_SET_PWD_FILE
}

# release information
[ -f "/etc/release" ] && cat /etc/release > $TROUBLE_DIR/release || echo "There is no release information."  > $TROUBLE_DIR/release

#tar file
tar -cz -C /tmp/ -f $TROUBLE_FILE trouble

rm -rf $TROUBLE_DIR
