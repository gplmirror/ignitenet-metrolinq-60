stop_ra_process() {
	/etc/init.d/lighttpd stop
	/etc/init.d/dropbear stop
	/etc/init.d/telnet stop
	/etc/init.d/mgmt stop
	/etc/init.d/mini_snmpd stop
	/etc/init.d/rssi stop
	killall lighttpd dropbear telnetd mgmtd
}

append sysupgrade_pre_upgrade stop_ra_process

stop_wireless() {
	# shut down wifi to avoid flash programing interference
	wifi down
	rmmod prs_falcon
	wifi unload
}

append sysupgrade_pre_upgrade stop_wireless

drop_caches() {
	# Force free all kernel caches
	echo 3 > /proc/sys/vm/drop_caches
}

append sysupgrade_pre_upgrade drop_caches
