#!/bin/sh

uci del acn.register.login 2> /dev/null
uci del acn.register.pass 2> /dev/null
uci del acn.register.host 2> /dev/null
uci set acn.register.state=0

uci commit acn.register