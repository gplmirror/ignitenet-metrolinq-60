#!/bin/sh
#
# need to run with -y switch

format_media() {
	return 0
}

. /lib/functions.sh
include /lib/upgrade


[ x"$1" != x"-y" ] && return

trap '' HUP
stop_ra_process

/sbin/jffs2reset $@ || return
format_media

/sbin/reboot -f

sleep 5
echo b 2>/dev/null >/proc/sysrq-trigger
