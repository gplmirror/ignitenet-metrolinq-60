#!/bin/sh

cat /sys/kernel/debug/usb/devices | grep -qs usb-storage || {
	echo "No USB device with usb-storage driver."
	exit 1
}

cat /proc/mounts | grep -qs mnt && {
	echo "/mnt is already mounted. umount it now and then mount again."
	umount /mnt
}

mount /dev/sda /mnt >/dev/null 2>&1 || mount /dev/sda1 /mnt >/dev/null 2>&1 || {
	echo "Can't mount usb device."
	exit 1
}

echo "Write Data to USB device ......"
dd count=2048 bs=4k if=/dev/urandom of=/mnt/testfile
if [ $? = 0 ];then
        echo "USB write test OK"
else
        echo "USB write test Fail"
		exit 1
fi

echo "Read Data from USB device ......"
[ -f /mnt/testfile ] || dd count=2048 bs=4k if=/dev/urandom of=/mnt/testfile
cp -f /mnt/testfile /tmp/testfile
if [ $? = 0 ];then
        echo "USB read test OK"
else
        echo "USB read test Fail"
		exit 1
fi

[ -f /mnt/testfile ] && rm -f /mnt/testfile
[ -f /tmp/testfile ] && rm -f /tmp/testfile
umount /mnt
exit 0
