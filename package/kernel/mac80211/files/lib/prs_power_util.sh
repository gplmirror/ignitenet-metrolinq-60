#!/bin/sh

prs_power_init() {
	prs_power_on
	sleep 2
	# wait for 60G usb dongle detect & init done
	wig_exist=0
	i=0
	while [ "${i}" != "5" ]
	do
		# will have wrong check if only check ttyACMx exist or not and then cause prs_flash hang
		# because for NXP LEDE code base, if dongle image be erased. it still can create ttyACM0
		# but in different path(USB 2.0 speed only)
		#find /sys/devices/ -name ttyACM*| grep -q ttyACM && {
		[ -d "/sys/devices/platform/xhci-hcd/usb2/2-1" ] && {
			wig_exist=1
			break
		}
		i=$(($i+1))
		echo ">>> USB3 dongle not ready yet, waiting...$i" >/dev/console
		sleep 1
	done
	if [ $wig_exist = 1 ]; then
		tty_ports=`find /sys/devices/ -name ttyACM*|sed 's/.*ttyACM/device/g'`
		for tty_port in $tty_ports; do
			/usr/sbin/prs_flash $tty_port >/dev/console
		done
		sleep 1
	else
		# check if ttyACM0 exist or not? if yes, means something wrong on dongle image. download image again
		echo ">>> Cannot find dongle information, image seems corrupted... " >/dev/console
		if [ -e /dev/ttyACM0 ]; then
			echo ">>> Reflash dongle image..., it may take 1~2 minutes." >/dev/console
			cd /etc/prs_fw/sparrow
			prs_download_flash
			cd -
			sleep 1
		fi
	fi
}

prs_power_on() {
	echo 0 > /sys/devices/virtual/gpio/gpio60/value
}

prs_power_off() {
	echo 1 > /sys/devices/virtual/gpio/gpio60/value
}
