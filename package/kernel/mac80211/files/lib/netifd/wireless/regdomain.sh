rtk_set_regdomain() {
    local phys="$1"
    local country="$2"
    local dev_name=`echo wlan${phys#phy}`

    iwpriv $dev_name set_mib disable_txpwrlmt=0

    case "$country" in
        "JP" ) #MKK
            iwpriv $dev_name set_mib regdomain=6
            iwpriv $dev_name set_mib txpwr_lmt_index=4
            product_name=`acc hw all |grep product_name |sed 's/^.*=//g'`
            case "$product_name" in
                OAP123*|EAP1282*|FN* ) #8812 products
                    if [ "$hwmode" == "a" ]; then
                        iwpriv $dev_name set_mib adaptivity_enable=1
                        iwpriv $dev_name set_mib TH_L2H_ini=149
                    fi
                ;;
            esac
            ;;
        "US" | "AR" | "BR" | "CA" | "CO" | "CR" | "EC" | \
        "GU" | "HN" | "LB" | "MX" | "NZ" | "PE" | "PH" | \
        "PR" | "SG" | "TH" | "TT" | "AE" | "UZ" | "VN" | \
        "DO" | "SV" | "GT" | "MY" | "PA" | "VE" | "TW" )    #FCC
            iwpriv $dev_name set_mib regdomain=1
            iwpriv $dev_name set_mib txpwr_lmt_index=4
            ;;
        "OT" ) #Other
            iwpriv $dev_name set_mib regdomain=16
            iwpriv $dev_name set_mib txpwr_lmt_index=4
            ;;
        *)
            iwpriv $dev_name set_mib regdomain=3
            iwpriv $dev_name set_mib txpwr_lmt_index=4
            ;;
    esac
}
