#!/bin/sh
. /lib/netifd/netifd-wireless.sh
. /lib/netifd/hostapd.sh
. /lib/netifd/wireless/regdomain.sh
. /lib/netifd/wireless/forward.sh

init_wireless_driver "$@"

MP_CONFIG_INT="mesh_retry_timeout mesh_confirm_timeout mesh_holding_timeout mesh_max_peer_links
	       mesh_max_retries mesh_ttl mesh_element_ttl mesh_hwmp_max_preq_retries
	       mesh_path_refresh_time mesh_min_discovery_timeout mesh_hwmp_active_path_timeout
	       mesh_hwmp_preq_min_interval mesh_hwmp_net_diameter_traversal_time mesh_hwmp_rootmode
	       mesh_hwmp_rann_interval mesh_gate_announcements mesh_sync_offset_max_neighor
	       mesh_rssi_threshold mesh_hwmp_active_path_to_root_timeout mesh_hwmp_root_interval
	       mesh_hwmp_confirmation_interval mesh_awake_window mesh_plink_timeout"
MP_CONFIG_BOOL="mesh_auto_open_plinks mesh_fwding"
MP_CONFIG_STRING="mesh_power_mode"

drv_mac80211_init_device_config() {
	hostapd_common_add_device_config

	config_add_string path phy 'macaddr:macaddr'
	config_add_string hwmode
	config_add_int beacon_int chanbw frag rts
	config_add_int rxantenna txantenna antenna_gain txpower distance
	config_add_boolean noscan
	config_add_array ht_capab
	config_add_boolean \
		rxldpc \
		short_gi_80 \
		short_gi_160 \
		tx_stbc_2by1 \
		su_beamformer \
		su_beamformee \
		mu_beamformer \
		mu_beamformee \
		vht_txop_ps \
		htc_vht \
		rx_antenna_pattern \
		tx_antenna_pattern
	config_add_int vht_max_a_mpdu_len_exp vht_max_mpdu vht_link_adapt vht160 rx_stbc tx_stbc
	config_add_boolean \
		ldpc \
		greenfield \
		short_gi_20 \
		short_gi_40 \
		dsss_cck_40
	config_add_int max_sta_assoc
	config_add_int ack sgi stbc mpdu dfs tx_streams rx_streams amsdu_802_3
	config_add_string data_rate
	config_add_int ptp
	config_add_int backup_enable

	# 60G auto mode prefer pcp
	config_add_int prefer_pcp

	config_add_int client_isolation
}

drv_mac80211_init_iface_config() {
	hostapd_common_add_bss_config

	config_add_string 'macaddr:macaddr' ifname

	config_add_boolean wds powersave
	config_add_int maxassoc
	config_add_int max_listen_int
	config_add_int dtim_period

	# mesh
	config_add_string mesh_id
	config_add_int $MP_CONFIG_INT
	config_add_boolean $MP_CONFIG_BOOL
	config_add_string $MP_CONFIG_STRING

	# minimal station signal
	config_add_int min_signal_allowed

	# band steering pair, this setting is created by netifd
	config_add_int is_bs_pair

	# wmm parameters
	config_add_int wmm_enable wmm_power
	config_add_string wmm_bk wmm_be wmm_vi wmm_vo

	# client isolation
	config_add_int client_isolation	

	# ACL
	config_add_int acl_enable
	config_add_string acl_select acl_mac_list

	# 60G encryption
	config_add_string encryption_60g encryption_60g_key
	config_add_int linkid_enable
}

mac80211_add_capabilities() {
	local __var="$1"; shift
	local __mask="$1"; shift
	local __out= oifs

	oifs="$IFS"
	IFS=:
	for capab in "$@"; do
		set -- $capab

		[ "$(($4))" -gt 0 ] || continue
		[ "$(($__mask & $2))" -eq "$((${3:-$2}))" ] || continue
		__out="$__out[$1]"
	done
	IFS="$oifs"

	export -n -- "$__var=$__out"
}

mac80211_hostapd_setup_base() {
	local phy="$1"

	json_select config

	#[ "$auto_channel" -gt 0 ] && channel=acs_survey
	[ "$auto_channel" -gt 0 ] && { 
			case "$hwmode" in
				*a*)
				    if [ -n "$(iw phy phy0 info |grep 5180|grep -v disabled)" ]; then
					channel=36 
				    elif [ -n "$(iw phy phy0 info |grep 5745|grep -v disabled)" ]; then
					channel=149
				    else
					channel=acs_survey
				    fi
				;;
				*)
					channel=1 
				;;
			esac
	}

	json_get_vars noscan htmode
	json_get_values ht_capab_list ht_capab

	ieee80211n=1
	[ -n "$enable_ht" ] && {
		case "$htmode" in
			VHT*) ;;
			*)
				if [ "$enable_ht" == "0" ]; then
					ieee80211n=
				fi
			;;
		esac
	}
	ht_capab=
	case "$htmode" in
		VHT20|HT20) ;;
		HT40*|VHT40|VHT80|VHT160)
			case "$hwmode" in
				a)
					case "$(( ($channel / 4) % 2 ))" in
						1) ht_capab="[HT40+]";;
						0) ht_capab="[HT40-]";;
					esac
				;;
				*)
					case "$htmode" in
						HT40+) ht_capab="[HT40+]";;
						HT40-) ht_capab="[HT40-]";;
						*)
							if [ "$channel" -lt 7 ]; then
								ht_capab="[HT40+]"
							else
								ht_capab="[HT40-]"
							fi
						;;
					esac
				;;
			esac
			[ "$auto_channel" -gt 0 ] && ht_capab="[HT40+]"
		;;
		*) ieee80211n= ;;
	esac

	[ -n "$ieee80211n" ] && {
		append base_cfg "ieee80211n=1" "$N"

		json_get_vars \
			ldpc:1 \
			greenfield:0 \
			short_gi_20:1 \
			short_gi_40:1 \
			tx_stbc:1 \
			rx_stbc:3 \
			dsss_cck_40:1

		ht_cap_mask=0
		for cap in $(iw phy "$phy" info | grep 'Capabilities:' | cut -d: -f2); do
			ht_cap_mask="$(($ht_cap_mask | $cap))"
		done

		cap_rx_stbc=$((($ht_cap_mask >> 8) & 3))
		[ "$rx_stbc" -lt "$cap_rx_stbc" ] && cap_rx_stbc="$rx_stbc"
		ht_cap_mask="$(( ($ht_cap_mask & ~(0x300)) | ($cap_rx_stbc << 8) ))"

		mac80211_add_capabilities ht_capab_flags $ht_cap_mask \
			LDPC:0x1::$ldpc \
			GF:0x10::$greenfield \
			SHORT-GI-20:0x20::$sgi \
			SHORT-GI-40:0x40::$sgi \
			TX-STBC:0x80::$tx_stbc \
			RX-STBC1:0x300:0x100:1 \
			RX-STBC12:0x300:0x200:1 \
			RX-STBC123:0x300:0x300:1 \
			DSSS_CCK-40:0x1000::$dsss_cck_40

		ht_capab="$ht_capab$ht_capab_flags"
		[ -n "$ht_capab" ] && append base_cfg "ht_capab=$ht_capab" "$N"
	}

	# 802.11ac
	enable_ac=0
	idx="$channel"
	case "$htmode" in
		VHT20) enable_ac=1;;
		VHT40)
			case "$(( ($channel / 4) % 2 ))" in
				1) idx=$(($channel + 2));;
				0) idx=$(($channel - 2));;
			esac
			enable_ac=1
			append base_cfg "vht_oper_chwidth=0" "$N"
			append base_cfg "vht_oper_centr_freq_seg0_idx=$idx" "$N"
		;;
		VHT80)
			case "$(( ($channel / 4) % 4 ))" in
				1) idx=$(($channel + 6));;
				2) idx=$(($channel + 2));;
				3) idx=$(($channel - 2));;
				0) idx=$(($channel - 6));;
			esac
			enable_ac=1
			append base_cfg "vht_oper_chwidth=1" "$N"
			append base_cfg "vht_oper_centr_freq_seg0_idx=$idx" "$N"
		;;
		VHT160)
			case "$channel" in
				36|40|44|48|52|56|60|64) idx=50;;
				100|104|108|112|116|120|124|128) idx=114;;
			esac
			enable_ac=1
			append base_cfg "vht_oper_chwidth=2" "$N"
			append base_cfg "vht_oper_centr_freq_seg0_idx=$idx" "$N"
		;;
	esac

	if [ "$enable_ac" != "0" ]; then
		json_get_vars \
			rxldpc:1 \
			short_gi_80:1 \
			short_gi_160:1 \
			tx_stbc_2by1:1 \
			su_beamformer:1 \
			su_beamformee:1 \
			mu_beamformer:1 \
			mu_beamformee:1 \
			vht_txop_ps:1 \
			htc_vht:1 \
			rx_antenna_pattern:1 \
			tx_antenna_pattern:1 \
			vht_max_a_mpdu_len_exp:7 \
			vht_max_mpdu:11454 \
			rx_stbc:4 \
			tx_stbc:4 \
			vht_link_adapt:3 \
			vht160:2

		append base_cfg "ieee80211ac=1" "$N"
		vht_cap=0
		for cap in $(iw phy "$phy" info | awk -F "[()]" '/VHT Capabilities/ { print $2 }'); do
			vht_cap="$(($vht_cap | $cap))"
		done

		cap_rx_stbc=$((($vht_cap >> 8) & 7))
		[ "$rx_stbc" -lt "$cap_rx_stbc" ] && cap_rx_stbc="$rx_stbc"
		ht_cap_mask="$(( ($vht_cap & ~(0x700)) | ($cap_rx_stbc << 8) ))"

		mac80211_add_capabilities vht_capab $vht_cap \
			RXLDPC:0x10::$rxldpc \
			SHORT-GI-80:0x20::$short_gi_80 \
			SHORT-GI-160:0x40::$short_gi_160 \
			TX-STBC-2BY1:0x80::$tx_stbc \
			SU-BEAMFORMER:0x800::$su_beamformer \
			SU-BEAMFORMEE:0x1000::$su_beamformee \
			MU-BEAMFORMER:0x80000::$mu_beamformer \
			MU-BEAMFORMEE:0x100000::$mu_beamformee \
			VHT-TXOP-PS:0x200000::$vht_txop_ps \
			HTC-VHT:0x400000::$htc_vht \
			RX-ANTENNA-PATTERN:0x10000000::$rx_antenna_pattern \
			TX-ANTENNA-PATTERN:0x20000000::$tx_antenna_pattern \
			RX-STBC1:0x700:0x100:1 \
			RX-STBC12:0x700:0x200:1 \
			RX-STBC123:0x700:0x300:1 \
			RX-STBC1234:0x700:0x400:1 \

		# supported Channel widths
		vht160_hw=0
		[ "$(($vht_cap & 12))" -eq 4 -a 1 -le "$vht160" ] && \
			vht160_hw=1
		[ "$(($vht_cap & 12))" -eq 8 -a 2 -le "$vht160" ] && \
			vht160_hw=2
		[ "$vht160_hw" = 1 ] && vht_capab="$vht_capab[VHT160]"
		[ "$vht160_hw" = 2 ] && vht_capab="$vht_capab[VHT160-80PLUS80]"

		# maximum MPDU length
		vht_max_mpdu_hw=3895
		[ "$(($vht_cap & 3))" -ge 1 -a 7991 -le "$vht_max_mpdu" ] && \
			vht_max_mpdu_hw=7991
		[ "$(($vht_cap & 3))" -ge 2 -a 11454 -le "$vht_max_mpdu" ] && \
			vht_max_mpdu_hw=11454
		[ "$vht_max_mpdu_hw" != 3895 ] && \
			vht_capab="$vht_capab[MAX-MPDU-$vht_max_mpdu_hw]"
			
		# maximum A-MPDU length exponent
		vht_max_a_mpdu_len_exp_hw=0
		[ "$(($vht_cap & 58720256))" -ge 8388608 -a 1 -le "$vht_max_a_mpdu_len_exp" ] && \
			vht_max_a_mpdu_len_exp_hw=1
		[ "$(($vht_cap & 58720256))" -ge 16777216 -a 2 -le "$vht_max_a_mpdu_len_exp" ] && \
			vht_max_a_mpdu_len_exp_hw=2
		[ "$(($vht_cap & 58720256))" -ge 25165824 -a 3 -le "$vht_max_a_mpdu_len_exp" ] && \
			vht_max_a_mpdu_len_exp_hw=3
		[ "$(($vht_cap & 58720256))" -ge 33554432 -a 4 -le "$vht_max_a_mpdu_len_exp" ] && \
			vht_max_a_mpdu_len_exp_hw=4
		[ "$(($vht_cap & 58720256))" -ge 41943040 -a 5 -le "$vht_max_a_mpdu_len_exp" ] && \
			vht_max_a_mpdu_len_exp_hw=5
		[ "$(($vht_cap & 58720256))" -ge 50331648 -a 6 -le "$vht_max_a_mpdu_len_exp" ] && \
			vht_max_a_mpdu_len_exp_hw=6
		[ "$(($vht_cap & 58720256))" -ge 58720256 -a 7 -le "$vht_max_a_mpdu_len_exp" ] && \
			vht_max_a_mpdu_len_exp_hw=7
		vht_capab="$vht_capab[MAX-A-MPDU-LEN-EXP$vht_max_a_mpdu_len_exp_hw]"

		# whether or not the STA supports link adaptation using VHT variant
		vht_link_adapt_hw=0
		[ "$(($vht_cap & 201326592))" -ge 134217728 -a 2 -le "$vht_link_adapt" ] && \
			vht_link_adapt_hw=2
		[ "$(($vht_cap & 201326592))" -ge 201326592 -a 3 -le "$vht_link_adapt" ] && \
			vht_link_adapt_hw=3
		[ "$vht_link_adapt_hw" != 0 ] && \
			vht_capab="$vht_capab[VHT-LINK-ADAPT-$vht_link_adapt_hw]"

		[ -n "$vht_capab" ] && append base_cfg "vht_capab=$vht_capab" "$N"
	fi

	hostapd_prepare_device_config "$hostapd_conf_file" nl80211
	cat >> "$hostapd_conf_file" <<EOF
${channel:+channel=$channel}
${noscan:+noscan=$noscan}
$base_cfg

EOF
	json_select ..
}

mac80211_hostapd_setup_bss() {
	local phy="$1"
	local ifname="$2"
	local macaddr="$3"
	local type="$4"

	hostapd_cfg=
	append hostapd_cfg "$type=$ifname" "$N"

	hostapd_set_bss_options hostapd_cfg "$vif" || return 1
	json_get_vars wds dtim_period max_listen_int

	set_default wds 0

	append hostapd_cfg "wds_sta=1" "$N"
	[ "$staidx" -gt 0 ] && append hostapd_cfg "start_disabled=1" "$N"

	cat >> /var/run/hostapd-$phy.conf <<EOF
$hostapd_cfg
bssid=$macaddr
${dtim_period:+dtim_period=$dtim_period}
${max_listen_int:+max_listen_interval=$max_listen_int}
EOF
}

mac80211_generate_hwmac() {
	local phy="$1"
	local id="${macidx:-0}"

	if [ ${phy} != "phy1" ]; then
		local mac_name="HW_WLAN0_WLAN_ADDR"
	else
		local mac_name="HW_WLAN1_WLAN_ADDR"
	fi
	
	if [ ${id} != "0" ]; then
		local mac_val="$(flash allhw | grep ${mac_name}${id}= | cut -d= -f 2 )"
	else
		local mac_val="$(flash allhw | grep ${mac_name}= | cut -d= -f 2 )"
	fi
	
	macidx=$(($id + 1))
	
	local mac_1=${mac_val:0:2}
	local mac_2=${mac_val:2:2}
	local mac_3=${mac_val:4:2}
	local mac_4=${mac_val:6:2}
	local mac_5=${mac_val:8:2}
	local mac_6=${mac_val:10:2}	

	printf "%s:%s:%s:%s:%s:%s" $mac_1 $mac_2 $mac_3 $mac_4 $mac_5 $mac_6
	return	
}

mac80211_generate_mac() {
	local phy="$1"
	local id="${macidx:-0}"

	local ref="$(cat /sys/class/ieee80211/${phy}/macaddress)"
	local mask="$(cat /sys/class/ieee80211/${phy}/address_mask)"

	[ "$mask" = "00:00:00:00:00:00" ] && mask="ff:ff:ff:ff:ff:ff";
	local oIFS="$IFS"; IFS=":"; set -- $mask; IFS="$oIFS"

	local mask1=$1
	local mask6=$6

	local oIFS="$IFS"; IFS=":"; set -- $ref; IFS="$oIFS"
 
	macidx=$(($id + 1))
	[ "$((0x$mask1))" -gt 0 ] && {
		b1="0x$1"
		[ "$id" -gt 0 ] && \
			b1=$(($b1 ^ ((($id - 1) << 2) | 0x2)))
		printf "%02x:%s:%s:%s:%s:%s" $b1 $2 $3 $4 $5 $6
		return
	}

	[ "$((0x$mask6))" -lt 255 ] && {
		printf "%s:%s:%s:%s:%s:%02x" $1 $2 $3 $4 $5 $(( 0x$6 ^ $id ))
		return
	}

	off2=$(( (0x$6 + $id) / 0x100 ))
	printf "%s:%s:%s:%s:%02x:%02x" \
		$1 $2 $3 $4 \
		$(( (0x$5 + $off2) % 0x100 )) \
		$(( (0x$6 + $id) % 0x100 ))
}

find_phy() {
	[ -n "$phy" -a -d /sys/class/ieee80211/$phy ] && return 0
	[ -n "$path" ] && {
		for phy in /sys/devices/$path/ieee80211/phy*; do
			[ -e "$phy" ] && {
				phy="${phy##*/}"
				return 0
			}
		done
	}
	[ -n "$macaddr" ] && {
		for phy in $(ls /sys/class/ieee80211 2>/dev/null); do
			grep -i -q "$macaddr" "/sys/class/ieee80211/${phy}/macaddress" && return 0
		done
	}
	return 1
}

mac80211_check_ap() {
	has_ap=1
}

set_rtk_wds_mib() {
	
	local wlname="$1"
	
	json_select_config
	
	json_get_vars \
	rtk_wds rtk_wds_num rtk_wds_privacy rtk_wds_wepkey rtk_wds_passphrase \
	rtk_wds_macaddr0 rtk_wds_macaddr1 rtk_wds_macaddr2 rtk_wds_macaddr3 \
	rtk_wds_macaddr4 rtk_wds_macaddr5 rtk_wds_macaddr6 rtk_wds_macaddr7

	echo "wds=$rtk_wds  wds_num=$rtk_wds_num  wds_mac=$rtk_wds_macaddr0" > /etc/debug_mib
	
	iwpriv "$wlname" set_mib wds_enable=0
	[ -n "$rtk_wds" ] && iwpriv "$wlname" set_mib wds_enable="$rtk_wds"
	
	iwpriv "$wlname" set_mib wds_num=0
	#[ -n "$rtk_wds_num" ] && iwpriv "$wlname" set_mib wds_num="$rtk_wds_num"

	iwpriv "$wlname" set_mib rtk_wds_privacy=0
	[ -n "$rtk_wds_privacy" ] && iwpriv "$wlname" set_mib wds_encrypt="$rtk_wds_privacy"

	iwpriv "$wlname" set_mib rtk_wds_wepkey=0
	[ -n "$rtk_wds_wepkey" ] && iwpriv "$wlname" set_mib wds_wepkey="$rtk_wds_wepkey"

	iwpriv "$wlname" set_mib rtk_wds_passphrase=0
	[ -n "$rtk_wds_passphrase" ] && iwpriv "$wlname" set_mib wds_passphrase="$rtk_wds_passphrase"

	[ -n "$rtk_wds_macaddr0" ] && {
		iwpriv "$wlname" set_mib wds_add="$rtk_wds_macaddr0"
	}
	
	[ -n "$rtk_wds_macaddr1" ] && {
		iwpriv "$wlname" set_mib wds_add="$rtk_wds_macaddr1"
	}
	
	[ -n "$rtk_wds_macaddr2" ] && {
		iwpriv "$wlname" set_mib wds_add="$rtk_wds_macaddr2"
	}
	
	[ -n "$rtk_wds_macaddr3" ] && {
		iwpriv "$wlname" set_mib wds_add="$rtk_wds_macaddr3"
	}
	
	[ -n "$rtk_wds_macaddr4" ] && {
		iwpriv "$wlname" set_mib wds_add="$rtk_wds_macaddr4"
	}
	
	[ -n "$rtk_wds_macaddr5" ] && {
		iwpriv "$wlname" set_mib wds_add="$rtk_wds_macaddr5"
	}
	
	[ -n "$rtk_wds_macaddr6" ] && {
		iwpriv "$wlname" set_mib wds_add="$rtk_wds_macaddr6"
	}
	
	[ -n "$rtk_wds_macaddr7" ] && {
		iwpriv "$wlname" set_mib wds_add="$rtk_wds_macaddr7"
	}
}

enable_rtk_wds_priv() {
	
	local wlname="$1"

	json_select config

	json_get_vars \
	rtk_wds rtk_wds_num \
	rtk_wds_macaddr0 rtk_wds_macaddr1 rtk_wds_macaddr2 rtk_wds_macaddr3 \
	rtk_wds_macaddr4 rtk_wds_macaddr5 rtk_wds_macaddr6 rtk_wds_macaddr7

	echo "wds=$rtk_wds  wds_num=$rtk_wds_num  wds_mac=$rtk_wds_macaddr0" > /etc/debug_enable
	
	[ -n "$rtk_wds_macaddr0" ] && {	
		ifconfig "$wlname"-wds0 up
		brctl addif br-lan "$wlname"-wds0
	}

	[ -n "$rtk_wds_macaddr1" ] && {
		ifconfig "$wlname"-wds1 up
		brctl addif br-lan "$wlname"-wds1
	}

	[ -n "$rtk_wds_macaddr2" ] && {	
		ifconfig "$wlname"-wds2 up
		brctl addif br-lan "$wlname"-wds2
	}

	[ -n "$rtk_wds_macaddr3" ] && {
		ifconfig "$wlname"-wds3 up
		brctl addif br-lan "$wlname"-wds3
	}
	
	[ -n "$rtk_wds_macaddr4" ] && {	
		ifconfig "$wlname"-wds4 up
		brctl addif br-lan "$wlname"-wds4
	}

	[ -n "$rtk_wds_macaddr5" ] && {
		ifconfig "$wlname"-wds5 up
		brctl addif br-lan "$wlname"-wds5
	}

	[ -n "$rtk_wds_macaddr6" ] && {	
		ifconfig "$wlname"-wds6 up
		brctl addif br-lan "$wlname"-wds6
	}

	[ -n "$rtk_wds_macaddr7" ] && {
		ifconfig "$wlname"-wds7 up
		brctl addif br-lan "$wlname"-wds7
	}

}

wmm_val()
{
	wmm_part=$1
	wmm_chunk=$2

	i=0
	for wmm_value in  ${wmm_chunk//,/ }; do

		[ $wmm_part == 'bk' ] && {
			[ $i == 0 ] && ac_bk_cwmin=$wmm_value
			[ $i == 1 ] && ac_bk_cwmax=$wmm_value
			[ $i == 2 ] && ac_bk_aifsn=$wmm_value
			[ $i == 3 ] && ac_bk_txop=$wmm_value
		}
		[ $wmm_part == 'be' ] && {
			[ $i == 0 ] && ac_be_cwmin=$wmm_value
			[ $i == 1 ] && ac_be_cwmax=$wmm_value
			[ $i == 2 ] && ac_be_aifsn=$wmm_value
			[ $i == 3 ] && ac_be_txop=$wmm_value
		}
		[ $wmm_part == 'vi' ] && {
			[ $i == 0 ] && ac_vi_cwmin=$wmm_value
			[ $i == 1 ] && ac_vi_cwmax=$wmm_value
			[ $i == 2 ] && ac_vi_aifsn=$wmm_value
			[ $i == 3 ] && ac_vi_txop=$wmm_value
		}
		[ $wmm_part=='vo' ] && {
			[ $i == 0 ] && ac_vo_cwmin=$wmm_value
			[ $i == 1 ] && ac_vo_cwmax=$wmm_value
			[ $i == 2 ] && ac_vo_aifsn=$wmm_value
			[ $i == 3 ] && ac_vo_txop=$wmm_value
		}

		i=$((i+1))
	done
}

get_wireless_link_id() {
	if [ -f /sys/class/net/eth0/address ]; then
		cat /sys/class/net/eth0/address | tr -d ':'
	fi
}

mac80211_prepare_vif() {
	json_select config

	json_get_vars ifname mode ssid wds powersave macaddr rtk_wds min_signal_allowed is_bs_pair max_sta_assoc maxassoc

	[ -n "$ifname" ] || ifname="wlan${phy#phy}${if_idx:+-$if_idx}"

	if_idx=$((${if_idx:-0} + 1))

	if [ ! -z "$rtk_wds" ] && [ "$rtk_wds" -gt 0 ]; then
		case "$ifname" in
			wlan0)
				#set_rtk_wds_mib "$ifname"
		;;
			wlan1)
				#set_rtk_wds_mib "$ifname"
		;;
		esac
	fi

	set_default wds 0
	set_default powersave 0

	json_select ..

	[ -n "$macaddr" ] || {
		macaddr="$(mac80211_generate_mac $phy)"
		# breaks EAP as no flash info available
		#macaddr="$(mac80211_generate_hwmac $phy)"
		
		macidx="$(($macidx + 1))"
	}

	json_add_object data
	json_add_string ifname "$ifname"
	json_close_object
	json_select config

	set_default vif_txpower "$txpower"
	[ -z "$vif_txpower" ] || iw dev "$ifname" set txpower fixed "${vif_txpower%%.*}00"

	[ -n "$sgi" ] && {
		iwpriv $ifname set_mib shortGI20M=$sgi
		iwpriv $ifname set_mib shortGI40M=$sgi
	}
	[ -n "$stbc" ] && iwpriv $ifname set_mib stbc=$stbc

	# driver ACL
	set_rtk_acl "$ifname"

	# wmm parameters
	json_get_vars wmm_enable wmm_power wmm_bk wmm_be wmm_vi wmm_vo linkid_enable

	wmm_val "bk" $wmm_bk
	wmm_val "be" $wmm_be
	wmm_val "vi" $wmm_vi
	wmm_val "vo" $wmm_vo

	iwpriv $ifname set_mib ap_bkq_cwmin=${ac_bk_cwmin:-4}
	iwpriv $ifname set_mib ap_bkq_cwmax=${ac_bk_cwmax:-10}
	iwpriv $ifname set_mib ap_bkq_aifsn=${ac_bk_aifsn:-7}
	iwpriv $ifname set_mib ap_bkq_txoplimit=${ac_bk_txop:-0}

	iwpriv $ifname set_mib ap_beq_cwmin=${ac_be_cwmin:-4}
	iwpriv $ifname set_mib ap_beq_cwmax=${ac_be_cwmax:-10}
	iwpriv $ifname set_mib ap_beq_aifsn=${ac_be_aifsn:-3}
	iwpriv $ifname set_mib ap_beq_txoplimit=${ac_be_txop:-0}

	iwpriv $ifname set_mib ap_viq_cwmin=${ac_vi_cwmin:-3}
	iwpriv $ifname set_mib ap_viq_cwmax=${ac_vi_cwmax:-4}
	iwpriv $ifname set_mib ap_viq_aifsn=${ac_vi_aifsn:-2}
	iwpriv $ifname set_mib ap_viq_txoplimit=${ac_vi_txop:-94}

	iwpriv $ifname set_mib ap_voq_cwmin=${ac_vo_cwmin:-2}
	iwpriv $ifname set_mib ap_voq_cwmax=${ac_vo_cwmax:-3}
	iwpriv $ifname set_mib ap_voq_aifsn=${ac_vo_aifsn:-2}
	iwpriv $ifname set_mib ap_voq_txoplimit=${ac_vo_txop:-47}
	
	# PtP fixed rate default value shall be 0(disable).
	iwpriv $ifname set_mib wds_ptp=0
	
	if [ "AA$wmm_enable" != "AA" ]; then
		iwpriv $ifname set_mib qos_enable=$wmm_enable
	else
		iwpriv $ifname set_mib qos_enable=0
	fi
	
	if [ "AA$wmm_power" != "AA" ]; then
		iwpriv $ifname set_mib apsd_enable=$wmm_power
	else
		iwpriv $ifname set_mib apsd_enable=0
	fi

	if [ "$mode" == "ap" ]; then
		json_get_vars client_isolation

		if [ "AA$client_isolation" != "AA" ]; then
			iwpriv $ifname set_mib block_relay=$client_isolation
		else
			iwpriv $ifname set_mib block_relay=0
		fi
	fi

	link_id=$(get_wireless_link_id)
	if [ "$linkid_enable" = "1" -a -n $link_id ]; then
		echo "$link_id" > /sys/class/net/$ifname/linkid
	fi

	# It is far easier to delete and create the desired interface
	case "$mode" in
		adhoc)
			iw phy "$phy" interface add "$ifname" type adhoc
		;;
		ap)
			iwpriv $ifname set_mib disable_DFS=0
			# Hostapd will handle recreating the interface and
			# subsequent virtual APs belonging to the same PHY
			if [ -n "$hostapd_ctrl" ]; then
				type=bss
			else
				type=interface
			fi

			[ "$min_signal_allowed" -ge "0" -a "$min_signal_allowed" -le "99" ] && {
				iwpriv $ifname set_mib min_signal=$min_signal_allowed
			}
			[ "$is_bs_pair" = "1" ] && {
				iwpriv $ifname set_mib bs_enable=1
			}
			# max STAs per radio
			[ ! -z "$max_sta_assoc" ] && [ "$max_sta_assoc" -gt "0" -a "$max_sta_assoc" -le "128" ] && {
				iwpriv $ifname set_mib max_sta_assoc=$max_sta_assoc
			}
			# max STAs per SSID
			[ ! -z "$maxassoc" ] && [ "$maxassoc" -ge "0" -a "$maxassoc" -le "128" ] && {
				iwpriv $ifname set_mib stanum=$maxassoc
			}
			mac80211_hostapd_setup_bss "$phy" "$ifname" "$macaddr" "$type" || return

			[ -n "$hostapd_ctrl" ] || {
				iw phy "$phy" interface add "$ifname" type managed
				hostapd_ctrl="${hostapd_ctrl:-/var/run/hostapd/$ifname}"
			}
			json_get_vars encryption
			json_get_vars key
			if [ "$encryption" == "none" ]; then
				iwpriv $ifname set_mib wds_encrypt=0		#no security
			elif [ "$encryption" == "wep-open" ]; then
				json_get_vars key1
				len=${#key1}
				if [ $len -gt 12 ]; then
					iwpriv $ifname set_mib wds_encrypt=5		#WEP128
				else	
					iwpriv $ifname set_mib wds_encrypt=1		#WEP64
				fi
				iwpriv $ifname set_mib wds_wepkey=$(prepare_key_wep "$key1")
			elif [ "$encryption" == "psk2+tkip" ] || [ "$encryption" == "psk+tkip" ]; then
				iwpriv $ifname set_mib wds_encrypt=2		#TKIP
				iwpriv $ifname set_mib wds_passphrase=$key
			elif [ "$encryption" == "psk2+tkip+ccmp" ] || [ "$encryption" == "psk+tkip+ccmp" ]; then
				iwpriv $ifname set_mib wds_encrypt=4		#CCMP
				iwpriv $ifname set_mib wds_passphrase=$key
			elif [ "$encryption" == "psk2+ccmp" ] || [ "$encryption" == "psk+ccmp" ] || [ "$encryption" == "psk2" ] || [ "$encryption" == "psk" ]; then
				iwpriv $ifname set_mib wds_encrypt=4		#AES(CCMP)
				iwpriv $ifname set_mib wds_passphrase=$key
			fi
			iwpriv $ifname set_mib wds_client_enable=0
			iwpriv $ifname set_mib wds_max_number=64

			ptp_ap_set_mib "$ptp" "$ifname"

			if [ -n "$dfs" ] && [ "$country" == "OT" ] && [ "$dfs" == "0" ]; then
				iwpriv $ifname set_mib CAC_enable=0
				iwpriv $ifname set_mib dfsdbgmode=1
			else
				iwpriv $ifname set_mib CAC_enable=1
			# Radar detection is too sensitive,ignore DFS detected event temporarily
				iwpriv $ifname set_mib dfsdbgmode=1
			fi
		;;
		mesh)
			json_get_vars key mesh_id
			if [ -n "$key" ]; then
				iw phy "$phy" interface add "$ifname" type mp
			else
				iw phy "$phy" interface add "$ifname" type mp mesh_id "$mesh_id"
			fi
		;;
		monitor)
			iw phy "$phy" interface add "$ifname" type monitor
		;;
		sta)
			local wdsflag=
			staidx="$(($staidx + 1))"
			#[ "$wds" -gt 0 ] && wdsflag="4addr on"
			iw phy "$phy" interface add "$ifname" type managed $wdsflag
			[ "$powersave" -gt 0 ] && powersave="on" || powersave="off"
			iw "$ifname" set power_save "$powersave"
			ptp_clientwds_set_mib "$ptp" "$ifname"
		;;
	esac

	case "$mode" in
		monitor|mesh)
			[ "$auto_channel" -gt 0 ] || iw dev "$ifname" set channel "$channel" $htmode
		;;
	esac

	if [ "$mode" != "ap" ]; then
		# ALL ap functionality will be passed to hostapd
		# All interfaces must have unique mac addresses
		# which can either be explicitly set in the device
		# section, or automatically generated
		ifconfig "$ifname" hw ether "$macaddr"
	fi

	json_select ..
}

mac80211_setup_supplicant() {
	wpa_supplicant_prepare_interface "$ifname" nl80211 || return 1
	wpa_supplicant_add_network "$ifname"
	wpa_supplicant_run "$ifname" ${hostapd_ctrl:+-H $hostapd_ctrl}
}

mac80211_setup_adhoc() {
	json_get_vars bssid ssid key mcast_rate

	keyspec=
	[ "$auth_type" == "wep" ] && {
		set_default key 1
		case "$key" in
			[1234])
				local idx
				for idx in 1 2 3 4; do
					json_get_var ikey "key$idx"

					[ -n "$ikey" ] && {
						ikey="$(($idx - 1)):$(prepare_key_wep "$ikey")"
						[ $idx -eq $key ] && ikey="d:$ikey"
						append keyspec "$ikey"
					}
				done
			;;
			*)
				append keyspec "d:0:$(prepare_key_wep "$key")"
			;;
		esac
	}

	brstr=
	for br in $basic_rate_list; do
		hostapd_add_rate brstr "$br"
	done

	mcval=
	[ -n "$mcast_rate" ] && hostapd_add_rate mcval "$mcast_rate"

	iw dev "$ifname" set freq $freq $htmode
	iw dev "$ifname" ibss join "$ssid" $freq $bssid \
		${beacon_int:+beacon-interval $beacon_int} \
		${brstr:+basic-rates $brstr} \
		${mcval:+mcast-rate $mcval} \
		${keyspec:+keys $keyspec}
}

mac80211_setup_vif() {
	local name="$1"
	local failed

	json_select data
	json_get_vars ifname
	json_select ..

	json_select config
	json_get_vars mode ssid
	#json_get_var vif_txpower txpower #rtk_wds

#	ifconfig "$ifname" up || {
#		wireless_setup_vif_failed IFUP_ERROR
#		json_select ..
#		return
#	}

	#set_default vif_txpower "$txpower"
	#[ -z "$vif_txpower" ] || iw dev "$ifname" set txpower fixed "${vif_txpower%%.*}00"

	case "$mode" in
		mesh)
			for var in $MP_CONFIG_INT $MP_CONFIG_BOOL $MP_CONFIG_STRING; do
				json_get_var mp_val "$var"
				[ -n "$mp_val" ] && iw dev "$ifname" set mesh_param "$var" "$mp_val"
			done

			# authsae
			json_get_vars key
			if [ -n "$key" ]; then
				if [ -e "/lib/wifi/authsae.sh" ]; then
					. /lib/wifi/authsae.sh
					authsae_start_interface || failed=1
				else
					wireless_setup_vif_failed AUTHSAE_NOT_INSTALLED
					json_select ..
					return
				fi
			fi
		;;
		adhoc)
			wireless_vif_parse_encryption
			if [ "$wpa" -gt 0 -o "$auto_channel" -gt 0 ]; then
				mac80211_setup_supplicant || failed=1
			else
				mac80211_setup_adhoc
			fi
		;;
		sta)
			json_get_vars wds
			#if [ $wds == "1" ]; then
				iwpriv $ifname set_mib wds_client_enable=1
				iwpriv $ifname set_mib wds_max_number=1
				json_get_vars encryption
				json_get_vars key
				if [ "$encryption" == "none" ]; then
					iwpriv $ifname set_mib wds_encrypt=0		#no security
				elif [ "$encryption" == "wep-open" ]; then
					json_get_vars key1
					len=${#key1}
					if [ $len -gt 12 ]; then
						iwpriv $ifname set_mib wds_encrypt=5		#WEP128
					else	
						iwpriv $ifname set_mib wds_encrypt=1		#WEP64
					fi
					iwpriv $ifname set_mib wds_wepkey=$(prepare_key_wep "$key1")
				elif [ "$encryption" == "psk2+tkip" ] || [ "$encryption" == "psk+tkip" ]; then
					iwpriv $ifname set_mib wds_encrypt=2		#TKIP
					iwpriv $ifname set_mib wds_passphrase=$key
				elif [ "$encryption" == "psk2+ccmp" ] || [ "$encryption" == "psk+ccmp" ] || [ "$encryption" == "psk2" ] || [ "$encryption" == "psk" ]; then
					iwpriv $ifname set_mib wds_encrypt=4		#AES(CCMP)
					iwpriv $ifname set_mib wds_passphrase=$key
				fi
			    ptp_clientwds_set_mib "$ptp" "$ifname"
			#else
			#	iwpriv $ifname set_mib wds_client_enable=0
			#fi
			mac80211_setup_supplicant || failed=1
		;;
		ap)
			#sleep 1  #sleep is not necessary. without sleep, hostapd setting chan 36 is earlier than iwpriv setting chan 0 for 410ms
			### Auto channel fix : re-setup auto channel to override hospapd's channel setting 
			iwpriv $ifname set_mib disable_DFS=0
			[ "$auto_channel" -gt 0 ] && {
				if [ "$channel" != "acs_survey" ]; then
					iwpriv ${ifname} set_mib channel=0
				fi
				#iwpriv ${ifname} autoch
			}

		;;
	esac

	[ -z "$ack" ] || iwpriv $ifname set_mib ack_timeout=$ack
	[ -n "$mpdu" ] && iwpriv $ifname set_mib ampdu=$mpdu
        if [ "$data_rate" == "auto" ]; then
                iwpriv $ifname set_mib autorate=1
        else
                if [ $data_rate -le 28 ]; then
                                c=$((1<<$data_rate))
                else
                                a=$((1<<31))
                                b=$data_rate-30
                                c=$(($a|$b))
                fi
                iwpriv $ifname set_mib autorate=0
                iwpriv $ifname set_mib fixrate=$c
        fi

	if [ $tx_streams == "1" ]; then
		if [ $rx_streams == "1" ]; then
			#1T1R
			iwpriv $ifname set_mib MIMO_TR_mode=4
		else
			#1T2R
			iwpriv $ifname set_mib MIMO_TR_mode=1
		fi
	else
		#2T2R
		iwpriv $ifname set_mib MIMO_TR_mode=3
	fi
	# ensure AP in 11ac mode, to fix hostapd can't set driver in 11ac mode if VHT40/VHT20
	#[ "$enable_ac" != "0" ] && {
		# 11a(4)+11n(8)+11ac(64)=76
		#iwpriv $ifname set_mib band=76
	#}
	# down/up interface for workaround fix connection issue of some 11n clients
	#ifconfig "$ifname" down
	#sleep 1
	#ifconfig "$ifname" up || {
		#wireless_setup_vif_failed IFUP_ERROR
		#json_select ..
		#return
	#}

	# restore hiddenAP to default for configured interface (should not override hostapd setting)
	# iwpriv "$ifname" set_mib hiddenAP=0

	json_select ..
	[ -n "$failed" ] || wireless_add_vif "$name" "$ifname"
}

get_freq() {
	local phy="$1"
	local chan="$2"
	iw "$phy" info | grep -E -m1 "(\* ${chan:-....} MHz${chan:+|\\[$chan\\]})" | grep MHz | awk '{print $2}'
}

mac80211_interface_cleanup() {
	local phy="$1"

	acc_set_forward_id

	for wdev in $(list_phy_interfaces "$phy"); do
		ifconfig "$wdev" down 2>/dev/null
		iw dev "$wdev" del
	done
}

drv_mac80211_cleanup() {
	hostapd_common_cleanup
}

drv_mac80211_setup() {
	json_select config
	json_get_vars \
		phy macaddr path \
		country chanbw distance \
		txpower antenna_gain \
		rxantenna txantenna \
		frag rts beacon_int max_sta_assoc \
		ack sgi stbc mpdu dfs data_rate tx_streams rx_streams ptp backup_enable amsdu_802_3 disabled prefer_pcp client_isolation
	json_get_values basic_rate_list basic_rate
	json_select ..

	find_phy || {
		echo "Could not find PHY for device '$1'"
		wireless_set_retry 0
		return 1
	}

	if [ "$phy" = "phy0" ]; then
		tfile=/tmp/.wifi-config-radio0
	else
		tfile=/tmp/.wifi-config-radio1
	fi
	touch $tfile

	wireless_set_data phy="$phy"
	mac80211_interface_cleanup "$phy"

	# convert channel to frequency
	[ "$auto_channel" -gt 0 ] || freq="$(get_freq "$phy" "$channel")"

	[ -n "$country" ] && {
		iw reg get | grep -q "^country $country:" || {
			iw reg set "$country"
			sleep 1
		}
	}
	rtk_set_regdomain "$phy" "$country"

	hostapd_conf_file="/var/run/hostapd-$phy.conf"

	no_ap=1
	macidx=0
	staidx=0

	[ -n "$chanbw" ] && {
		for file in /sys/kernel/debug/ieee80211/$phy/ath9k/chanbw /sys/kernel/debug/ieee80211/$phy/ath5k/bwmode; do
			[ -f "$file" ] && echo "$chanbw" > "$file"
		done
	}

	set_default rxantenna all
	set_default txantenna all
	set_default distance 0
	set_default antenna_gain 0

	iw phy "$phy" set antenna $txantenna $rxantenna >/dev/null 2>&1
	iw phy "$phy" set antenna_gain $antenna_gain
	iw phy "$phy" set distance "$distance"

	[ -n "$frag" ] && iw phy "$phy" set frag "${frag%%.*}"
	[ -n "$rts" ] && iw phy "$phy" set rts "${rts%%.*}"

	has_ap=
	hostapd_ctrl=
	for_each_interface "ap" mac80211_check_ap

	rm -f "$hostapd_conf_file"
	[ -n "$has_ap" ] && mac80211_hostapd_setup_base "$phy"

	for_each_interface "ap" mac80211_prepare_vif
	for_each_interface "sta adhoc mesh monitor" mac80211_prepare_vif

	[ -n "$hostapd_ctrl" ] && {
		/usr/sbin/hostapd -P /var/run/wifi-$phy.pid -B "$hostapd_conf_file"
		ret="$?"
		wireless_add_process "$(cat /var/run/wifi-$phy.pid)" "/usr/sbin/hostapd" 1
		[ "$ret" != 0 ] && {
			wireless_setup_failed HOSTAPD_START_FAILED
			return
		}
	}

	for_each_interface "ap sta adhoc mesh monitor" mac80211_setup_vif

	wireless_set_up

	rm $tfile
	[ -e /tmp/.network-config ] && rm /tmp/.network-config
	[ -e /tmp/.dongle-reset ] && rm /tmp/.dongle-reset
}

list_phy_interfaces() {
	local phy="$1"
	if [ -d "/sys/class/ieee80211/${phy}/device/net" ]; then
		ls "/sys/class/ieee80211/${phy}/device/net" 2>/dev/null;
	else
		ls "/sys/class/ieee80211/${phy}/device" 2>/dev/null | grep net: | sed -e 's,net:,,g'
	fi
}

drv_mac80211_teardown() {
	wireless_process_kill_all

	json_select data
	json_get_vars phy
	json_select ..
	json_select config
	json_get_vars disabled
	json_select ..

	mac80211_interface_cleanup "$phy"
}

ptp_ap_set_mib() {
    ptp_enable="$1"
    ifname="$2"

    if [ "$ptp_enable" == "1" ]; then
        iwpriv $ifname set_mib wds_ptp=1
        
        iwpriv $ifname set_mib ap_bkq_aifsn=0 
        iwpriv $ifname set_mib ap_bkq_cwmin=0
        iwpriv $ifname set_mib ap_bkq_cwmax=1
        iwpriv $ifname set_mib ap_beq_aifsn=0 
        iwpriv $ifname set_mib ap_beq_cwmin=0 
        iwpriv $ifname set_mib ap_beq_cwmax=1 
        iwpriv $ifname set_mib ap_viq_aifsn=0 
        iwpriv $ifname set_mib ap_viq_cwmin=0 
        iwpriv $ifname set_mib ap_viq_cwmax=1 
        iwpriv $ifname set_mib ap_voq_aifsn=0 
        iwpriv $ifname set_mib ap_voq_cwmin=0 
        iwpriv $ifname set_mib ap_voq_cwmax=1 
        
        iwpriv $ifname set_mib sta_bkq_aifsn=1
        iwpriv $ifname set_mib sta_bkq_cwmin=1
        iwpriv $ifname set_mib sta_bkq_cwmax=2
        iwpriv $ifname set_mib sta_beq_aifsn=1
        iwpriv $ifname set_mib sta_beq_cwmin=1
        iwpriv $ifname set_mib sta_beq_cwmax=2
        iwpriv $ifname set_mib sta_viq_aifsn=1
        iwpriv $ifname set_mib sta_viq_cwmin=1
        iwpriv $ifname set_mib sta_viq_cwmax=2
        iwpriv $ifname set_mib sta_voq_aifsn=1
        iwpriv $ifname set_mib sta_voq_cwmin=1
        iwpriv $ifname set_mib sta_voq_cwmax=2
	

	# If metro_backup daemon is enabled, do not try to change forward_id in script
	if [ "$backup_enable" == "1" ]; then

		if [ ! -n "$(ps |grep metro_backup|grep -v grep)" ]; then
			echo "2" > /sys/devices/virtual/net/br-wan/bridge/forward_id
		fi
	fi
	echo "1" > /sys/devices/virtual/net/br-wan/bridge/ptmp

    else
        iwpriv $ifname set_mib wds_ptp=0
        iwpriv $ifname set_mib txforce=255
	echo "2" > /sys/devices/virtual/net/br-wan/bridge/forward_id
    fi
}

ptp_clientwds_set_mib() {
    ptp_enable="$1"
    ifname="$2"

    if [ "$ptp_enable" == "1" ]; then
        iwpriv $ifname set_mib wds_ptp=1
	# If metro_backup daemon is enabled, do not try to change forward_id in script

	if [ "$backup_enable" == "1" ]; then  
		if [ ! -n "$(ps |grep metro_backup|grep -v grep)" ]; then
			echo "2" > /sys/devices/virtual/net/br-wan/bridge/forward_id
		fi
	fi
    else
        iwpriv $ifname set_mib wds_ptp=0
    fi
    # Temp Workaround to prevent scan stop in client mode
    iwpriv $ifname set_mib disable_DFS=1
}

set_rtk_acl() 
{
        local ifname="$1"
        local acl_mac

	json_get_vars acl_enable acl_select acl_mac_list

        # Clear out ACL mode and list
        iwpriv $ifname set_mib aclmode=0
        iwpriv $ifname set_mib aclnum=0

        if [ "$acl_enable" == "1" ]; then 

                if [ "$acl_select" == "accept" ]; then  
                        iwpriv $ifname set_mib aclmode=1
                else
                        iwpriv $ifname set_mib aclmode=2
                fi

                # remove spaces
                acl_mac_list=${acl_mac_list// /}

                for acl_mac_entry in ${acl_mac_list//;/ }; do   
                        # MAC is stored as 001122334455 format (sans colons)
                        acl_mac=${acl_mac_entry:0:12}
                        iwpriv $ifname set_mib acladdr="$acl_mac"
                done

        fi
} 


add_driver mac80211
