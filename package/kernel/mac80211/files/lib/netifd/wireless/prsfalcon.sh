#!/bin/sh
. /lib/netifd/netifd-wireless.sh
. /lib/prs_power_util.sh

init_wireless_driver "$@"

drv_prsfalcon_init_device_config() {
	config_add_string path country
	config_add_int txpower ack mpdu amsdu_802_3 client_isolation igmp_snooping
	config_add_string data_rate

	# 60G auto mode prefer pcp
	config_add_int prefer_pcp
}

drv_prsfalcon_init_iface_config() {
	config_add_string ifname

	# 60G encryption
	config_add_string encryption_60g encryption_60g_key linkid_enable peer_intf bssid
}

prsfalcon_prepare_vif() {
	json_select config

	json_get_vars ifname mode ssid
	json_get_vars encryption_60g
	json_get_vars encryption_60g_key
	json_get_vars linkid_enable peer_intf
	json_get_vars bssid

	if [ -z "$txpower" -o $txpower -lt 0 -o $txpower -gt 9 ]; then
		dmg_txpower="8"
	else
		pwr_lst="3c 36 30 28 22 1c 14 e 8 0"
		i=0
		for pwr in $pwr_lst
		do
			if [ $i -eq $txpower ]; then
				dmg_txpower=$pwr
				break
			fi
			i=$(($i+1))
		done
	fi

	[ -n "$(lsmod|grep prs_falcon)" ] && {
		rmmod prs_falcon
		sleep 2
	}
	insmod prs_falcon PRS_BRIDGE_MODE_ENABLE=1 >/dev/console

	# prs_power_on
	i=0
	while [ "${i}" != "5" ]
	do
		ifname=$(list_phy_interfaces $path)
		[ -z $ifname ] || break;
		i=$(($i+1))
		echo ">>>>> WiGig interface not ready yet, waiting...$i" >/dev/console
		sleep 1
	done
	#move the smp settings from swdone to here
	echo 3 > /sys/class/net/$ifname/queues/rx-0/rps_cpus
	echo 3 > /sys/class/net/$ifname/queues/tx-0/xps_cpus

	echo 0 > /sys/devices/$path/prs_attrs/fwlogs
	echo 522 > /sys/devices/$path/prs_attrs/trace_features

	[ "${i}" = "5" ] && {
		wireless_setup_vif_failed IFUP_ERROR
		return
	}

	link_id=$(get_wireless_link_id)
	if [ "$linkid_enable" = "1" -a -n $link_id ]; then
		echo "$link_id" > /sys/class/net/$ifname/linkid
	fi

#	if [ "$peer_intf" = "1" ]; then
#		echo 1 > /sys/devices/$path/prs_attrs/peer_intf
#	else
#		echo 0 > /sys/devices/$path/prs_attrs/peer_intf
#	fi

	if [ "$client_isolation" = "1" ]; then
		echo 1 > /sys/devices/virtual/net/br-wan/bridge/client_isolation
	else
		echo 0 > /sys/devices/virtual/net/br-wan/bridge/client_isolation
	fi

	if [ "$igmp_snooping" = "1" ]; then
		echo 1 > /sys/devices/virtual/net/br-wan/bridge/multicast_querier
		echo 1 > /sys/devices/virtual/net/br-wan/bridge/multicast_snooping
	else
		echo 0 > /sys/devices/virtual/net/br-wan/bridge/multicast_querier
		echo 0 > /sys/devices/virtual/net/br-wan/bridge/multicast_snooping
	fi

	if [ "$mode" = "dmgpcp" ]; then
		ctrl_intf="/var/run/hostapd/$ifname"
		prsfalcon_hostapd_setup_bss "$ifname" "$channel" "$ssid" "$encryption_60g" "$encryption_60g_key" || return
	else
		ctrl_intf="/var/run/wpa_supplicant/$ifname"
		prsfalcon_wpa_supplicant_setup_sta "$ifname" "$channel" "$ssid" "$encryption_60g" "$encryption_60g_key" "$bssid" || return
	fi

	json_select ..

	json_add_object data
	json_add_string ifname "$ifname"
	json_close_object
	json_select config

	tty_idx=$(ls /sys/devices/$path/*/tty | sed -e 's,ttyACM,,g' | tr -d '\n')

	prs_serial device$tty_idx "mib s 11-42-1-15 2"

	# change AMSDU type
	# not implemented in driver
	#amsdu_type_file="/sys/devices/$path/prs_attrs/amsdu_type"
	#if [ $amsdu_802_3 == "1" ]; then 
	#    echo 1 > $amsdu_type_file
	#else
	#    echo 0 > $amsdu_type_file
	#fi

	# disabled fwlogs
	#wpa_cli -g $ctrl_intf -i $ifname vendor 0x20cec4 127 210000000000000000

	echo "$mpdu" > /sys/devices/$path/prs_attrs/ampdu
	#wpa_cli -g $ctrl_intf -i $ifname vendor 0x20cec4 8 0${mpdu}000000

	if [ -z "$data_rate" -o "$data_rate" = "auto" ]; then
		mcs=9
	else
		mcs=$(printf '%x' $(($data_rate - 12)))
	fi

	# enable rate adaptation
	prs_serial device$tty_idx "mib s 12-9-1-1 1"
	wpa_cli -g $ctrl_intf -i $ifname vendor 0x20cec4 14 110101112

	# set max MCS
	wpa_cli -g $ctrl_intf -i $ifname vendor 0x20cec4 14 120901040$mcs

	# periodic mcs update
	prs_serial device$tty_idx "mib s 11-1-1-11 1"

	# periodic update interval - 5000 ms
	prs_serial device$tty_idx "mib s 11-1-1-12 1388"

	# change txpower, sometimes may have command failed, need to set again
	nset=1
	cmdret=`prs_serial device$tty_idx "mib s 11-42-1-12 $dmg_txpower"|tr -d "\n"`
	while [ -n "$cmdret" ]
	do
		echo "Failed to set WiGig interface attenuation to $dmg_txpower, $nset retry...!!" >/dev/console
		nset=$((nset + 1))
		[ $nset -ge 10 ] && break
		sleep 1
		cmdret=`prs_serial device$tty_idx "mib s 11-42-1-12 $dmg_txpower"|tr -d "\n"`
	done

        #if [ -z "$ack" -o "$ack" = "0" ]; then
        #        dmg_ack=28
        #else
        #        dmg_ack=$ack
        #fi

        # convert to propagation delay in 100ns, hexadecimal value
        #dmg_propg=`printf "%x" $(((($dmg_ack - 8) / 2) * 10))` #propagation delay

        prs_serial device$tty_idx "mib s 14-7-1-2 64" # sets the propagation delay to MAX value

	# enable CTS/RTS protection
	prs_serial device$tty_idx "mib s 12-1-1-4 1"

	# enable continuous_rssi to auto update rx mcs and rx rssi
	echo 1 > /sys/devices/$path/prs_attrs/continuous_rssi

	# enable aiming prsnl capability
	prs_serial device$tty_idx "mib s 12-1-1-21 1"

	json_select ..
}

prsfalcon_setup_vif() {
	local name="$1"
	local failed

	json_select data
	json_get_vars ifname
	json_select ..

	json_select config
	json_get_vars mode ssid

	ifconfig "$ifname" up || {
		wireless_setup_vif_failed IFUP_ERROR
		json_select ..
		return
	}

	#dmg_freq=`iwconfig $ifname |grep Frequency|sed 's/.*Frequency://g'|sed 's/ GHz.*//g'|awk '{printf $1*1000}'`
	# need to set iface down/up for ibss join command to work in pcp mode
	#ifconfig $ifname down
	#ifconfig $ifname up
	#iw $ifname ibss join "$ssid" $dmg_freq

	json_select ..
	[ -n "$failed" ] || wireless_add_vif "$name" "$ifname"
}

prsfalcon_interface_cleanup() {
	for dev in $(ls /sys/class/net/); do
		[ -d /sys/class/net/${dev}/device ] && { \
			[ "/sys/devices/${path}" = "$(readlink -f /sys/class/net/${dev}/device)" ] && { \
				[ -f /var/run/hostapd-${dev}.conf ] && { \
					ifconfig ${dev} down
					#wpa_cli -g /var/run/hostapd/global raw REMOVE ${dev}
					rm -f /var/run/hostapd-${dev}.conf
				}
				[ -f /var/run/hostapd-${dev}.pid ] && { \
					cat /var/run/hostapd-${dev}.pid | xargs kill
				}
				[ -f /var/run/wpa_supplicant-${dev}.conf ] && { \
					ifconfig ${dev} down
					#wpa_cli -g /var/run/wpa_supplicantglobal interface_remove ${dev}
					rm -f /var/run/wpa_supplicant-${dev}.conf
				}
				[ -f /var/run/wpa_supplicant-${dev}.pid ] && { \
					cat /var/run/wpa_supplicant-${dev}.pid | xargs kill
				}
			}
		}
	done
	# prs_power_off $devname
	sleep 1
}

drv_prsfalcon_cleanup() {
	# prs_power_off
	sleep 1
}

drv_prsfalcon_setup() {
	json_select config
	json_get_vars \
		path country \
		txpower ack mpdu data_rate \
		amsdu_802_3 disabled prefer_pcp client_isolation \
		igmp_snooping
	json_select ..

	devname=$1
	hw_ver=`cat /tmp/state/60g_${devname}_hw_ver`

	tfile=/tmp/.wifi-config-$devname
	touch $tfile

	prsfalcon_interface_cleanup

        [ -n "$country" ] && {
                iw reg get | grep -q "^country $country:" || {
                        iw reg set "$country"
                        sleep 1
                }
        }

	for_each_interface "dmgpcp dmgsta dmgauto" prsfalcon_prepare_vif

	for_each_interface "dmgpcp dmgsta dmgauto" prsfalcon_setup_vif

	wireless_set_up

	rm $tfile
	[ -e /tmp/.network-config ] && rm /tmp/.network-config
	[ -e /tmp/.dongle-reset ] && rm /tmp/.dongle-reset
}

prsfalcon_hostapd_setup_bss() {
	local ifname="$1"
	local channel="$2"
	local ssid="$3"
	local encr_en="$4"
	local encr_key="$5"

	case "$channel" in
		1|2|3|4)
			channel=$channel
			;;
		*)
			channel=1
			;;
	esac

	hostapd_cfg=
	append hostapd_cfg "interface=$ifname" "$N"
	append hostapd_cfg "ctrl_interface=/var/run/hostapd" "$N"
	append hostapd_cfg "ssid=$ssid" "$N"
	append hostapd_cfg "channel=$channel" "$N"
	append hostapd_cfg "hw_mode=ad" "$N"

	if [ "$encr_en" = "1" ]; then
		append hostapd_cfg "auth_algs=1" "$N"
		append hostapd_cfg "num_of_ptksa_replay_counters=3" "$N"
		append hostapd_cfg "num_of_gtksa_replay_counters=3" "$N"
		append hostapd_cfg "wpa=2" "$N"
		append hostapd_cfg "wpa_key_mgmt=WPA-PSK" "$N"
		append hostapd_cfg "wpa_pairwise=GCMP" "$N"
		append hostapd_cfg "wpa_passphrase=$encr_key" "$N"
		append hostapd_cfg "wpa_group_rekey=0" "$N"
	fi

	cat > /var/run/hostapd-$ifname.conf <<EOF
$hostapd_cfg
EOF

	#wpa_cli -g /var/run/hostapd/global raw ADD bss_config=$ifname:/var/run/hostapd-$ifname.conf
	hostapd -g /var/run/hostapd/$ifname -i $ifname /var/run/hostapd-$ifname.conf -B -P /var/run/hostapd-$ifname.pid
}

prsfalcon_wpa_supplicant_setup_sta() {
	local ifname="$1"
	local channel="$2"
	local ssid="$3"
	local encr_en="$4"
	local encr_key="$5"
	local bssid="$6"

	local T="	"

	local freq="58320 60480 62640 64800"

	case "$channel" in
		1)
			freq=58320
			;;
		2)
			freq=60480
			;;
		3)
			freq=62640
			;;
		4)
			freq=64800
			;;
	esac

	wpa_cfg=
	append wpa_cfg "ctrl_interface=/var/run/wpa_supplicant" "$N"

	wpa_network_cfg=
	append wpa_network_cfg "ssid=\"$ssid\"" "$N$T"
	append wpa_network_cfg "scan_freq=$freq" "$N$T"
	append wpa_network_cfg "scan_ssid=1" "$N$T"

	if [ "$encr_en" = "1" ]; then
		append wpa_cfg "num_of_ptksa_replay_counters=3" "$N"
		append wpa_cfg "num_of_gtksa_replay_counters=3" "$N"

		append wpa_network_cfg "key_mgmt=WPA-PSK" "$N$T"
		append wpa_network_cfg "proto=WPA2" "$N$T"
		append wpa_network_cfg "pairwise=GCMP" "$N$T"
		append wpa_network_cfg "group=GCMP" "$N$T"
		append wpa_network_cfg "psk=\"$encr_key\"" "$N$T"
	else
		append wpa_network_cfg "key_mgmt=NONE" "$N$T"
	fi

	if [ -n "$bssid" ]; then
		append wpa_network_cfg "bssid=$bssid" "$N$T"
	fi

	cat > /var/run/wpa_supplicant-$ifname.conf <<EOF
$wpa_cfg
network={
	$wpa_network_cfg
}
EOF

	#wpa_cli -g /var/run/wpa_supplicantglobal interface_add $ifname /var/run/wpa_supplicant-$ifname.conf nl80211 /var/run/wpa_supplicant
	wpa_supplicant -i $ifname -Dnl80211 -c/var/run/wpa_supplicant-$ifname.conf -B -P /var/run/wpa_supplicant-$ifname.pid
}

list_phy_interfaces() {
	local path=$1
	if [ -d "/sys/devices/${path}/net" ]; then
		ls "/sys/devices/${path}/net" | grep -v sta 2>/dev/null;
	fi
}

get_wireless_link_id() {
	if [ -f /sys/class/net/eth0/address ]; then
		cat /sys/class/net/eth0/address | tr -d ':'
	fi
}

drv_prsfalcon_teardown() {
	wireless_process_kill_all

	json_select config
	json_get_vars disabled path
	json_select ..

	devname=$1

	prsfalcon_interface_cleanup
}

add_driver prsfalcon
