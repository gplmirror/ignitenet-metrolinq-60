acc_set_forward_id() {
    [ "$board" = "Metrolinq" ] && {
        wigig_disabled=`uci get wireless.radio1.disabled`
        ac_disabled=`uci get wireless.radio0.disabled`
        backup=`uci get wireless.radio1.backup_enable`
        ac_mode=`uci get wireless.radio0.mode`
        if [ "$ac_disabled" = "0"  ]; then
            if [ "$wigig_disabled" = "0" ]; then
                if [ "$backup" = "0" ]; then
                    echo "1" > /sys/devices/virtual/net/br-wan/bridge/forward_id
                fi
            else
                echo "2" > /sys/devices/virtual/net/br-wan/bridge/forward_id
                if [ "$ac_mode" = "ap" ]; then
                    echo "1" > /sys/devices/virtual/net/br-wan/bridge/ptmp
                fi
            fi
        else
            echo "1" > /sys/devices/virtual/net/br-wan/bridge/forward_id
        fi
    }
}
