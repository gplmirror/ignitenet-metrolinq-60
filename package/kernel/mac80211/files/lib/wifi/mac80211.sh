#!/bin/sh
append DRIVERS "mac80211"

lookup_phy() {
	[ -n "$phy" ] && {
		[ -d /sys/class/ieee80211/$phy ] && return
	}

	local devpath
	config_get devpath "$device" path
	[ -n "$devpath" ] && {
		for _phy in /sys/devices/$devpath/ieee80211/phy*; do
			[ -e "$_phy" ] && {
				phy="${_phy##*/}"
				return
			}
		done
	}

	local macaddr="$(config_get "$device" macaddr | tr 'A-Z' 'a-z')"
	[ -n "$macaddr" ] && {
		for _phy in $(ls /sys/class/ieee80211 2>/dev/null); do
			[ "$macaddr" = "$(cat /sys/class/ieee80211/${_phy}/macaddress)" ] || continue
			phy="$_phy"
			return
		done
	}
	phy=
	return
}

find_mac80211_phy() {
	local device="$1"

	config_get phy "$device" phy
	lookup_phy
	[ -n "$phy" -a -d "/sys/class/ieee80211/$phy" ] || {
		echo "PHY for wifi device $1 not found"
		return 1
	}
	config_set "$device" phy "$phy"

	config_get macaddr "$device" macaddr
	[ -z "$macaddr" ] && {
		config_set "$device" macaddr "$(cat /sys/class/ieee80211/${phy}/macaddress)"
	}

	return 0
}

check_mac80211_device() {
	config_get phy "$1" phy
	[ -z "$phy" ] && {
		find_mac80211_phy "$1" >/dev/null || return 0
		config_get phy "$1" phy
	}
	[ "$phy" = "$dev" ] && found=1
}

detect_mac80211() {
	devidx=0
	config_load wireless
	while :; do
		config_get type "radio$devidx" type
		[ -n "$type" ] || break
		devidx=$(($devidx + 1))
	done

	config_get bandsteering "global" bandsteering
	has_bs=1

	[  -z "$bandsteering" ] && has_bs=0

	total_radio_count=$(ls /sys/class/ieee80211 | grep -c phy)

	for dev in $(ls /sys/class/ieee80211); do
		found=0
		config_foreach check_mac80211_device wifi-device
		[ "$found" -gt 0 ] && continue

		# Don't continue if we already have wifi 
		[ "$devidx" -ge $total_radio_count ] && break

		mode_band="ng"
		channel="auto"
		htmode=""
		ht_capab=""

		iw phy "$dev" info | grep -q 'Capabilities:' && htmode=HT20
		iw phy "$dev" info | grep -q '2412 MHz' || mode_band="a"

		vht_cap=$(iw phy "$dev" info | grep -c 'VHT Capabilities')
		[ "$vht_cap" -gt 0 ] && {
			mode_band="ac";
			htmode="VHT20"
		}

		[ -n $htmode ] && append ht_capab "	option htmode	$htmode" "$N"

		if [ -x /usr/bin/readlink ]; then
			path="$(readlink -f /sys/class/ieee80211/${dev}/device)"
			path="${path##/sys/devices/}"
			dev_id="	option path	'$path'"
		fi

		# To get MAC address from driver file, modified by Jason Huang on 20151008
		if [ -f /sys/class/ieee80211/${dev}/macaddress ]; then
			dev_mac="       option macaddr  $(cat /sys/class/ieee80211/${dev}/macaddress)"
		fi

## TODO: one of many todos here - loop to add all virtual vifs
# we still do not have vap patched into driver so hardcode 8 now
		v_config=""
		VAP_NUM_ARRAY="1"

		for i in ${VAP_NUM_ARRAY}; do
			append v_config "config wifi-iface radio"${devidx}"_$((i))" "$N"
			append v_config "	option device   radio${devidx}" "$N"
			if [ $i == 1 ]; then
				append v_config "	option disabled 0" "$N"
				append v_config "	option created 1" "$N"
				append v_config "	option ssid     ACN${devidx}" "$N"
			else
				append v_config "	option disabled 1" "$N"
				append v_config "	option created 0" "$N"
				append v_config "	option ssid     ACN${devidx}.$((i-1))" "$N"
			fi
			append v_config "	option network  lan" "$N"
			append v_config "	option mode     ap" "$N"
			append v_config "	option encryption none" "$N"
			append v_config "	option wmm_enable 1" "$N"
			append v_config "	option wmm_power 0" "$N"
			append v_config "	option acl_enable 0" "$N"
			append v_config "	option limit_up_enable 0" "$N"
			append v_config "	option limit_down_enable 0" "$N"
			append v_config "	option vlan_tag_traffic 0" "$N"
			append v_config "	option wmm_bk 4,10,7,0" "$N"
			append v_config "	option wmm_be 4,6,3,0" "$N"
			append v_config "	option wmm_vi 3,4,1,188" "$N"
			append v_config "	option wmm_vo 2,3,1,102" "$N"
			# default min_signal_allowed
			append v_config "	option min_signal_allowed 0" "$N"
			append v_config "	option radius_mac_acl 0" "$N"
			append v_config "	option auth_port 1812" "$N"
			append v_config "$N"
		done

		# noscan default value for 2.4Ghz
		if [ $mode_band == "ng" ]; then
			noscan_option="	option noscan  0"
			freq_option = "	option supported_freq  2.4"
		else
			noscan_option=""
			freq_option = "	option supported_freq  5"
		fi

		if [ "$devidx" != "1" ]; then
			# generic 2.4G/5G interface
			cat <<EOF
config wifi-device  radio$devidx
	option type     mac80211
	option mode	ap
	option channel  ${channel}
	option ptp  0
	option hwmode	11${mode_band}
$dev_id
$dev_mac
$ht_capab
	# REMOVE THIS LINE TO ENABLE WIFI:
	option disabled 0
	option country US
	option tx_streams 2
	option rx_streams 2
	option sgi 1
	option stbc 0
	option data_rate auto
	option mpdu 1
	option wifi_num 8
	option frag 2346
	option rts 2347
	option beacon_int 100
	option dfs 0
	option ack 64
$noscan_option
$freq_option
$v_config

EOF
		fi
		devidx=$(($devidx + 1))
	done

	[ "$found" -gt 0 ] || {
		[ $has_bs == 0 ] && {
			# default band steering
			cat <<EOF
config settings 'global'
	option bandsteering 0
EOF
		}
	}
}

