#!/bin/sh
append DRIVERS "prsfalcon"

lookup_phy() {
	[ -n "$phy" ] && {
		[ -d /sys/class/ieee80211/$phy ] && return
	}

	local devpath
	config_get devpath "$device" path
	[ -n "$devpath" ] && {
		for _phy in /sys/devices/$devpath/ieee80211/phy*; do
			[ -e "$_phy" ] && {
				phy="${_phy##*/}"
				return
			}
		done
	}

	phy=
	return
}

find_prsfalcon_phy() {
	local device="$1"

	config_get phy "$device" phy
	lookup_phy
	[ -n "$phy" -a -d "/sys/class/ieee80211/$phy" ] || {
		echo "PHY for wifi device $1 not found"
		return 1
	}
	config_set "$device" phy "$phy"

	return 0
}

check_prsfalcon_device() {
	config_get phy "$1" phy
	[ -z "$phy" ] && {
		find_prsfalcon_phy "$1" >/dev/null || return 0
		config_get phy "$1" phy
	}
	[ "$phy" = "$dev" ] && found=1
}

detect_prsfalcon() {
	devidx=0
	config_load wireless
	while :; do
		config_get type "radio$devidx" type
		[ -n "$type" ] || break
		devidx=$(($devidx + 1))
	done

	total_radio_count=$(ls /sys/class/ieee80211 | grep -c phy)

	for dev in $(ls /sys/class/ieee80211); do
		found=0
		config_foreach check_prsfalcon_device wifi-device
		[ "$found" -gt 0 ] && continue

		# skip non-prs devices
		[ ! -d /sys/class/ieee80211/${dev}/device/prs_attrs ] && continue

		# Don't continue if we already have wifi 
		[ "$devidx" -ge $total_radio_count ] && break

		if [ -x /usr/bin/readlink ]; then
			path="$(readlink -f /sys/class/ieee80211/${dev}/device)"
			path="${path##/sys/devices/}"
			dev_id="	option path '$path'"
		fi

		v_config=""
		append v_config "config wifi-iface 'radio"${devidx}"_1'" "$N"
		append v_config "	option device 'radio${devidx}'" "$N"
		append v_config "	option disabled '0'" "$N"
		append v_config "	option created '1'" "$N"
		append v_config "	option ssid 'ACN${devidx}'" "$N"
		append v_config "	option network 'none'" "$N"
		append v_config "	option mode 'dmgpcp'" "$N"
		append v_config "	option encryption_60g '0'" "$N"
		append v_config "	option backup_enable '0'" "$N"
		append v_config "       option linkid_enable '1'" "$N"
		append v_config "$N"

		cat <<EOF
config wifi-device 'radio$devidx'
	option type 'prsfalcon'
	option mode 'dmgpcp'
	option channel '1'
	option hwmode '11ad'
	option htmode '2160'
$dev_id
	option disabled '0'
	option data_rate 'auto'
	option mpdu '1'
	option ack '28'
	option txpower '9'
	option amsdu_802_3 '1'

$v_config

EOF
		devidx=$(($devidx + 1))
	done

}

