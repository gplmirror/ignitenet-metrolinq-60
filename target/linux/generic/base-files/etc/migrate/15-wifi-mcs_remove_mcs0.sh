#!/bin/sh

. /lib/functions.sh
cfg_idx_60g=$(find_60g_idx)
data_rate=`uci get wireless.radio${cfg_idx_60g}.data_rate 2> /dev/null`

if [ $data_rate == 12 ]; then
    uci set wireless.radio${cfg_idx_60g}.data_rate=auto
    uci commit wireless.radio${cfg_idx_60g}
fi