#!/bin/sh

. /lib/functions.sh
cfg_idx_60g=$(find_60g_idx)
[ "$cfg_idx_60g" == "1" ] && cfg_idx_5g="0" || cfg_idx_5g="1"

uci batch <<EOF
set network.bond='interface'
set network.bond.type='dynamic_bonding'
set network.bond.proto='static'
set network.bond.xmit_hash_policy='0'
set network.bond.mode='1'
set network.bond.primary='wlan1'
set network.bond.arp_interval='0'
set network.bond.arp_validate='0'
set network.bond.wifimon='500'
set network.bond.downdelay='3000'
set network.bond.updelay='2000'
set network.bond.bridge='br-wan'
set network.bond.fail_over_mac='1'

set wireless.radio${cfg_idx_60g}.backup_interface='wlan0'
set wireless.radio${cfg_idx_60g}.backup_enable='1'
set wireless.radio${cfg_idx_60g}_1.network='none'
set wireless.radio${cfg_idx_5g}_1.network='none'
set wireless.radio${cfg_idx_5g}_1.linkid_enable='1'
set wireless.radio${cfg_idx_5g}_1.peer_intf='0'
EOF

dmg_mode=$(uci get wireless.radio${cfg_idx_60g}.mode 2>/dev/null)
prefer_pcp=$(uci get wireless.radio${cfg_idx_60g}.prefer_pcp 2>/dev/null)

case $dmg_mode in
        dmgauto)
                if [ "$prefer_pcp" == "1" ]; then
                        new_mode='dmgpcp'
                else
                        new_mode='dmgsta'
                fi
                ;;
        dmgpcp)
                new_mode='dmgpcp'
                ;;
        dmgsta)
                new_mode='dmgsta'
                ;;
        *)
                new_mode='dmgpcp'
                ;;
esac

[ "$new_mode" == "dmgpcp" ] && uci batch <<EOF
set wireless.radio${cfg_idx_60g}.mode='dmgpcp'
set wireless.radio${cfg_idx_60g}_1.mode='dmgpcp'
set network.bond.peer_intf_mode='1'
EOF

[ "$new_mode" == "dmgsta" ] && uci batch <<EOF
set wireless.radio${cfg_idx_60g}.mode='dmgsta'
set wireless.radio${cfg_idx_60g}_1.mode='dmgsta'
set network.bond.peer_intf_mode='0'
EOF

uci commit
