uci del_list mini_snmpd.@mini_snmpd[0].interfaces="loopback"
uci del_list mini_snmpd.@mini_snmpd[0].interfaces="lan"
uci del_list mini_snmpd.@mini_snmpd[0].interfaces="wan"

uci del_list mini_snmpd.@mini_snmpd[0].interfaces="eth0"
uci del_list mini_snmpd.@mini_snmpd[0].interfaces="eth1"
uci del_list mini_snmpd.@mini_snmpd[0].interfaces="wlan0"
uci del_list mini_snmpd.@mini_snmpd[0].interfaces="wlan1"


uci add_list mini_snmpd.@mini_snmpd[0].interfaces="eth0"
uci add_list mini_snmpd.@mini_snmpd[0].interfaces="eth1"
uci add_list mini_snmpd.@mini_snmpd[0].interfaces="wlan0"
uci add_list mini_snmpd.@mini_snmpd[0].interfaces="wlan1"

uci commit mini_snmpd