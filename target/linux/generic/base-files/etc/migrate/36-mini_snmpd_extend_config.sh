mini_snmpd_radios="$(uci -q get mini_snmpd.@mini_snmpd[0].radios)"
[ -z "$mini_snmpd_radios" ] && {
	uci add_list mini_snmpd.@mini_snmpd[0].radios="wlan0"
	uci add_list mini_snmpd.@mini_snmpd[0].radios="radio1"	

}
mini_snmpd_ethernet="$(uci -q get mini_snmpd.@mini_snmpd[0].ethernet)"
[ -z "$mini_snmpd_ethernet" ] && {
	uci add_list mini_snmpd.@mini_snmpd[0].ethernet="eth0"
	uci add_list mini_snmpd.@mini_snmpd[0].ethernet="eth1"	
}

mini_snmpd_phy="$(uci -q get mini_snmpd.@mini_snmpd[0].phy)"
[ -z "$mini_snmpd_phy" ] && {
	uci add_list mini_snmpd.@mini_snmpd[0].phy="0"
	uci add_list mini_snmpd.@mini_snmpd[0].phy="4"
}

uci commit mini_snmpd
