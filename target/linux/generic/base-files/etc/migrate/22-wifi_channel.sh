#!/bin/sh

. /lib/functions.sh
cfg_idx_60g=$(find_60g_idx)

channel_60g=$(uci get wireless.radio${cfg_idx_60g}.channel 2> /dev/null)
hw_ver=`cat /tmp/state/60g_hw_ver`

if [ "$hw_ver" = "1.1" -o "$hw_ver" = "1.1A" ]; then
    if [ "$channel_60g" == "4" ]; then
        uci set wireless.radio${cfg_idx_60g}.channel='1'
        uci commit wireless.radio${cfg_idx_60g}
    fi
fi