#!/bin/sh

. /lib/functions.sh
cfg_idx_60g=$(find_60g_idx)

MID=`acc hw get product_name |cut -d'=' -f2`

case $MID in
	"ML-60-"*)
	    case $MID in
	        *"-N2"*)
	            supported_freq_0=$(uci get wireless.radio${cfg_idx_60g}.supported_freq 2> /dev/null)
	            [ "$supported_freq_0" = "" ] && {
        			uci set wireless.radio${cfg_idx_60g}.supported_freq=60
        			uci commit wireless.radio${cfg_idx_60g}
        		}
	        ;;    

            *)
                supported_freq_0=$(uci get wireless.radio0.supported_freq 2> /dev/null)
                supported_freq_1=$(uci get wireless.radio1.supported_freq 2> /dev/null)
        
        		[ "$supported_freq_0" = "" ] && {
        			uci set wireless.radio0.supported_freq=5
        			uci commit wireless.radio0
        		}
        		[ "$supported_freq_1" = "" ] && {
        			uci set wireless.radio1.supported_freq=60
        			uci commit wireless.radio1
        		}
            ;;
        esac
    ;;
esac