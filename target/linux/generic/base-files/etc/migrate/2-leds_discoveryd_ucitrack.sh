#!/bin/sh

uci get acn.settings.leds_enable >/dev/null 2>&1
if [ $? -ne 0 ]; then
	uci set acn.settings.leds_enable=1
	uci commit acn
fi

uci batch <<-EOF >/dev/null 2>/dev/null
delete ucitrack.@acn[-1]
add ucitrack acn
add_list ucitrack.@acn[-1].affects=reset_btn
add_list ucitrack.@acn[-1].affects=mgmtd
add_list ucitrack.@acn[-1].affects=rssi
EOF

uci get ucitrack.@reset_btn[-1] >/dev/null 2>&1
if [ $? -eq 1 ]; then
	uci batch <<-EOF >/dev/null 2>/dev/null
	add ucitrack reset_btn
	set ucitrack.@reset_btn[-1].init=reset_btn
	EOF
fi

uci get ucitrack.@mgmtd[-1] >/dev/null 2>&1
if [ $? -eq 1 ]; then
	uci batch <<-EOF >/dev/null 2>/dev/null
	add ucitrack mgmtd
	set ucitrack.@mgmtd[-1].init=mgmt
	EOF
fi 

uci get ucitrack.@rssi[-1] >/dev/null 2>&1
if [ $? -eq 1 ]; then
	uci batch <<-EOF >/dev/null 2>/dev/null
	add ucitrack rssi
	set ucitrack.@rssi[-1].init=rssi
	EOF
fi

uci get ucitrack.@discoveryd[-1] >/dev/null 2>&1
if [ $? -eq 1 ]; then
	uci batch <<-EOF >/dev/null 2>/dev/null
	add ucitrack discoveryd
	set ucitrack.@discoveryd[-1].init=discovery
	EOF
fi

uci get ucitrack.@network[-1].affects|grep -q discoveryd >/dev/null 2>&1
if [ $? -eq 1 ]; then
	uci add_list ucitrack.@network[-1].affects=discoveryd
fi

uci get ucitrack.@ethernet[-1].affects|grep -q discoveryd >/dev/null 2>&1
if [ $? -eq 1 ]; then
	uci add_list ucitrack.@ethernet[-1].affects=discoveryd
fi

uci commit ucitrack

# Also need firewall for discovery daemon
# If there's a better way to do this please fix!

local rule="$(uci show firewall | grep Allow-Multicast-Discovery)"

if [ -z "$rule" ]; then
	uci batch <<-EOF >/dev/null 2>/dev/null
		add firewall rule
		set firewall.@rule[-1].name="Allow-Multicast-Discovery"
		set firewall.@rule[-1].src="wan"
		set firewall.@rule[-1].proto="udp"
		set firewall.@rule[-1].dest_port="17371"
		set firewall.@rule[-1].dest_ip="233.89.188.1"
		set firewall.@rule[-1].target="ACCEPT"
		set firewall.@rule[-1].family="ipv4"
	EOF

	uci commit firewall
fi

