#!/bin/sh

system_init=$(uci get ucitrack.@system[-1].init)
if [ "$system_init" == "log" ]; then
	uci batch <<-EOF >/dev/null 2>/dev/null
    delete ucitrack.@system[-1]
    add ucitrack system
    set ucitrack.@system[-1].init='system'
    add_list ucitrack.@system[-1].affects='log'
    add_list ucitrack.@system[-1].affects='luci_statistics'
    add_list ucitrack.@system[-1].affects='sysntpd'
	EOF
fi

uci get ucitrack.@log[-1] >/dev/null 2>&1
if [ $? -eq 1 ]; then
	uci batch <<-EOF >/dev/null 2>/dev/null
	add ucitrack log
	set ucitrack.@log[-1].init='log'
	EOF
fi

uci commit ucitrack