#!/bin/sh

. /lib/functions.sh
cfg_idx_60g=$(find_60g_idx)

product_name=`acc hw get product_name |cut -d'=' -f2`
country=$(uci get wireless.radio${cfg_idx_60g}.country 2> /dev/null)
txpower=$(uci get wireless.radio${cfg_idx_60g}.txpower 2> /dev/null)

if [ "$product_name" == "ML-60-19" -a "$country" == "SG" -a $txpower -gt 7 ]; then
    uci set wireless.radio${cfg_idx_60g}.txpower='7'
    uci commit wireless.radio${cfg_idx_60g}
fi