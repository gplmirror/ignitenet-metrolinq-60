#!/bin/sh

txpower=`uci get wireless.radio0.txpower 2> /dev/null`

[ -z $txpower ] && {

    MID=`acc hw get product_name |cut -d'=' -f2`

    # Max tx power for SunSpot
    case $MID in

    "SS-N300"*|"OAP7235RL"*)
        # SkyFire and SunSpot: 2.4 radio only
        uci set wireless.radio0.txpower=27
    ;;

    "OAP1235RL"*)
        # SkyFire: 5 radio only
        uci set wireless.radio0.txpower=17
    ;;

    "OAP1232RL"*)
        # SkyFire Dual
        uci set wireless.radio0.txpower=17
        uci set wireless.radio1.txpower=27
    ;;

    "EAP1282"*)
        # SunSpot Dual
        uci set wireless.radio0.txpower=25
        uci set wireless.radio1.txpower=27
    ;;

    "FN"*"AC866"*)
        # Fusion 5
        uci set wireless.radio0.txpower=25
    ;;

    "SP-N300"*)
        # Spark N300
        uci set wireless.radio0.txpower=23
    ;;

    "IRAC750"*|"SP-AC750"*)
        # Spark AC750
        uci set wireless.radio0.txpower=23
        uci set wireless.radio1.txpower=23
    ;;
    
    "ML-60-"*)
        # MetroLinq PTP60-X
        uci set wireless.radio0.txpower=17
        uci set wireless.radio1.txpower=8
    ;;

    *)
        uci set wireless.radio0.txpower=17
        uci set wireless.radio1.txpower=27
    ;;

    esac

    uci commit
}
