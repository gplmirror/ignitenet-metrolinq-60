#!/bin/sh
. /lib/functions.sh
    
MID=`acc hw get product_name |cut -d'=' -f2`
    
case $MID in
    "ML-60-"*)
        # MetroLinq PTP60-X
        cfg_idx_60g=$(find_60g_idx)
        txpower=`uci get wireless.radio${cfg_idx_60g}.txpower 2> /dev/null`
        if [ $txpower -gt 9 ]; then
            uci set wireless.radio${cfg_idx_60g}.txpower=8
            uci commit wireless.radio${cfg_idx_60g}
        fi
    ;;
esac
