#!/bin/sh

bootmaxcnt=$(uci get acn.settings.bootmaxcnt 2> /dev/null)

[ -z $bootmaxcnt ] && uci set acn.settings.bootmaxcnt=3
uci commit acn.settings

uci get ucitrack.@flashboot[-1] >/dev/null 2>&1
if [ $? -eq 1 ]; then
	uci batch <<-EOF >/dev/null 2>/dev/null
	add ucitrack flashboot
	set ucitrack.@flashboot[-1].init=flashboot
	EOF
	uci commit ucitrack
fi

affects=$(uci get ucitrack.@acn[-1].affects)
if [ "$affects" = "${affects/flashboot/}" ]; then
	uci add_list ucitrack.@acn[-1].affects=flashboot
	uci commit ucitrack
fi
