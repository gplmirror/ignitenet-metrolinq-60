#!/bin/sh

migration_port=$(uci get acn.register.port 2> /dev/null)

[ -z $migration_port ] && uci set acn.register.port='5222,443'
uci commit acn.register

