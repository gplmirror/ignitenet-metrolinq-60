#!/bin/sh

MID=`acc hw get product_name |cut -d'=' -f2`

case $MID in
	"ML-60-"*)
		# MetroLinq PTP60-X
		usb_hang=`uci get rssi.sys.usb_hang_restart 2> /dev/null`
		[ "$usb_hang" = "" ] && {
			uci set rssi.sys.usb_hang_restart=1
			uci commit rssi
		}
		;;
esac
