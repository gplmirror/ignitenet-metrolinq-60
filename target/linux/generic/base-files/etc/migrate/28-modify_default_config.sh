#!/bin/sh

#auto negoation: remove link speed config 
auto_nego=$(uci get ethernet.eth0.auto_nego 2> /dev/null)
[ "$auto_nego" == "1" ] && uci del ethernet.eth0.link_speed 2> /dev/null

auto_nego=$(uci get ethernet.eth1.auto_nego 2> /dev/null)
[ "$auto_nego" == "1" ] && uci del ethernet.eth1.link_speed 2> /dev/null

uci commit ethernet

#set memt vlan default config
mgmt_vlan=$(uci get network.wan.mgmt_vlan 2> /dev/null)
[ "$mgmt_vlan" == "" ] && uci set network.wan.mgmt_vlan=0

uci commit network.wan