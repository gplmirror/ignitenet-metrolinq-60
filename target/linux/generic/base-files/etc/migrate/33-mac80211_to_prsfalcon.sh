#!/bin/sh

. /lib/functions.sh
cfg_idx_60g=$(find_60g_idx)
[ "$cfg_idx_60g" == "1" ] && cfg_idx_5g="0" || cfg_idx_5g="1"

[ "$(uci get wireless.radio${cfg_idx_60g}.type 2>/dev/null)" != "mac80211" ] && return

dmg_mode=$(uci get wireless.radio${cfg_idx_60g}.mode 2>/dev/null)
prefer_pcp=$(uci get wireless.radio${cfg_idx_60g}.prefer_pcp 2>/dev/null)

case $dmg_mode in
	dmgauto)
		if [ "$prefer_pcp" == "1" ]; then
			new_mode='dmgpcp'
		else
			new_mode='dmgsta'
		fi
		;;
	dmgpcp)
		new_mode='dmgpcp'
		;;
	dmgsta)
		new_mode='dmgsta'
		;;
	*)
		new_mode='dmgpcp'
		;;
esac

[ "$new_mode" == "dmgpcp" ] && uci batch <<EOF
set wireless.radio${cfg_idx_60g}.mode='dmgpcp'
set wireless.radio${cfg_idx_60g}_1.mode='dmgpcp'
set network.bond.peer_intf_mode='1'
EOF

[ "$new_mode" == "dmgsta" ] && uci batch <<EOF
set wireless.radio${cfg_idx_60g}.mode='dmgsta'
set wireless.radio${cfg_idx_60g}_1.mode='dmgsta'
set network.bond.peer_intf_mode='0'
EOF

uci batch <<EOF
set wireless.radio${cfg_idx_60g}.type='prsfalcon'
set wireless.radio${cfg_idx_60g}_1.linkid_enable='1'
set wireless.radio${cfg_idx_60g}_1.backup_enable='1'
set wireless.radio${cfg_idx_60g}_1.backup_interface='wlan0'
set wireless.radio${cfg_idx_5g}_1.peer_intf='1'
set network.bond.primary='wlan1'
delete wireless.radio${cfg_idx_60g}.prefer_pcp
delete wireless.radio${cfg_idx_60g}.amsdu
delete wireless.radio${cfg_idx_60g}.tx_streams
delete wireless.radio${cfg_idx_60g}.rx_streams
delete wireless.radio${cfg_idx_60g}.beacon_int
delete wireless.radio${cfg_idx_60g}.frag
delete wireless.radio${cfg_idx_60g}.rts
delete wireless.radio${cfg_idx_60g}.sgi
delete wireless.radio${cfg_idx_60g}.stbc
delete wireless.radio${cfg_idx_60g}.ptmp_support
delete wireless.radio${cfg_idx_60g}_1.encryption
delete wireless.radio${cfg_idx_60g}_1.min_signal_allowed
delete wireless.radio${cfg_idx_60g}_1.wmm_enable
delete wireless.radio${cfg_idx_60g}_1.radius_mac_acl
delete wireless.radio${cfg_idx_60g}_1.auth_port
delete wireless.radio${cfg_idx_60g}_1.acl_enable
EOF

uci commit
