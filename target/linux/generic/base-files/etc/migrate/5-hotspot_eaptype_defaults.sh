# Add Hotspot EAP type default

hs_eaptype=`uci get hotspot.hotspot.hs_radius_eaptype 2> /dev/null`

[ -z "$hs_eaptype" ] && {

    uci set hotspot.hotspot.hs_radius_eaptype="chap"
    uci commit hotspot
}
