add_min_signal_defaults () {

    local min_signal_allowed

    config_get min_signal_allowed "$1" min_signal_allowed

    [ -z "$min_signal_allowed" ] &&  {
        uci set wireless."$1".min_signal_allowed=0
    }
}

# Add bandsteering defaults

bs=`uci get wireless.global.bandsteering 2> /dev/null`

[ -z "$bs" ] && {

    uci set wireless.global=settings
    uci set wireless.global.bandsteering=0
    uci commit wireless
}

# Add min signal allowed defaults

. /lib/functions.sh

config_load wireless
config_foreach add_min_signal_defaults wifi-iface
uci commit wireless
