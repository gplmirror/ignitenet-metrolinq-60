#!/bin/sh

. /lib/functions.sh
[ "$(find_60g_idx)" == "1" ] && cfg_idx_5g="0" || cfg_idx_5g="1"

sgi_5g=$(uci get wireless.radio${cfg_idx_5g}.sgi 2> /dev/null)
uci set wireless.radio${cfg_idx_5g}.sgi='1'
uci commit wireless
