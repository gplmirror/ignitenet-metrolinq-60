# Migrate from v1 to v2 11ac wireless keys

set_dev2_keys () {

    local htmode rtk_priv_bw rtk_ac_enable is_80Mhz

    config_get htmode "$1" htmode
    config_get rtk_priv_bw "$1" rtk_priv_bw
    config_get rtk_ac_enable "$1" rtk_ac_enable

    case "$htmode" in 
         "VHT"*)
            # Do nothing, we're already on devices v2
        ;;
        *40*)
            uci set wireless."$1".htmode="HT40"
        ;;
        *20*)
            uci set wireless."$1".htmode="HT20"
        ;;  
    esac

    case "$rtk_priv_bw" in 
        *80*)
            uci set wireless."$1".htmode="VHT80"
            uci set wireless."$1".rtk_priv_bw=""
            is_80Mhz=1
        ;; 
        5*|10*)

            if [ "$rtk_ac_enable" == "1" ]; then 
                uci set wireless."$1".htmode="VHT20"
            else 
                uci set wireless."$1".htmode="HT20"
            fi
        ;;
        *)
            uci set wireless."$1".rtk_priv_bw=""
        ;;
    esac

    if [ "$rtk_ac_enable" == "1" ] && [ -z "$is_80Mhz" ]; then
        case "$htmode" in 
            *40*)
                uci set wireless."$1".htmode="VHT40"
            ;;
            *20*)
                uci set wireless."$1".htmode="VHT20"
            ;;      
        esac
    fi

    uci set wireless."$1".rtk_ac_enable=""
}

. /lib/functions.sh

config_load wireless
config_foreach set_dev2_keys wifi-device
uci commit wireless
