#!/bin/sh

. /lib/functions.sh
cfg_idx_60g=$(find_60g_idx)

ptmp_support=$(uci get wireless.radio${cfg_idx_60g}.ptmp_support 2> /dev/null)
hw_ver=`cat /tmp/state/60g_hw_ver`

[ -z "$ptmp_support" ] && {
    uci set wireless.radio${cfg_idx_60g}.mode='dmgauto'
    uci set wireless.radio${cfg_idx_60g}_1.mode='dmgauto'
    uci set wireless.radio${cfg_idx_60g}.ptmp_support='1'
    uci commit wireless.radio${cfg_idx_60g}
}

if [ "$hw_ver" = "1.2A" ]; then
    uci set wireless.radio${cfg_idx_60g}.mode='dmgauto'
    uci set wireless.radio${cfg_idx_60g}_1.mode='dmgauto'
    uci set wireless.radio${cfg_idx_60g}.ptmp_support='0'
    uci commit wireless.radio${cfg_idx_60g}
fi