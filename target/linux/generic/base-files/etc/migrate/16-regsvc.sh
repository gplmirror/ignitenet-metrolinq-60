#!/bin/sh

REGSVC=$(uci get acn.register.url)
PROD=$(acc hw get product_name | cut -d'=' -f2)

CLOUD="cloud.ignitenet.com"
IRAC="IRAC"

if [[ ${REGSVC#*$CLOUD} != $REGSVC ]] && [[ ${PROD#*$IRAC} == $PROD ]]; then
	uci set acn.register.url=https://regsvc.ignitenet.com/register
	uci commit
fi

