#!/bin/sh

uci batch <<EOF
set rssi.sys.led1_lower_bound='20'
set rssi.sys.led2_lower_bound='35'
set rssi.sys.led3_lower_bound='45'
set rssi.sys.led4_lower_bound='65'
delete rssi.sys.usb_hang_restart
EOF

uci commit
