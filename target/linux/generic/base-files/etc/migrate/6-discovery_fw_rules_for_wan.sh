# Allow user to disable discovery daemon
discovery_section=`uci get acn.discovery 2> /dev/null`

[ -z "$discovery_section" ] && {

    uci set acn.discovery=acn
    uci set acn.discovery.enabled="1"
    uci commit acn
}

# Add discoveryd dependency to ucitrack
uci batch <<-EOF >/dev/null 2>/dev/null
delete ucitrack.@acn[-1]
add ucitrack acn
add_list ucitrack.@acn[-1].affects=reset_btn
add_list ucitrack.@acn[-1].affects=mgmtd
add_list ucitrack.@acn[-1].affects=rssi
add_list ucitrack.@acn[-1].affects=discoveryd
EOF

uci commit ucitrack

# Add firewall rules for discovery over the WAN
local rule="$(uci show firewall | grep Allow-Broadcast-Discovery)"

if [ -z "$rule" ]; then
    uci batch <<-EOF >/dev/null 2>/dev/null
        add firewall rule
        set firewall.@rule[-1].name="Allow-Broadcast-Discovery"
        set firewall.@rule[-1].src="wan"
        set firewall.@rule[-1].proto="udp"
        set firewall.@rule[-1].dest_port="17371"
        set firewall.@rule[-1].dest_ip="255.255.255.255"
        set firewall.@rule[-1].target="ACCEPT"
        set firewall.@rule[-1].family="ipv4"
EOF
fi

rule="$(uci show firewall | grep Allow-Multicast-Discovery)"

if [ -z "$rule" ]; then
    uci batch <<-EOF >/dev/null 2>/dev/null
        add firewall rule
        set firewall.@rule[-1].name="Allow-Multicast-Discovery"
        set firewall.@rule[-1].src="wan"
        set firewall.@rule[-1].proto="udp"
        set firewall.@rule[-1].dest_port="17371"
        set firewall.@rule[-1].dest_ip="233.89.188.1"
        set firewall.@rule[-1].target="ACCEPT"
        set firewall.@rule[-1].family="ipv4"
EOF
fi

uci commit firewall
