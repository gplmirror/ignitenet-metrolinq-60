#!/bin/sh

. /lib/functions.sh
cfg_idx_60g=$(find_60g_idx)

radio_mode=$(uci get wireless.radio${cfg_idx_60g}.mode 2> /dev/null)
if [ "$radio_mode" == "dmgpcp" -o "$radio_mode" == "dmgsta" ]; then
  client_isolation=$(uci get wireless.radio${cfg_idx_60g}.client_isolation 2> /dev/null)
  [ "$client_isolation" == "" ] && uci set wireless.radio${cfg_idx_60g}.client_isolation=0
fi