#!/bin/sh

. /lib/functions.sh
cfg_idx_60g=$(find_60g_idx)
[ "$cfg_idx_60g" == "1" ] && cfg_idx_5g="0" || cfg_idx_5g="1"

country_code=$(uci get wireless.radio0.country 2> /dev/null)
txpower_5g=$(uci get wireless.radio${cfg_idx_5g}.txpower 2> /dev/null)
txpower_60g=$(uci get wireless.radio${cfg_idx_60g}.txpower 2> /dev/null)

if [ "$country_code" == "JP" ]; then
    [ $txpower_5g -gt 10 ] && uci set wireless.radio${cfg_idx_5g}.txpower='4'
    [ $txpower_60g -gt 7 ] && uci set wireless.radio${cfg_idx_60g}.txpower='7'
    uci commit wireless
fi

