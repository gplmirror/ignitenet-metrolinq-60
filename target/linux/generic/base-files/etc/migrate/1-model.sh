#!/bin/sh

MID=`acc hw get product_name |cut -d'=' -f2`

# Default swconfig for all devices: assume first two eth ports
# are in different networks (one wan, one lan)
if   [ -x /sbin/swconfig ]; then
    uci batch <<EOF
set network.eth0=switch
set network.eth0.name=eth0
set network.eth0.reset=1
set network.eth0.enable_vlan=1
set network.eth0_1=switch_vlan
set network.eth0_1.device=eth0
set network.eth0_1.vlan=1
set network.eth0_1.ports="0 6t"
set network.eth0_2=switch_vlan
set network.eth0_2.device=eth0
set network.eth0_2.vlan=2
set network.eth0_2.ports="4 6t"
set network.sp0=switch_port
set network.sp0.port=0
set network.sp0.pvid=1
set network.sp1=switch_port
set network.sp1.port=4
set network.sp1.pvid=2
EOF
fi

#
# Modifications for SunSpot (3 eth ports) and Spark
#
case $MID in
"ML-60"*)
if   [ ! -x /sbin/swconfig ]; then
uci batch <<EOF
set ethernet.eth1=eth_port
set ethernet.eth1.status=1
set ethernet.eth1.auto_nego=1
set ethernet.eth1.link_speed=1Gbps
set ethernet.eth1.full_duplex=1
EOF
fi
;;

"SS-"*|"EAP1282"*)

# Add extra ethernet port
uci batch <<EOF
set ethernet.eth2=eth_port
set ethernet.eth2.status=1
set ethernet.eth2.auto_nego=1
set ethernet.eth2.link_speed=100Mbps
set ethernet.eth2.full_duplex=1
EOF

# Set default ports for lan network 
uci set network.lan.ifname="eth1 eth0.2 eth0.3"

# Set default system information
uci batch <<EOF
set system.active=active
set system.active.wifi_led_low_active=0
set system.active.rssi_led_low_active=0
EOF

uci batch <<EOF
set network.eth0_2.ports="4 1 6t"   
set network.sp2=switch_port
set network.sp2.port=1
set network.sp2.pvid=2
EOF

;;

"IR"*|"SP-"*)
# Add extra ethernet port
uci batch <<EOF
set ethernet.eth2=eth_port
set ethernet.eth2.status=1
set ethernet.eth2.auto_nego=1
set ethernet.eth2.link_speed=100Mbps
set ethernet.eth2.full_duplex=1
EOF

# Set default ports for lan network 
uci set network.lan.ifname="eth1 eth0.2 eth0.3"

# Set default system information
uci batch <<EOF
set system.active=active
set system.active.wifi_led_low_active=0
set system.active.rssi_led_low_active=0
EOF

uci batch <<EOF
set network.eth0_2.ports="4 2 6t"
set network.sp2=switch_port
set network.sp2.port=2
set network.sp2.pvid=2
EOF

;;

esac
# END case $MID for SunSpot Spark


# Modifications for Spark AC750 (one Spatial Stream only on 5 Ghz radio)
case $MID in
    "IRAC750"*|"SP-AC750"*)
        # Set default tx/rx stream 
        uci set wireless.radio0.rx_streams=1
        uci set wireless.radio0.tx_streams=1
        ;;
esac
# END Spark 

# Modifications for Fusion ethernet port
case $MID in
    "FN-"*)
        uci set network.eth0_1.ports="4 6t"
        uci set network.eth0_2.ports="0 6t"
        uci set network.sp0.port=4
        uci set network.sp1.port=0
        ;;
esac
# END Fusion

# Modifications for Metrolinq network
case $MID in
    "ML-60"*)
	# create default settings for wlan1
	uci set wireless.radio1=wifi-device
	uci set wireless.radio1.type=mac80211
	uci set wireless.radio1.channel=1
	uci set wireless.radio1.hwmode=11ad
	uci set wireless.radio1.htmode=2160
	uci set wireless.radio1.country=US
	uci set wireless.radio1.mpdu=1
	uci set wireless.radio1.amsdu=1
	uci set wireless.radio1.amsdu_802_3=1
	uci set wireless.radio1.data_rate=auto
	uci set wireless.radio1.mode=dmgauto
	uci set wireless.radio1.prefer_pcp=1
	uci set wireless.radio1.tx_streams=1
	uci set wireless.radio1.rx_streams=1
	uci set wireless.radio1.path="platform/xhci-hcd/usb2/2-1"
	uci set wireless.radio1.disabled=0
	uci set wireless.radio1.beacon_int=100
	uci set wireless.radio1.backup_enable=0
	uci set wireless.radio1.txpower=8
	uci set wireless.radio1.ack=10
	uci set wireless.radio1.frag=2346
	uci set wireless.radio1.rts=2347
	uci set wireless.radio1.sgi=0
	uci set wireless.radio1.stbc=0
	uci set wireless.radio1.supported_freq=60
	uci set wireless.radio1_1=wifi-iface
	uci set wireless.radio1_1.device=radio1
	uci set wireless.radio1_1.network=wan
	uci set wireless.radio1_1.mode=dmgauto
	uci set wireless.radio1_1.encryption=none
	uci set wireless.radio1_1.encryption_60g=0
	uci set wireless.radio1_1.ssid=IgniteNet1-1
	uci set wireless.radio1_1.disabled=0
	uci set wireless.radio1_1.min_signal_allowed=0
	uci set wireless.radio1_1.created=1
	uci set wireless.radio1_1.wmm_enable=0
	uci set wireless.radio1_1.radius_mac_acl=0
	uci set wireless.radio1_1.auth_port=1812
	uci set wireless.radio1_1.acl_enable=0
	uci set wireless.radio1_1.limit_up_enable=0
	uci set wireless.radio1_1.limit_down_enable=0

        if   [ -x /sbin/swconfig ]; then
            uci set network.eth0_1.ports="0 4 6t"
            uci set network.eth0_2.ports=""
            uci set network.sp0.port=4
            uci set network.sp1.port=0
            uci set network.lan.ifname=""
            uci set network.wan.type=bridge
            uci set network.wan.ifname="eth0.1 eth0.2"
            uci set network.eth0_2.vlan=1
            uci set network.sp1.pvid=1
            uci set wireless.radio0_1.network="wan"
            uci set wireless.radio0.disabled=1
            uci set wireless.radio0.ptp=1
        else
            uci set network.lan.ifname=""
            uci set network.wan.type=bridge
            uci set network.wan.ifname="eth0 eth1"
            uci set network.wan.inet_src="eth0"
            uci set wireless.radio0_1.network="wan"
            uci set wireless.radio0.disabled=0
            uci set wireless.radio0.ptp=1
        fi
        ;;
esac
# END Metrolinq

# commit changes
uci commit
