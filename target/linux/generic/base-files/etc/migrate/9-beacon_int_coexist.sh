#!/bin/sh

# 20/40MHz Coexist is only for 2.4G
MID=`acc hw get product_name |cut -d'=' -f2`
case $MID in
    "SS-N300"*|"OAP7235RL"*|"SP-N300"*)
        # SunSpot N300 and SkyFire N300 and Spark N300: 2.4 radio only
        noscan=`uci get wireless.radio0.noscan 2> /dev/null`
        [ -z $noscan ] && uci set wireless.radio0.noscan="0"
    ;;
    
    "IRAC750"*|"EAP1282"*|"OAP1232RL"*|"SP-AC750"*)
        # SunSpot AC1200 and SkyFire AC1200 and Spark AC750: Dual
        noscan=`uci get wireless.radio1.noscan 2> /dev/null`
        [ -z $noscan ] && uci set wireless.radio1.noscan="0"
    ;;
esac

#Beacon Interval
beacon_int=`uci get wireless.radio0.beacon_int 2> /dev/null`
set_dev2_beacon () {
    [ -z $beacon_int ] && {
        uci set wireless."$1".beacon_int="100"
    }
}

. /lib/functions.sh

config_load wireless
config_foreach set_dev2_beacon wifi-device
uci commit wireless
