#!/bin/sh

. /lib/functions.sh
cfg_idx_60g=$(find_60g_idx)

opmode=$(uci get wireless.radio${cfg_idx_60g}.mode 2> /dev/null)
ptmp_support=$(uci get wireless.radio${cfg_idx_60g}.ptmp_support 2> /dev/null)

if [ "$opmode" = "dmgauto" -a "$ptmp_support" == "1" ]; then
    uci set wireless.radio${cfg_idx_60g}.prefer_pcp='1'
    uci commit wireless.radio${cfg_idx_60g}
fi