#!/bin/sh

ack_max_value_5g=255
ack_max_value_60g=28

uci set wireless.radio0.ack=$ack_max_value_5g
uci set wireless.radio1.ack=$ack_max_value_60g
uci commit
