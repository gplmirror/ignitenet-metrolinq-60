
set_hwmode () {
    local supported_freq htmode

    config_get supported_freq "$1" supported_freq
    config_get htmode "$1" htmode

    if [ "$supported_freq" == "5" ]; then
      uci set wireless."$1".hwmode="11ac"
      case "$htmode" in 
        "HT20")
            uci set wireless."$1".htmode="VHT20"
        ;;
        "HT40")
            uci set wireless."$1".htmode="VHT40"
        ;; 
      esac
    fi
}

. /lib/functions.sh

config_load wireless
config_foreach set_hwmode wifi-device
uci commit wireless
