#!/bin/sh

. /lib/functions.sh
cfg_idx_60g=$(find_60g_idx)
[ "$cfg_idx_60g" == "1" ] && cfg_idx_5g="0" || cfg_idx_5g="1"

product_name=`acc hw get product_name |cut -d'=' -f2`
txpower_5g=$(uci get wireless.radio${cfg_idx_5g}.txpower 2> /dev/null)
txpower_60g=$(uci get wireless.radio${cfg_idx_60g}.txpower 2> /dev/null)

MID=`acc hw get product_name |cut -d'=' -f2`
PDN="-TH"

if [ "$MID" != "${MID%$PDN*}" ]; then
    uci set wireless.radio0.country=TH
    uci set wireless.radio1.country=TH
    if [ "$product_name" == "ML-60-19-TH" ]; then
        [ $txpower_5g -gt 11 ] && uci set wireless.radio${cfg_idx_5g}.txpower='11'
        [ $txpower_60g -gt 4 ] && uci set wireless.radio${cfg_idx_60g}.txpower='4'
    fi
    if [ "$product_name" == "ML-60-35-TH" ]; then
        [ $txpower_5g -gt 5 ] && uci set wireless.radio${cfg_idx_5g}.txpower='5'
        [ $txpower_60g -gt 2 ] && uci set wireless.radio${cfg_idx_60g}.txpower='2'
    fi
    uci commit wireless
fi

