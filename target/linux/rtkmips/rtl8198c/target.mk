SUBTARGET:=rtl8198c
BOARDNAME:=RTL8198c based boards
#CPU_TYPE:=mips32r2
CPU_TYPE:=24kec
DEFAULT_PACKAGES +=
# device type force to non router
# this gets global for all 8198c products now
DEVICE_TYPE:=other

define Target/Description
	RTL8198C based boards
endef
