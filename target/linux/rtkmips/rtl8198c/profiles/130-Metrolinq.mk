#
# Copyright (C) 2006-2008 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

define Profile/Metrolinq
  NAME:=Metrolinq board
  PACKAGES:=-wpad-mini
  KCONFIG:=CONFIG_RTL_8198C_METROLINQ CONFIG_USE_PCIE_SLOT_1
  FWNAMEPREFIX:=MetroLinq
endef

define Profile/Metrolinq/Description
	Metrolinq ML-60-36 ML-60-19 support
endef

$(eval $(call Profile,Metrolinq))
