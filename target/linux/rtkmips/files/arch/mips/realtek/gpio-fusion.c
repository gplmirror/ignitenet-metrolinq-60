/*
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) 2007 Aurelien Jarno <aurelien@aurel32.net>
 */


#define CONFIG_GPIO_PIN_MODE_2 1

#include <linux/init.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/spinlock.h>
#include <linux/gpio.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
//#include "bspchip.h"
#include <asm/mach-realtek/bspchip.h>

extern struct proc_dir_entry proc_root;

static struct timer_list watchdog_timer;
static char en_wdg_pulse='1';
#ifndef CONFIG_GPIO_PIN_MODE_2
extern int gPort3Test;
#endif

static unsigned int rtl819x_gpio_mux(u32 pin, u32 *value, u32 *address );

static DEFINE_SPINLOCK(rtl819x_gpio_lock);

static int __rtl819x_gpio_get_value(unsigned gpio)
{
	unsigned int data;

	data = (__raw_readl((void __iomem*)BSP_GPIO_DAT_REG(gpio)) >> BSP_GPIO_BIT(gpio) ) & 1;

	return data;
}

static int rtl819x_gpio_get_value(struct gpio_chip *chip, unsigned pin)
{
	if (pin >= BSP_GPIO_PIN_MAX)
		return -1;

	return __rtl819x_gpio_get_value(pin);
}

static int __rtl819x_gpio_set_value(unsigned pin, int value)
{
	unsigned int data;

	/*
	 * Debugging/tracking down who are messing with USB port power.
	 * Logging only USB host related GPIO stuff.
	 *
	 *  o XHCI driver
	 *  o peraso driver
	 *  o init/boot on start up
	 *  o usbwatchd USB state monitor
	 */
	if (pin == 60) {
		printk("Trigger GPIO60: %d -> gpio60\n", value);
	}

	data = __raw_readl((void __iomem*)BSP_GPIO_DAT_REG(pin));

	#if 0
	printk("%s(%d)xxx 0x%08x\n",__FUNCTION__,__LINE__,
		__raw_readl((void __iomem*)PINMUX_SEL_REG(_ata_rtl8972d_v1xx_leds_gpio[i].gpio)));
	printk("%s(%d)xxx 0x%08x\n",__FUNCTION__,__LINE__,
		__raw_readl((void __iomem*)BSP_GPIO_CNR_REG(_ata_rtl8972d_v1xx_leds_gpio[i].gpio)));
	printk("%s(%d)xxx 0x%08x\n",__FUNCTION__,__LINE__,
		__raw_readl((void __iomem*)BSP_GPIO_DIR_REG(_ata_rtl8972d_v1xx_leds_gpio[i].gpio)));

	printk("%s(%d)xxx pin=%d, val=%d\n",__FUNCTION__,__LINE__,pin,value);
	printk("%s(%d)xxx 0x%08x\n",__FUNCTION__,__LINE__,REG32(BSP_GPIO_DAT_REG(pin)));
	#endif

	if (value == 0)
		data &= ~(1 << BSP_GPIO_BIT(pin));
	else
		data |= (1 << BSP_GPIO_BIT(pin));

	//printk("%s(%d)yyy 0x%08x\n",__FUNCTION__,__LINE__,data);

	__raw_writel(data, (void __iomem*)BSP_GPIO_DAT_REG(pin));

	//printk("%s(%d)xxx 0x%08x\n",__FUNCTION__,__LINE__,
	//	__raw_readl((void __iomem*)BSP_GPIO_DAT_REG(pin)));

	return 0;
}

static void rtl819x_gpio_set_value(struct gpio_chip *chip,
				  unsigned pin, int value)
{
	unsigned long flags;

	if (pin >= BSP_GPIO_PIN_MAX)
		return;

	spin_lock_irqsave(&rtl819x_gpio_lock, flags);
	__rtl819x_gpio_set_value(pin, value);
	spin_unlock_irqrestore(&rtl819x_gpio_lock, flags);
}

static int rtl819x_gpio_direction_input(struct gpio_chip *chip,
				       unsigned pin)
{
	unsigned long flags;

	if (pin >= BSP_GPIO_PIN_MAX)
		return -1;

	spin_lock_irqsave(&rtl819x_gpio_lock, flags);

	/* 0 : input */
	__raw_writel(__raw_readl((void __iomem*)BSP_GPIO_DIR_REG(pin)) & ~(1 << BSP_GPIO_BIT(pin)), 
					(void __iomem*)BSP_GPIO_DIR_REG(pin));

	spin_unlock_irqrestore(&rtl819x_gpio_lock, flags);

	return 0;
}

static int rtl819x_gpio_direction_output(struct gpio_chip *chip,
					unsigned pin, int value)
{
	unsigned long flags;

	if (pin >= BSP_GPIO_PIN_MAX)
		return -1;

	spin_lock_irqsave(&rtl819x_gpio_lock, flags);

	__raw_writel(__raw_readl((void __iomem*)BSP_GPIO_DIR_REG(pin)) | (1 << BSP_GPIO_BIT(pin)), 
					(void __iomem*)BSP_GPIO_DIR_REG(pin) );

	spin_unlock_irqrestore(&rtl819x_gpio_lock, flags);

	return 0;
}


static struct gpio_chip rtl819x_gpio_peripheral = {

	.label				= "rtl819x_gpio",
	.get				= rtl819x_gpio_get_value,
	.set				= rtl819x_gpio_set_value,
	.direction_input	= rtl819x_gpio_direction_input,
	.direction_output	= rtl819x_gpio_direction_output,
	.base				= 0,
};

void rtl819x_gpio_pin_enable(u32 pin)
{
	unsigned long flags;
	unsigned int mask  = 0;
	unsigned int mux =0;
	unsigned int mux_reg;
	unsigned int val;
	


    if (pin >= TOTAL_PIN_MAX)
		return;

	spin_lock_irqsave(&rtl819x_gpio_lock, flags);

	/* pin MUX1 */
	mask = rtl819x_gpio_mux(pin,&val,&mux_reg);

	//mux  = __raw_readl((void __iomem*) BSP_PINMUX_SEL_REG(pin));
	mux  = __raw_readl((void __iomem*)mux_reg);

	//if (mask != 0 && (mux & mask) == 0)
	if (mask != 0)
	    __raw_writel( ((mux&(~mask)) | (val)), (void __iomem*)mux_reg);

	/* 0 as BSP_GPIO pin */
    if(pin < BSP_GPIO_PIN_MAX)
    {
        __raw_writel(__raw_readl((void __iomem*)BSP_GPIO_CNR_REG(pin)) & ~(1<<BSP_GPIO_BIT(pin)), 
					(void __iomem*)BSP_GPIO_CNR_REG(pin));
    }

	spin_unlock_irqrestore(&rtl819x_gpio_lock, flags);

}

void rtl819x_gpio_pin_disable(u32 pin)
{
	unsigned long flags;

	spin_lock_irqsave(&rtl819x_gpio_lock, flags);

	/* 1 as peripheral pin */
	__raw_writel(__raw_readl((void __iomem*)BSP_GPIO_CNR_REG(pin)) | (1<<BSP_GPIO_BIT(pin)), 
					(void __iomem*)BSP_GPIO_CNR_REG(pin));

	spin_unlock_irqrestore(&rtl819x_gpio_lock, flags);
}

void rtl819x_gpio_pin_set_val(u32 pin, int val)
{
	unsigned long flags;

	spin_lock_irqsave(&rtl819x_gpio_lock, flags);
	__rtl819x_gpio_set_value(pin, val);
	spin_unlock_irqrestore(&rtl819x_gpio_lock, flags);
}

int rtl819x_gpio_pin_get_val(u32 pin)
{
	return __rtl819x_gpio_get_value(pin);
}

static void watchdog_function(unsigned long data)
{
	if(en_wdg_pulse == '1') {
		if (watchdog_timer.data==0)
		{
			rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_E1, 1);
			watchdog_timer.data=1;
		} else {
			rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_E1, 0);
			watchdog_timer.data=0;
		}
	}

	mod_timer(&watchdog_timer, jiffies + HZ);
}

static int led_check_read_proc(struct seq_file *s, void *v)
{
#if 0 //For debug
	seq_printf(s,"REG32(0xb8000008): 0x%08x\n\n",REG32(0xb8000008));
	seq_printf(s,"PIN_MUX_SEL_1: 	 0x%08x\n",  REG32(BSP_PIN_MUX_SEL1));
	seq_printf(s,"PIN_MUX_SEL_2: 	 0x%08x\n",  REG32(BSP_PIN_MUX_SEL2));
	seq_printf(s,"PIN_MUX_SEL_3: 	 0x%08x\n",  REG32(BSP_PIN_MUX_SEL3));
	seq_printf(s,"PIN_MUX_SEL_4: 	 0x%08x\n",  REG32(BSP_PIN_MUX_SEL4));
	seq_printf(s,"PIN_MUX_SEL_5: 	 0x%08x\n\n",REG32(BSP_PIN_MUX_SEL5));
	seq_printf(s,"BSP_PABCD_CNR: 	 0x%08x\n",  REG32(BSP_PABCD_CNR));
	seq_printf(s,"BSP_PABCD_DIR: 	 0x%08x\n",  REG32(BSP_PABCD_DIR));
	seq_printf(s,"BSP_PABCD_DAT: 	 0x%08x\n",  REG32(BSP_PABCD_DAT));
	seq_printf(s,"BSP_PEFGH_CNR: 	 0x%08x\n",  REG32(BSP_PEFGH_CNR));
	seq_printf(s,"BSP_PEFGH_DIR: 	 0x%08x\n",  REG32(BSP_PEFGH_DIR));
	seq_printf(s,"BSP_PEFGH_DAT: 	 0x%08x\n\n",REG32(BSP_PEFGH_DAT));
#endif

#ifdef CONFIG_GPIO_PIN_MODE_2
	seq_printf(s,"wan 	 B6: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_B6));
	seq_printf(s,"lan 	 B2: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_B2));
	seq_printf(s,"rssi 	 F0: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_F0));
	seq_printf(s,"rssi 	 F1: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_F1));
	seq_printf(s,"rssi 	 F2: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_F2));
	seq_printf(s,"en_wdg              F4: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_F4));
	seq_printf(s,"en_usb_power        F7: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_F7));
	seq_printf(s,"en_usb_over_current H0: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_H0));
/*
	seq_printf(s,"Ext Antenna Switch Control Pin B7: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_B7));
	seq_printf(s,"Ext Antenna Switch Control Pin E0: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_E0));
	seq_printf(s,"Ext Antenna Switch Control Pin C0: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_C0));
	seq_printf(s,"Ext Antenna Switch Control Pin H7: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_H7));
	seq_printf(s,"Ext Antenna Switch Control Pin C7: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_C7));
	seq_printf(s,"Ext Antenna Switch Control Pin H6: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_H6));
	seq_printf(s,"Ext Antenna Switch Control Pin F6: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_F6));
*/
	seq_printf(s,"pulse	              E1: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_E1));
#else
	seq_printf(s,"wps 	E2: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_E2));
	seq_printf(s,"wan 	B2: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_B2));
	seq_printf(s,"lan1 	B3: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_B3));
	seq_printf(s,"lan2 	B4: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_B4));
	seq_printf(s,"lan3 	F1: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_F1));
	seq_printf(s,"lan4 	B6: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_B6));
	seq_printf(s,"pulse	E1: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_E1));
#endif


	return 0;
}

static int led_check_write_proc(struct file *file, const char *buffer,
                unsigned long count, void *data)
{
	char led_check;

	if (count < 2)
		return -EFAULT;

	if (buffer && !copy_from_user(&led_check, buffer, 1)) {
		switch(led_check)
		{
			case 'o':
#ifdef CONFIG_GPIO_PIN_MODE_2
				REG32(BSP_PIN_MUX_SEL3) = REG32(BSP_PIN_MUX_SEL3) & (0x036d9249);
#else
				REG32(BSP_PIN_MUX_SEL3) = REG32(BSP_PIN_MUX_SEL3) & (0x18019249);
				gPort3Test=0;
#endif
				break;
			case 'i':
#ifdef CONFIG_GPIO_PIN_MODE_2
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_B6); // WAN
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_B2); // LAN
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_F0); // RSSI
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_F1); // RSSI
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_F2); // RSSI
/*
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_B7); // Ext Antenna Switch Control Pin
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_E0); // Ext Antenna Switch Control Pin
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_C0); // Ext Antenna Switch Control Pin
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_H7); // Ext Antenna Switch Control Pin
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_C7); // Ext Antenna Switch Control Pin
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_H6); // Ext Antenna Switch Control Pin
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_F6); // Ext Antenna Switch Control Pin
*/
				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_B6, 1);
				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_B2, 1);
				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_F0, 1);
				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_F1, 1);
				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_F2, 1);
/*
				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_B7, 1);
				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_E0, 1);
				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_C0, 1);
				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_H7, 1);
				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_C7, 1);
				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_H6, 1);
				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_F6, 1);
*/
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B6, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B2, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F0, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F1, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F2, 1);
/*
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B7, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_E0, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_C0, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_H7, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_C7, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_H6, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F6, 0);
*/
#else
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_E2); // WPS
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_B2); // wan
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_B3); // lan1
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_B4); // lan2
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_F1); // lan3
				rtl819x_gpio_pin_enable(BSP_GPIO_PIN_B6); // lan4

				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_E2, 1);
				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_B2, 1);
				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_B3, 1);
				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_B4, 1);
				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_F1, 1);
				rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_B6, 1);

				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_E2, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B2, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B3, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B4, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F1, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B6, 1);
				gPort3Test=1;
#endif
				break;
			case '1':
#ifdef CONFIG_GPIO_PIN_MODE_2
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B6, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B2, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F0, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F1, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F2, 1);
/*
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B7, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_E0, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_C0, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_H7, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_C7, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_H6, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F6, 1);
*/
#else
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_E2, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B2, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B3, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B4, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F1, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B6, 1);
#endif
				break;
			case '0':
#ifdef CONFIG_GPIO_PIN_MODE_2
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B6, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B2, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F0, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F1, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F2, 0);
/*
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B7, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_E0, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_C0, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_H7, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_C7, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_H6, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F6, 0);
*/
#else
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_E2, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B2, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B3, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B4, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F1, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B6, 0);
#endif
				break;
#ifdef CONFIG_GPIO_PIN_MODE_2
			case '3':
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B6, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B2, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F0, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F1, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F2, 0);
/*
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B7, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_E0, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_C0, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_H7, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_C7, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_H6, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F6, 1);
*/
				break;
			case '4':
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B6, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B2, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F0, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F1, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F2, 1);
/*
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B7, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_E0, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_C0, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_H7, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_C7, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_H6, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F6, 0);
*/
				break;
			case 'a':
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F0, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F1, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F2, 1);
				break;
			case 'b':
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F0, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F1, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F2, 1);
				break;
			case 'c':
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F0, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F1, 0);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F2, 1);
				break;
			case 'd':
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F0, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F1, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F2, 0);
				break;
			case 'e':
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F0, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F1, 1);
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F2, 1);
				break;
			case 'w':
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B6, 0);
				break;
			case 'l':
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_B2, 0);
				break;
#endif
			default:
				printk("Invaild Value!\n");
				return -EFAULT;
		}
		return count;
	}
	return -EFAULT;
}

int led_check_single_open(struct inode *inode, struct file *file)
{
        return(single_open(file, led_check_read_proc, NULL));
}

static ssize_t led_check_single_write(struct file * file, const char __user * userbuf,
             size_t count, loff_t * off)
{
    return led_check_write_proc(file, userbuf,count, off);
}

static const struct file_operations led_check_proc_fops = {
	.open 		= led_check_single_open,
	.write 		= led_check_single_write,
	.read 		= seq_read,
	.llseek 	= seq_lseek,
	.release 	= single_release,
};

static int en_wdg_pulse_read_proc(struct seq_file *s, void *v)
{
	seq_printf(s,"en_wdg_pulse:%c\n", en_wdg_pulse);
	return 0;
}

static int en_wdg_pulse_write_proc(struct file *file, const char *buffer,
                unsigned long count, void *data)
{
	if (count < 2)
		return -EFAULT;

	if (copy_from_user(&en_wdg_pulse, buffer, 1)) {
		return -EFAULT;
	}
	return count;
}

int en_wdg_pulse_single_open(struct inode *inode, struct file *file)
{
        return(single_open(file, en_wdg_pulse_read_proc, NULL));
}

static ssize_t en_wdg_pulse_single_write(struct file * file, const char __user * userbuf,
             size_t count, loff_t * off)
{
    return en_wdg_pulse_write_proc(file, userbuf,count, off);
}

static const struct file_operations en_wdg_pulse_proc_fops = {
	.open 		= en_wdg_pulse_single_open,
	.write 		= en_wdg_pulse_single_write,
	.read 		= seq_read,
	.llseek 	= seq_lseek,
	.release 	= single_release,
};


#ifdef CONFIG_GPIO_PIN_MODE_2
//----------------------------------------------
// Enable usb power
//----------------------------------------------
static int en_usb_power_read_proc(struct seq_file *s, void *v)
{
	seq_printf(s,"usb power F7: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_F7));
	return 0;
}

static int en_usb_power_write_proc(struct file *file, const char *buffer,
                unsigned long count, void *data)
{
	char en_usb_power;

	if (count < 2)
		return -EFAULT;

	if (buffer && !copy_from_user(&en_usb_power, buffer, 1)) {
		switch(en_usb_power)
		{
			case '1':
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F7, 0); //low active
				break;
			case '0':
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F7, 1);
				break;
			default:
				printk("Invaild Value!\n");
				return -EFAULT;
		}
		return count;
	}
	return -EFAULT;
}

int en_usb_power_single_open(struct inode *inode, struct file *file)
{
        return(single_open(file, en_usb_power_read_proc, NULL));
}

static ssize_t en_usb_power_single_write(struct file * file, const char __user * userbuf,
             size_t count, loff_t * off)
{
    return en_usb_power_write_proc(file, userbuf,count, off);
}

static const struct file_operations en_usb_power_proc_fops = {
	.open 		= en_usb_power_single_open,
	.write 		= en_usb_power_single_write,
	.read 		= seq_read,
	.llseek 	= seq_lseek,
	.release 	= single_release,
};

//----------------------------------------------
// Enable usb over current
//----------------------------------------------
static int en_usb_over_current_read_proc(struct seq_file *s, void *v)
{
	seq_printf(s,"usb over current H0: %d\n", rtl819x_gpio_pin_get_val(BSP_GPIO_PIN_H0));
	return 0;
}

static int en_usb_over_current_write_proc(struct file *file, const char *buffer,
                unsigned long count, void *data)
{
	char en_usb_over_current;

	if (count < 2)
		return -EFAULT;

	if (buffer && !copy_from_user(&en_usb_over_current, buffer, 1)) {
		switch(en_usb_over_current)
		{
			case '1':
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_H0, 0); //low active
				break;
			case '0':
				rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_H0, 1);
				break;
			default:
				printk("Invaild Value!\n");
				return -EFAULT;
		}
		return count;
	}
	return -EFAULT;
}

int en_usb_over_current_single_open(struct inode *inode, struct file *file)
{
        return(single_open(file, en_usb_over_current_read_proc, NULL));
}

static ssize_t en_usb_over_current_single_write(struct file * file, const char __user * userbuf,
             size_t count, loff_t * off)
{
    return en_usb_over_current_write_proc(file, userbuf,count, off);
}

static const struct file_operations en_usb_over_current_proc_fops = {
	.open 		= en_usb_over_current_single_open,
	.write 		= en_usb_over_current_single_write,
	.read 		= seq_read,
	.llseek 	= seq_lseek,
	.release 	= single_release,
};
#endif

static int __init rtl819x_gpio_peripheral_init(void)
{
	int err;

	rtl819x_gpio_peripheral.ngpio = BSP_GPIO_PIN_MAX;
	err = gpiochip_add(&rtl819x_gpio_peripheral);

	if (err)
		panic("cannot add rtl89x BSP_GPIO chip, error=%d", err);

	//Let HW RESET PIN Enable (0:Enable, 1:Disable)
	REG32(0xb8000008) = REG32(0xb8000008) &(~(1<<5));
	rtl819x_gpio_pin_enable(BSP_GPIO_PIN_B5);
	rtl819x_gpio_direction_input(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_B5);

	//Initial watchdog enable pin (0:Enable 1:Disable)
	rtl819x_gpio_pin_enable(BSP_GPIO_PIN_F4);
	rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_F4, 1);
	rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F4, 1);

	//Initial pulse pin value
	rtl819x_gpio_pin_enable(BSP_GPIO_PIN_E1);
	rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_E1, 0);
	rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_E1, 0);

	//Create pulse for external watchdog
	init_timer(&watchdog_timer);
	watchdog_timer.expires = jiffies + HZ;
	watchdog_timer.data = 0;
	watchdog_timer.function = &watchdog_function;
	mod_timer(&watchdog_timer, jiffies + HZ);

	proc_create("led_check", 0, NULL, &led_check_proc_fops);
	proc_create("en_wdg_pulse", 0, NULL, &en_wdg_pulse_proc_fops);

#ifdef CONFIG_GPIO_PIN_MODE_2
	//Initial usb power (0:Enable 1:Disable)
	rtl819x_gpio_pin_enable(BSP_GPIO_PIN_F7);
	rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_F7, 0);
	rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F7, 1);

	//USB3.0 POWER	
	rtl819x_gpio_pin_enable(BSP_GPIO_PIN_H4);
	rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_H4, 1);
	rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_H4, 1);	

	//Initial usb over current (0:Enable 1:Disable)
	rtl819x_gpio_pin_enable(BSP_GPIO_PIN_H0);
	rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_H0, 0);
	rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_H0, 0);

	//Initial rssi (0:Enable 1:Disable)
	rtl819x_gpio_pin_enable(BSP_GPIO_PIN_F0);
	rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_F0, 1);
	rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F0, 1);
	rtl819x_gpio_pin_enable(BSP_GPIO_PIN_F1);
	rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_F1, 1);
	rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F1, 1);
	rtl819x_gpio_pin_enable(BSP_GPIO_PIN_F2);
	rtl819x_gpio_direction_output(&rtl819x_gpio_peripheral, BSP_GPIO_PIN_F2, 1);
	rtl819x_gpio_pin_set_val(BSP_GPIO_PIN_F2, 1);

	proc_create("en_usb_power", 0, NULL, &en_usb_power_proc_fops);
	proc_create("en_usb_over_current", 0, NULL, &en_usb_over_current_proc_fops);
//	proc_create_data("en_usb_power", 0, &proc_root, &en_usb_power_proc_fops,NULL);
//	proc_create_data("en_usb_over_current", 0, &proc_root, &en_usb_over_current_proc_fops,NULL);
#endif
	return err;
}
device_initcall(rtl819x_gpio_peripheral_init);

int gpio_to_irq(unsigned gpio)
{
    /* FIXME */
    return -EINVAL;
}
EXPORT_SYMBOL(gpio_to_irq);

int irq_to_gpio(unsigned irq)
{
    /* FIXME */
    return -EINVAL;
}
EXPORT_SYMBOL(irq_to_gpio);  

static unsigned int rtl819x_gpio_mux(u32 pin, u32 *value, u32 *address )
{
	unsigned int mask = 0;

	switch(pin) {
// TODO evka: does it break WPS btn by shared address?
        case BSP_UART1_PIN:   
		mask = ((0xf<<3)|(7<<7)|(7<<10)|(0xf<<13));
		*value =  (0x7<<3)|(7<<7)|(7<<10)|(0x8<<13);
		*address=0xB800010C; /*SYS_PIN_MUX_SEL4*/
		break;

        case BSP_UART2_PIN:   
		mask = (0x1<<22)|(3<<23)|(3<<25);
		*value =  (0x0<<22)|(0<<23)|(0<<25);
		*address=0xB8000104;  /*SYS_PIN_MUX_SEL2*/
		break;

		case BSP_GPIO_PIN_F5: //WPS BTN
		mask = 0x3<<0;
		*value =  0x3<<0;
		*address=0xB800010C;  //Shared Pin Register(PIN_MUX_SEL_4)
		break;
		case BSP_GPIO_PIN_E7: //RESET BTN
		mask = 0x3<<27;
		*value =  0x3<<27;
		*address=0xB8000108;  //Shared Pin Register(PIN_MUX_SEL_3)
		break;
		case BSP_GPIO_PIN_E2: //WPS LED
		mask = 0x3<<15;
		*value =0x3<<15;
		*address=0xB8000108;
		break;
		case BSP_GPIO_PIN_B2: //LED WAN
		mask = 0x3<<0;
		*value =0x3<<0;
		*address=0xB8000108;
		break;
		case BSP_GPIO_PIN_B3: //LED LAN #1
		mask = 0x3<<3;
		*value =0x3<<3;
		*address=0xB8000108;
		break;
		case BSP_GPIO_PIN_B4: //LED LAN #2
		mask = 0x3<<6;
		*value =0x3<<6;
		*address=0xB8000108;
		break;
		case BSP_GPIO_PIN_F1: //LED LAN #3
		mask = 0x3<<21;
		*value =0x3<<21;
		*address=0xB8000108;
		break;
		case BSP_GPIO_PIN_B6: //LED LAN #4
		mask = 0x3<<12;
		*value =0x3<<12;
		*address=0xB8000108;
		break;
		case BSP_GPIO_PIN_F4: //Watchdog Enable
		mask = 0x3<<15;
		*value =0x3<<15;
		*address=0xB8000108;
		break;
		case BSP_GPIO_PIN_E1: //Watchdog Clock Pluse Out
		mask = 0x3<<15;
		*value =0x3<<15;
		*address=0xB8000108;
		break;
		case BSP_GPIO_PIN_B5: //Hardware Reset
		mask = 0x3<<9;
		*value =0x3<<9;
		*address=0xB8000108;
		break;
		case BSP_GPIO_PIN_F0: //RSSI
		mask = 0x3<<24;
		*value =0x3<<24;
		*address=0xB8000108;
		break;
		case BSP_GPIO_PIN_F2: //RSSI
		mask = 0x3<<18;
		*value =0x3<<18;
		*address=0xB8000108;
		break;
		case BSP_GPIO_PIN_H4: //USB3 Power
		case BSP_GPIO_PIN_H5: //Reset Button
		mask = 0x3<<23;
		*value =0x3<<23;
		*address=0xB800010C;
		break;
		case BSP_GPIO_PIN_F7: //USB Power Enable
		mask = 0x3<<21;
		*value =0x3<<21;
		*address=0xB800010C;
		break;
		case BSP_GPIO_PIN_H0: //USB Over Current
		mask = 0x3<<3;
		*value =0x3<<3;
		*address=0xB800010C;
		break;
/*
		case BSP_GPIO_PIN_B7: //Ext Antenna Switch Control Pin
		mask = 0x3<<20;
		*value =0x3<<20;
		*address=0xB8000110; //Shared Pin Register(PIN_MUX_SEL_5)
		break;
		case BSP_GPIO_PIN_E0: //Ext Antenna Switch Control Pin
		mask = 0x3<<18;
		*value =0x3<<18;
		*address=0xB8000110;
		break;
		case BSP_GPIO_PIN_C0: //Ext Antenna Switch Control Pin
		mask = 0x3<<16;
		*value =0x3<<16;
		*address=0xB8000110;
		break;
		case BSP_GPIO_PIN_H7: //Ext Antenna Switch Control Pin
		mask = 0x3<<14;
		*value =0x3<<14;
		*address=0xB8000110;
		break;
		case BSP_GPIO_PIN_C7: //Ext Antenna Switch Control Pin
		mask = 0x3<<22;
		*value =0x3<<22;
		*address=0xB8000110;
		break;
		case BSP_GPIO_PIN_H6: //Ext Antenna Switch Control Pin
		mask = 0x3<<0;
		*value =0x3<<0;
		*address=0xB8000110;
		break;
		case BSP_GPIO_PIN_F6: //Ext Antenna Switch Control Pin
		mask = 0x3<<0;
		*value =0x3<<0;
		*address=0xB800010C;
		break;
*/
		default:
		break;
	}

	return mask;
}


