/*
 *	Userspace interface
 *	Linux ethernet bridge
 *
 *	Authors:
 *	Lennert Buytenhek		<buytenh@gnu.org>
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version
 *	2 of the License, or (at your option) any later version.
 */

#include <linux/kernel.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/netpoll.h>
#include <linux/ethtool.h>
#include <linux/if_arp.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/rtnetlink.h>
#include <linux/if_ether.h>
#include <linux/slab.h>
#include <net/sock.h>
#include <linux/if_vlan.h>

#include "br_private.h"

#ifdef CONFIG_RTL_ACN_WDS_SUPPORT
#define WDS_MAX_VAP_NUM_PER_RADIO 8
#define WDS_MAX_WDS_NUM_PER_VAP	64
#define MAX_VLAN_BRIDGE_NUM		16
#define MAX_RESERVED_VLAN_ID		5
#define MGMT_VLAN_ID			1
struct vlan_bridge {
	int		  vlan_id;
	struct net_bridge *vlan_br;
};
static struct net_bridge *wlan0_br[WDS_MAX_VAP_NUM_PER_RADIO];
static struct net_bridge *wlan1_br[WDS_MAX_VAP_NUM_PER_RADIO];
static int wlan0_vlan[WDS_MAX_VAP_NUM_PER_RADIO];
static int wlan1_vlan[WDS_MAX_VAP_NUM_PER_RADIO];
static struct net_bridge *mgmt_br;
static int mgmt_br_count=0;
static struct vlan_bridge vlan_bridge_zone[MAX_VLAN_BRIDGE_NUM];
extern void autowds_clear_wds_vlan_dev(int radioid, int vapid);//in vlan.c
#endif

/*
 * Determine initial path cost based on speed.
 * using recommendations from 802.1d standard
 *
 * Since driver might sleep need to not be holding any locks.
 */
static int port_cost(struct net_device *dev)
{
	struct ethtool_cmd ecmd;

	if (!__ethtool_get_settings(dev, &ecmd)) {
		switch (ethtool_cmd_speed(&ecmd)) {
		case SPEED_10000:
			return 2;
		case SPEED_1000:
			return 4;
		case SPEED_100:
			return 19;
		case SPEED_10:
			return 100;
		}
	}

	/* Old silly heuristics based on name */
	if (!strncmp(dev->name, "lec", 3))
		return 7;

	if (!strncmp(dev->name, "plip", 4))
		return 2500;

	return 100;	/* assume old 10Mbps */
}


/* Check for port carrier transistions. */
void br_port_carrier_check(struct net_bridge_port *p)
{
	struct net_device *dev = p->dev;
	struct net_bridge *br = p->br;

	if (!(p->flags & BR_ADMIN_COST) &&
	    netif_running(dev) && netif_oper_up(dev))
		p->path_cost = port_cost(dev);

	if (!netif_running(br->dev))
		return;

	spin_lock_bh(&br->lock);
	if (netif_running(dev) && netif_oper_up(dev)) {
		if (p->state == BR_STATE_DISABLED)
			br_stp_enable_port(p);
	} else {
		if (p->state != BR_STATE_DISABLED)
			br_stp_disable_port(p);
	}
	spin_unlock_bh(&br->lock);
}

static void release_nbp(struct kobject *kobj)
{
	struct net_bridge_port *p
		= container_of(kobj, struct net_bridge_port, kobj);
	kfree(p);
}

static struct kobj_type brport_ktype = {
#ifdef CONFIG_SYSFS
	.sysfs_ops = &brport_sysfs_ops,
#endif
	.release = release_nbp,
};

static void destroy_nbp(struct net_bridge_port *p)
{
	struct net_device *dev = p->dev;

	p->br = NULL;
	p->dev = NULL;
	dev_put(dev);

	kobject_put(&p->kobj);
}

static void destroy_nbp_rcu(struct rcu_head *head)
{
	struct net_bridge_port *p =
			container_of(head, struct net_bridge_port, rcu);
	destroy_nbp(p);
}

/* Delete port(interface) from bridge is done in two steps.
 * via RCU. First step, marks device as down. That deletes
 * all the timers and stops new packets from flowing through.
 *
 * Final cleanup doesn't occur until after all CPU's finished
 * processing packets.
 *
 * Protected from multiple admin operations by RTNL mutex
 */
static void del_nbp(struct net_bridge_port *p)
{
	struct net_bridge *br = p->br;
	struct net_device *dev = p->dev;

	sysfs_remove_link(br->ifobj, p->dev->name);

	dev_set_promiscuity(dev, -1);

	spin_lock_bh(&br->lock);
	br_stp_disable_port(p);
	spin_unlock_bh(&br->lock);

	br_ifinfo_notify(RTM_DELLINK, p);

	nbp_vlan_flush(p);
	br_fdb_delete_by_port(br, p, 1);

	list_del_rcu(&p->list);

	dev->priv_flags &= ~IFF_BRIDGE_PORT;

	netdev_rx_handler_unregister(dev);

	netdev_upper_dev_unlink(dev, br->dev);

	br_multicast_del_port(p);

	kobject_uevent(&p->kobj, KOBJ_REMOVE);
	kobject_del(&p->kobj);

	br_netpoll_disable(p);

	call_rcu(&p->rcu, destroy_nbp_rcu);
}

/* Delete bridge device */
void br_dev_delete(struct net_device *dev, struct list_head *head)
{
	struct net_bridge *br = netdev_priv(dev);
	struct net_bridge_port *p, *n;

	list_for_each_entry_safe(p, n, &br->port_list, list) {
		del_nbp(p);
	}

	br_fdb_delete_by_port(br, NULL, 1);

	del_timer_sync(&br->gc_timer);

	br_sysfs_delbr(br->dev);
	unregister_netdevice_queue(br->dev, head);
}

/* find an available port number */
static int find_portno(struct net_bridge *br)
{
	int index;
	struct net_bridge_port *p;
	unsigned long *inuse;

	inuse = kcalloc(BITS_TO_LONGS(BR_MAX_PORTS), sizeof(unsigned long),
			GFP_KERNEL);
	if (!inuse)
		return -ENOMEM;

	set_bit(0, inuse);	/* zero is reserved */
	list_for_each_entry(p, &br->port_list, list) {
		set_bit(p->port_no, inuse);
	}
	index = find_first_zero_bit(inuse, BR_MAX_PORTS);
	kfree(inuse);

	return (index >= BR_MAX_PORTS) ? -EXFULL : index;
}

/* called with RTNL but without bridge lock */
static struct net_bridge_port *new_nbp(struct net_bridge *br,
				       struct net_device *dev)
{
	int index;
	struct net_bridge_port *p;

	index = find_portno(br);
	if (index < 0)
		return ERR_PTR(index);

	p = kzalloc(sizeof(*p), GFP_KERNEL);
	if (p == NULL)
		return ERR_PTR(-ENOMEM);

	p->br = br;
	dev_hold(dev);
	p->dev = dev;
	p->path_cost = port_cost(dev);
	p->priority = 0x8000 >> BR_PORT_BITS;
	p->port_no = index;
	p->flags = 0;
	br_init_port(p);
	p->state = BR_STATE_DISABLED;
	br_stp_port_timer_init(p);
	br_multicast_add_port(p);

	return p;
}

int br_add_bridge(struct net *net, const char *name)
{
	struct net_device *dev;
	int res;

	dev = alloc_netdev(sizeof(struct net_bridge), name,
			   br_dev_setup);

	if (!dev)
		return -ENOMEM;

	dev_net_set(dev, net);
	dev->rtnl_link_ops = &br_link_ops;

	res = register_netdev(dev);
	if (res)
		free_netdev(dev);
	return res;
}

int br_del_bridge(struct net *net, const char *name)
{
	struct net_device *dev;
	int ret = 0;

	rtnl_lock();
	dev = __dev_get_by_name(net, name);
	if (dev == NULL)
		ret =  -ENXIO; 	/* Could not find device */

	else if (!(dev->priv_flags & IFF_EBRIDGE)) {
		/* Attempt to delete non bridge device! */
		ret = -EPERM;
	}

	else if (dev->flags & IFF_UP) {
		/* Not shutdown yet. */
		ret = -EBUSY;
	}

	else {
#ifdef CONFIG_RTL_ACN_WDS_SUPPORT
		struct net_bridge *br = netdev_priv(dev);
		int id;
		
		br_dev_delete(dev, NULL);

		for (id = 0; id < WDS_MAX_VAP_NUM_PER_RADIO; id++)
			if (wlan0_br[id] && wlan0_br[id] == br) {
				wlan0_br[id] = NULL;
				wlan0_vlan[id] = 0;
				autowds_clear_wds_vlan_dev(0, id);
			}
			else if (wlan1_br[id] && wlan1_br[id] == br) {
				wlan1_br[id] = NULL;
				wlan1_vlan[id] = 0;
				autowds_clear_wds_vlan_dev(1, id);
			}

		if (!strncmp(name, "br-wan", 6)) {
			mgmt_br = NULL;

			mgmt_br_count = 0;
		}
#else
		br_dev_delete(dev, NULL);
#endif
	}

	rtnl_unlock();
	return ret;
}

/* MTU of the bridge pseudo-device: ETH_DATA_LEN or the minimum of the ports */
int br_min_mtu(const struct net_bridge *br)
{
	const struct net_bridge_port *p;
	int mtu = 0;

	ASSERT_RTNL();

	if (list_empty(&br->port_list))
		mtu = ETH_DATA_LEN;
	else {
		list_for_each_entry(p, &br->port_list, list) {
			if (!mtu  || p->dev->mtu < mtu)
				mtu = p->dev->mtu;
		}
	}
	return mtu;
}

/*
 * Recomputes features using slave's features
 */
netdev_features_t br_features_recompute(struct net_bridge *br,
	netdev_features_t features)
{
	struct net_bridge_port *p;
	netdev_features_t mask;

	if (list_empty(&br->port_list))
		return features;

	mask = features;
	features &= ~NETIF_F_ONE_FOR_ALL;

	list_for_each_entry(p, &br->port_list, list) {
		features = netdev_increment_features(features,
						     p->dev->features, mask);
	}

	return features;
}

#ifdef CONFIG_RTL_ACN_WDS_SUPPORT

/* called with RTNL */
int br_add_if(struct net_bridge *br, struct net_device *dev, int by_autowds)
#else
/* called with RTNL */
int br_add_if(struct net_bridge *br, struct net_device *dev)
#endif
{
	struct net_bridge_port *p;
	int err = 0;
	bool changed_addr;

	/* Don't allow bridging non-ethernet like devices */
	if ((dev->flags & IFF_LOOPBACK) ||
	    dev->type != ARPHRD_ETHER || dev->addr_len != ETH_ALEN ||
	    !is_valid_ether_addr(dev->dev_addr))
		return -EINVAL;

	/* No bridging of bridges */
	if (dev->netdev_ops->ndo_start_xmit == br_dev_xmit)
		return -ELOOP;

	/* Device is already being bridged */
	if (br_port_exists(dev))
		return -EBUSY;

	/* No bridging devices that dislike that (e.g. wireless) */
	if (dev->priv_flags & IFF_DONT_BRIDGE)
		return -EOPNOTSUPP;

	p = new_nbp(br, dev);
	if (IS_ERR(p))
		return PTR_ERR(p);

	call_netdevice_notifiers(NETDEV_JOIN, dev);

	err = dev_set_promiscuity(dev, 1);
	if (err)
		goto put_back;

	err = kobject_init_and_add(&p->kobj, &brport_ktype, &(dev->dev.kobj),
				   SYSFS_BRIDGE_PORT_ATTR);
	if (err)
		goto err1;

	err = br_sysfs_addif(p);
	if (err)
		goto err2;

	if (br_netpoll_info(br) && ((err = br_netpoll_enable(p, GFP_KERNEL))))
		goto err3;

	err = netdev_master_upper_dev_link(dev, br->dev);
	if (err)
		goto err4;

	err = netdev_rx_handler_register(dev, br_handle_frame, p);
	if (err)
		goto err5;

	dev->priv_flags |= IFF_BRIDGE_PORT;

#ifdef CONFIG_RTL_ACN_WDS_SUPPORT
	if (!by_autowds) {
		int id;
		int vlan_id;
		char *vlan_id_txt;

		vlan_id_txt = strchr(dev->name, '.');

		if (!strncmp(dev->name, "wlan0", 5)) {
			if ((strlen(dev->name) == 5) || (dev->name[5]=='.')) {//wlan0 or wlan0.100
				id = 0;
				wlan0_br[id] = br;
			}
			else {
				if ((strlen(dev->name) == 7) || (dev->name[7]=='.')) {////wlan0-1 or wlan0-1.100
					id = dev->name[6] - '0';
					if ((id > 0) && (id < WDS_MAX_VAP_NUM_PER_RADIO))
						wlan0_br[id] = br;
				}
			}

			if ((id >= 0) && (id < WDS_MAX_VAP_NUM_PER_RADIO)) {
				if (vlan_id_txt != NULL) {
					vlan_id_txt++;

					vlan_id = (int)simple_strtoul(vlan_id_txt, 0 ,10);

					wlan0_vlan[id] = vlan_id;
				}
				else {
					wlan0_vlan[id] = 0;
				}
			}
		}
		else if (!strncmp(dev->name, "wlan1", 5))	{ 
			if ((strlen(dev->name) == 5) || (dev->name[5]=='.')) {//wlan1 or wlan1.100
				id = 0;
				wlan1_br[id] = br;
			}
			else {
				if ((strlen(dev->name) == 7) || (dev->name[7]=='.')) {////wlan1-1 or wlan1-1.100
					id = dev->name[6] - '0';
					if ((id > 0) && (id < WDS_MAX_VAP_NUM_PER_RADIO))
						wlan1_br[id] = br;
				}
			}

			if ((id >= 0) && (id < WDS_MAX_VAP_NUM_PER_RADIO)) {
				if (vlan_id_txt != NULL) {
					vlan_id_txt++;

					vlan_id = (int)simple_strtoul(vlan_id_txt, 0 ,10);

					wlan1_vlan[id] = vlan_id;
				}
				else {
					wlan1_vlan[id] = 0;
		                }
	                }
		} else if (!strncmp(dev->name, "eth", 3)) {
			int j, k;

			if (vlan_id_txt != NULL) {
				vlan_id_txt++;

				vlan_id = (int)simple_strtoul(vlan_id_txt, 0 ,10);

				if (vlan_id > MAX_RESERVED_VLAN_ID) {
					for (j = 0, k = 0; j < MAX_VLAN_BRIDGE_NUM; j++) {
						if (vlan_bridge_zone[j].vlan_id > 0) {
							if (vlan_bridge_zone[j].vlan_id ==  vlan_id) {
								k = j + 1;

								break;
							}
						}
						else {
							if (k == 0)
								k = j + 1;
						}
					}

					if (k > 0) {
						vlan_bridge_zone[k - 1].vlan_id = vlan_id;

						vlan_bridge_zone[k - 1].vlan_br = br;
					}
				}
				else if (vlan_id == MGMT_VLAN_ID) {
					if (mgmt_br_count == 0)
						mgmt_br = br;

					mgmt_br_count++;
		                }
		        }
		}
	}
#endif

	dev_disable_lro(dev);

	list_add_rcu(&p->list, &br->port_list);

	netdev_update_features(br->dev);

	spin_lock_bh(&br->lock);
	changed_addr = br_stp_recalculate_bridge_id(br);

	if (netif_running(dev) && netif_oper_up(dev) &&
	    (br->dev->flags & IFF_UP))
		br_stp_enable_port(p);
	spin_unlock_bh(&br->lock);

	br_ifinfo_notify(RTM_NEWLINK, p);

	if (changed_addr)
		call_netdevice_notifiers(NETDEV_CHANGEADDR, br->dev);

	dev_set_mtu(br->dev, br_min_mtu(br));

	if (br_fdb_insert(br, p, dev->dev_addr, 0))
		netdev_err(dev, "failed insert local address bridge forwarding table\n");

	kobject_uevent(&p->kobj, KOBJ_ADD);

	return 0;

err5:
	netdev_upper_dev_unlink(dev, br->dev);
err4:
	br_netpoll_disable(p);
err3:
	sysfs_remove_link(br->ifobj, p->dev->name);
err2:
	kobject_put(&p->kobj);
	p = NULL; /* kobject_put frees */
err1:
	dev_set_promiscuity(dev, -1);
put_back:
	dev_put(dev);
	kfree(p);
	return err;
}

/* called with RTNL */
#ifdef CONFIG_RTL_ACN_WDS_SUPPORT
int br_del_if(struct net_bridge *br, struct net_device *dev, int by_autowds)
#else
int br_del_if(struct net_bridge *br, struct net_device *dev)
#endif
{
	struct net_bridge_port *p;
	bool changed_addr;

	p = br_port_get_rtnl(dev);
	if (!p || p->br != br)
		return -EINVAL;

#ifdef CONFIG_RTL_ACN_WDS_SUPPORT
	if (!by_autowds) {
		int id;
		int vlan_id;
		char *vlan_id_txt;

		vlan_id_txt = strchr(dev->name, '.');

		if (!strncmp(dev->name, "wlan0", 5)) {
			if ((strlen(dev->name) == 5) || (dev->name[5] == '.')) {
				id = 0;
				wlan0_br[id] = NULL;
			}
			else {
				if ((strlen(dev->name) == 7) || (dev->name[7] == '.')) {
				        id = dev->name[6] - '0';
				        if ((id > 0) && (id < WDS_MAX_VAP_NUM_PER_RADIO)) {
					        wlan0_br[id] = NULL;
				        }
			        }
		        }

			if ((id >= 0) && (id < WDS_MAX_VAP_NUM_PER_RADIO)) {
				wlan0_vlan[id] = 0;
			}
		}
		else if (!strncmp(dev->name, "wlan1", 5))	{ 
			if ((strlen(dev->name) == 5) || (dev->name[5] == '.')) {
				id = 0;
				wlan1_br[id] = NULL;
			}
			else {
				if ((strlen(dev->name) == 7) || (dev->name[7] == '.')) {
				        id = dev->name[6] - '0';
				        if ((id > 0) && (id < WDS_MAX_VAP_NUM_PER_RADIO)) {
					        wlan1_br[id] = NULL;
				        }
			        }
		        }

			if ((id >= 0) && (id < WDS_MAX_VAP_NUM_PER_RADIO)) {
				wlan1_vlan[id] = 0;
                        }
                } else if (!strncmp(dev->name, "eth", 3)) {
			int j;

			if (vlan_id_txt != NULL) {
				vlan_id_txt++;

				vlan_id = (int)simple_strtoul(vlan_id_txt, 0 ,10);

				if (vlan_id > MAX_RESERVED_VLAN_ID) {
					for (j = 0; j < MAX_VLAN_BRIDGE_NUM; j++) {
						if ( (vlan_bridge_zone[j].vlan_id > 0) &&
						     (vlan_bridge_zone[j].vlan_id ==  vlan_id) ){
							vlan_bridge_zone[j].vlan_id = 0;

							vlan_bridge_zone[j].vlan_br = NULL;

							break;
		                                }
	                                }
				}
				else if (vlan_id == MGMT_VLAN_ID) {
					mgmt_br_count--;

					if (mgmt_br_count == 0)
						mgmt_br = NULL;
		                }
	                }
		}
	}
#endif
	/* Since more than one interface can be attached to a bridge,
	 * there still maybe an alternate path for netconsole to use;
	 * therefore there is no reason for a NETDEV_RELEASE event.
	 */
	del_nbp(p);

	spin_lock_bh(&br->lock);
	changed_addr = br_stp_recalculate_bridge_id(br);
	spin_unlock_bh(&br->lock);

	if (changed_addr)
		call_netdevice_notifiers(NETDEV_CHANGEADDR, br->dev);

	netdev_update_features(br->dev);

	return 0;
}

void __net_exit br_net_exit(struct net *net)
{
	struct net_device *dev;
	LIST_HEAD(list);

	rtnl_lock();
	for_each_netdev(net, dev)
		if (dev->priv_flags & IFF_EBRIDGE)
			br_dev_delete(dev, &list);

	unregister_netdevice_many(&list);
	rtnl_unlock();

}

#ifdef CONFIG_RTL_ACN_WDS_SUPPORT
#define BRIDGE_WAIT_CNT 100 
#define BRIDGE_WAIT_MS 100
void autowds_br_add_if(struct net_device *dev, int wait_flag) {
	int id;
	int wait_cnt = 0;

	int vlan_id;
	char *vlan_id_txt;

	printk("autowds_br_add_if: %s\n", dev->name);
	
	vlan_id_txt = strchr(dev->name, '.');

	if (vlan_id_txt != NULL) {
		vlan_id_txt++;

		vlan_id = (int)simple_strtoul(vlan_id_txt, 0 ,10);

		if (vlan_id == 1) {
			if (mgmt_br != NULL) {
				rtnl_lock();
				br_add_if(mgmt_br, dev, 1);
				rtnl_unlock();
				return;
			}
			else {
				return;
			}
		}
	}
	else {
		vlan_id = 0;
	}

	if (!strncmp(dev->name, "wlan0", 5)) {
		if (dev->name[5] == '-') {
			id = (int)(dev->name[6] - '0');
			if ((id <= 0) || (id >= WDS_MAX_VAP_NUM_PER_RADIO)) {
				printk("invalid autowds_br_add_if dev name\n");
				return;
			}
		}	
		else
			id = 0;				
		
		//if br not exists, wait for max 2 sec
		while (!wlan0_br[id] && wait_flag && (wait_cnt < BRIDGE_WAIT_CNT)) {
			msleep(BRIDGE_WAIT_MS); //called by WdsControl kernel thread, msleep is allowed
			wait_cnt++;
		}
		if (wait_cnt) printk("null br wait_cnt %d\n",wait_cnt);
		if (wlan0_br[id]) {
			if ((vlan_id == 0) || (wlan0_vlan[id] == vlan_id)) {
				rtnl_lock();
				br_add_if(wlan0_br[id], dev, 1);
				rtnl_unlock();
			}
			else {
				int j;
				for (j = 0; j < MAX_VLAN_BRIDGE_NUM; j++) {
					if (vlan_bridge_zone[j].vlan_id == vlan_id) {
						if (vlan_bridge_zone[j].vlan_br) {
							rtnl_lock();
							if (vlan_bridge_zone[j].vlan_br)
								br_add_if(vlan_bridge_zone[j].vlan_br, dev, 1);
							rtnl_unlock();
						}
						break;
					}
				}
			}
		}
		else {
			int j;
			printk("?null wlan0_br\n");
			for (j = 0; j < MAX_VLAN_BRIDGE_NUM; j++) {
				if (vlan_bridge_zone[j].vlan_id == vlan_id) {
					if (vlan_bridge_zone[j].vlan_br) {
						rtnl_lock();
						if (vlan_bridge_zone[j].vlan_br)
							br_add_if(vlan_bridge_zone[j].vlan_br, dev, 1);
						rtnl_unlock();
					}
					break;
				}
			}

			if (j == MAX_VLAN_BRIDGE_NUM)
				printk("VLAN bridge does not exist.\n",id);
		}
	}
	else { //wlan1
		if (dev->name[5] == '-') {
			id = (int)(dev->name[6] - '0');
			if ((id <= 0) || (id >= WDS_MAX_VAP_NUM_PER_RADIO)) {
				printk("invalid autowds_br_add_if dev name\n");
				return;
			}
		}	
		else
			id = 0;		

		//if br not exists, wait for max 2 sec
		while (!wlan1_br[id] && wait_flag && (wait_cnt < BRIDGE_WAIT_CNT)) {
			msleep(BRIDGE_WAIT_MS); //called by WdsControl kernel thread, msleep is allowed
			wait_cnt++;
		}
		if (wait_cnt) printk("null br wait_cnt %d\n",wait_cnt);
		if (wlan1_br[id]) {
			if ((vlan_id == 0) || (wlan1_vlan[id] == vlan_id)) {
				rtnl_lock();
				br_add_if(wlan1_br[id], dev, 1);
				rtnl_unlock();
			}
			else {
				int j;
				for (j = 0; j < MAX_VLAN_BRIDGE_NUM; j++) {
					if (vlan_bridge_zone[j].vlan_id == vlan_id) {
						if (vlan_bridge_zone[j].vlan_br) {
							rtnl_lock();
							if (vlan_bridge_zone[j].vlan_br)
								br_add_if(vlan_bridge_zone[j].vlan_br, dev, 1);
							rtnl_unlock();
						}
						break;
					}
				}
			}
		}
		else {
			int j;

			for (j = 0; j < MAX_VLAN_BRIDGE_NUM; j++) {
				if (vlan_bridge_zone[j].vlan_id == vlan_id) {
					if (vlan_bridge_zone[j].vlan_br) {
						rtnl_lock();
						if (vlan_bridge_zone[j].vlan_br)
							br_add_if(vlan_bridge_zone[j].vlan_br, dev, 1);
						rtnl_unlock();
					}
					break;
				}
			}

			if (j == MAX_VLAN_BRIDGE_NUM)
				printk("VLAN bridge does not exist.\n",id);
		}
	}
}

void autowds_br_del_if(struct net_device *dev) {
	int id;

	int vlan_id;
	char *vlan_id_txt;

	printk("autowds_br_del_if: dev %08X %s\n",dev,dev->name);
	rtnl_lock();

	vlan_id_txt = strchr(dev->name, '.');

	if (vlan_id_txt != NULL) {
		vlan_id_txt++;

		vlan_id = (int)simple_strtoul(vlan_id_txt, 0 ,10);

		if (vlan_id == 1) {
			if (mgmt_br != NULL) {
				br_del_if(mgmt_br, dev, 1);

				rtnl_unlock();

				return;
			} 
			else {
				rtnl_unlock();

				return;
			}
		}
	}
	else {
		vlan_id = 0;
	}

	if (!strncmp(dev->name, "wlan0", 5)) {
		if (dev->name[5] == '-') {
			id = (int)(dev->name[6] - '0');
			if ((id <= 0) || (id >= WDS_MAX_VAP_NUM_PER_RADIO)) {
					printk("invalid autowds_br_del_if dev name\n");
					rtnl_unlock();
					return;
			}
		}	
		else
			id = 0;		

		if (wlan0_br[id]) {
			if (wlan0_vlan[id] == vlan_id) {
			        br_del_if(wlan0_br[id], dev, 1);
			}
			else {
				int j;

				for (j = 0; j < MAX_VLAN_BRIDGE_NUM; j++) {
					if (vlan_bridge_zone[j].vlan_id == vlan_id) {
						if (vlan_bridge_zone[j].vlan_br)
							br_del_if(vlan_bridge_zone[j].vlan_br, dev, 1);

						break;
					}
				}
			}
		}
		else
			printk("wlan0_br[%d] null !\n",id);	
	}
	else { //wlan1
		if (dev->name[5] == '-') {
			id = (int)(dev->name[6] - '0');
			if ((id <= 0) || (id >= WDS_MAX_VAP_NUM_PER_RADIO)) {
				printk("invalid autowds_br_del_if dev name\n");
				rtnl_unlock();
				return;
			}
		}	
		else
			id = 0;		

		if (wlan1_br[id]) {
			if (wlan1_vlan[id] == vlan_id) {
			        br_del_if(wlan1_br[id], dev, 1);
			}
			else {
				int j;

				for (j = 0; j < MAX_VLAN_BRIDGE_NUM; j++) {
					if (vlan_bridge_zone[j].vlan_id == vlan_id) {
						if (vlan_bridge_zone[j].vlan_br)
							br_del_if(vlan_bridge_zone[j].vlan_br, dev, 1);

						break;
					}
				}
			}
		}
		else	
			printk("wlan1_br[%d] null !\n",id);	
	}
	rtnl_unlock();
}

EXPORT_SYMBOL(autowds_br_add_if);
EXPORT_SYMBOL(autowds_br_del_if);
/*for dev2 netifd disabling wds interface*/
int isWdsDev(char* devname, int* radioId, int* vapId, int* wdsnum) {
	int len = strlen(devname);
	int i;
	int ret=0;
	if (len<=5) return 0;
	if (!strncmp(devname, "wlan0", 5)) {
		*radioId = 0;
		if (devname[5] == '-') {
			*vapId = (int)(devname[6] - '0');
			if ((*vapId <= 0) || (*vapId >= WDS_MAX_VAP_NUM_PER_RADIO)) {
					printk("invalid devname\n");
					return 0;
			}
		}	
		else
			*vapId = 0;		
	}
	else if (!strncmp(devname, "wlan1", 5)) {//wlan1
		*radioId = 1;
		if (devname[5] == '-') {
			*vapId = (int)(devname[6] - '0');
			if ((*vapId <= 0) || (*vapId >= WDS_MAX_VAP_NUM_PER_RADIO)) {
				printk("invalid devname\n");
				return 0;
			}
		}	
		else
			*vapId = 0;		
	}
	else
		return 0;
	
	for (i=5;i<len;i++) {
		if (devname[i]=='w') {
			ret = 1;
			break;
		}
	}
	if (ret) {	
		*wdsnum = (int)simple_strtoul(devname+i+1, 0 ,10);
		return 1;
	}
	else
		return 0;
}
#endif