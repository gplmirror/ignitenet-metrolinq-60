#include <linux/init.h>
#include <linux/skbuff.h>
#include <linux/netdevice.h>

//#define DBG

#if defined(DBG)
#define DEBUG_ERR printk
#else
#define DEBUG_ERR(format, args...)
#endif

#if defined(DBG)
  int _lock_owner_buf = -1;

  #define SMP_LOCK_RTL_BUF(__x__)	\
	do { \
		if(_lock_owner_buf!=smp_processor_id()) {\
			spin_lock_irqsave(&lock_rtl_buf, (__x__)); \
		} else \
			printk("[%s %d] recursion detection in BUF\n",__FUNCTION__,__LINE__); \
		_lock_owner_buf=smp_processor_id();\
	} while(0)
	
  #define SMP_UNLOCK_RTL_BUF(__x__) \
	do { \
		_lock_owner_buf=-1; \
		spin_unlock_irqrestore(&lock_rtl_buf, (__x__)); \
	} while(0)
#else /* !DBG */		
  #define SMP_LOCK_RTL_BUF(__x__)			spin_lock_irqsave(&lock_rtl_buf, (__x__))	
  #define SMP_UNLOCK_RTL_BUF(__x__) 		spin_unlock_irqrestore(&lock_rtl_buf, (__x__))	
#endif /* DBG */


#define RTL_MAGIC_CODE		"@RTK"
#define RTL_MAGIC_LEN		4


//#define RTL_SKB_BUF_SIZE 2348  //8112 //PRS_MAC_RX_MAX_DATA_BUFFER_SIZE
//#define MAX_RTL_SKB_NUM 256
#define RTL_SKB_BUF_SIZE 9200
#define MAX_RTL_SKB_NUM 128

#define RTL_PRIV_DATA_SIZE		128

struct rtl_skb {
	unsigned char magic[RTL_MAGIC_LEN];
	void			*buf_pointer;
	/* the below 2 filed MUST together */	
	struct list_head	list; 
	unsigned char buf[RTL_SKB_BUF_SIZE];
};

static struct rtl_skb rtl_skb_buf[MAX_RTL_SKB_NUM+1];
static struct list_head rtl_skb_list;
static int rtl_skb_free_num;
spinlock_t lock_rtl_buf;


static struct sk_buff *__rtl_alloc_skb_hdr(unsigned char *data, int size)
{
	struct sk_buff *skb;
	struct skb_shared_info *shinfo;

	struct kmem_cache *cache;
	cache = skbuff_head_cache;
	/* Get the HEAD */
	skb = kmem_cache_alloc(cache, GFP_ATOMIC & ~__GFP_DMA);
	if (!skb)
		goto out;

	memset(skb, 0, offsetof(struct sk_buff, truesize));
	atomic_set(&skb->users, 1);
	skb->head = data;
	skb->data = data;
	skb->tail = data;

	size = SKB_DATA_ALIGN(size+RTL_PRIV_DATA_SIZE+NET_SKB_PAD);

	skb->end  = data + size;
	skb->truesize = size + sizeof(struct sk_buff);

	/* make sure we initialize shinfo sequentially */
	shinfo = skb_shinfo(skb);
	atomic_set(&shinfo->dataref, 1);
	shinfo->nr_frags  = 0;
	shinfo->gso_size = 0;
	shinfo->gso_segs = 0;
	shinfo->gso_type = 0;
	shinfo->ip6_frag_id = 0;
	shinfo->frag_list = NULL;

	skb_reserve(skb, RTL_PRIV_DATA_SIZE);
	return skb;

out:
	return NULL;
}


static __inline__ unsigned char *__get_buf_from_poll(struct list_head *phead, unsigned int *count)
{
	unsigned long flags=0;
	unsigned char *buf;
	struct list_head *plist;

	SMP_LOCK_RTL_BUF(flags);

	if (list_empty(phead)) {
		SMP_UNLOCK_RTL_BUF(flags);
		DEBUG_ERR("rtl_skb: phead=%X buf is empty now!\n", (unsigned int)phead);
		DEBUG_ERR("free count %d\n", *count);
		return NULL;
	}

	if (*count == 1) {
		SMP_UNLOCK_RTL_BUF(flags);
		DEBUG_ERR("rtl_skb: phead=%X under-run!\n", (unsigned int)phead);
		return NULL;
	}

	*count = *count - 1;
	plist = phead->next;
	list_del_init(plist);
	buf = (unsigned char *)((unsigned int)plist + sizeof (struct list_head));
	SMP_UNLOCK_RTL_BUF(flags);
	return buf;
}


static __inline__ void __release_buf_to_poll(unsigned char *pbuf, struct list_head	*phead, unsigned int *count)
{
	unsigned long flags=0;
	struct list_head *plist;

	SMP_LOCK_RTL_BUF(flags);

	*count = *count + 1;
	plist = (struct list_head *)((unsigned int)pbuf - sizeof(struct list_head));
	list_add_tail(plist, phead);
	SMP_UNLOCK_RTL_BUF(flags);
}


struct sk_buff *rtl_alloc_skb(unsigned int size)
{
	struct sk_buff *skb;
	unsigned char *data;

	/* first argument is not used */
	if(rtl_skb_free_num>0)
	{
		data = __get_buf_from_poll(&rtl_skb_list, (unsigned int *)&rtl_skb_free_num);
		if (data == NULL) {
			DEBUG_ERR("rtl_skb: priv_skb buffer empty!\n");
			//return NULL;
			goto alloc;
		}

		skb = __rtl_alloc_skb_hdr(data, size);

		if (skb == NULL) {
			__release_buf_to_poll(data, &rtl_skb_list, (unsigned int *)&rtl_skb_free_num);
			DEBUG_ERR("alloc linux_skb buff failed!\n");
			//return NULL;
			goto alloc;
		}
		return skb;
	}

alloc:
	if (skb == NULL) {
		skb = dev_alloc_skb(size);
		if (likely(skb))
		    return skb;
	}

	return NULL;
}
EXPORT_SYMBOL(rtl_alloc_skb);


void rtl_free_skb(unsigned char *head)
{
	__release_buf_to_poll(head, &rtl_skb_list, (unsigned int *)&rtl_skb_free_num);
}
EXPORT_SYMBOL(rtl_free_skb);


int is_rtl_skb(unsigned char *head)
{
	unsigned long offset = (unsigned long)(&((struct rtl_skb *)0)->buf);
	struct rtl_skb *priv_buf = (struct rtl_skb *)(((unsigned long)head) - offset);

	if ((!memcmp(priv_buf->magic, RTL_MAGIC_CODE, RTL_MAGIC_LEN)) &&
		(priv_buf->buf_pointer==(void*)(priv_buf))) {
		return 1;
	}
	else {
		return 0;
	}
}
EXPORT_SYMBOL(is_rtl_skb);


void init_rtl_priv_skb_buf(void)
{
	int i;

	printk("  ==>  %s:%d, MAX_RTL_SKB_NUM=%d, RTL_SKB_BUF_SIZE=%d\n", __func__, __LINE__, MAX_RTL_SKB_NUM, RTL_SKB_BUF_SIZE);//bruce
	
	spin_lock_init(&lock_rtl_buf);

	DEBUG_ERR("Init priv skb.\n");
	memset(rtl_skb_buf, '\0', sizeof(struct rtl_skb)*(MAX_RTL_SKB_NUM));
	INIT_LIST_HEAD(&rtl_skb_list);
	rtl_skb_free_num=MAX_RTL_SKB_NUM;

	for (i=0; i<MAX_RTL_SKB_NUM; i++)  {
		memcpy(rtl_skb_buf[i].magic, RTL_MAGIC_CODE, RTL_MAGIC_LEN);
		rtl_skb_buf[i].buf_pointer = (void*)(&rtl_skb_buf[i]);
		INIT_LIST_HEAD(&rtl_skb_buf[i].list);
		list_add_tail(&rtl_skb_buf[i].list, &rtl_skb_list);
	}
}
EXPORT_SYMBOL(init_rtl_priv_skb_buf);
